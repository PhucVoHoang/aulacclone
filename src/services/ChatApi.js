import { AsyncStorage, Alert } from 'react-native';
import axios from 'axios';
import qs from 'qs';
import _ from 'lodash';
import AppConfig from '../config/AppConfig';
import {getInternetCredentials} from './storageServices';

function checkServerStatus(data) {
  if (data.status === 200) {
    return data.data;
  }
  const error = new Error(data.message || 'Network error');
  error.error_fields = data.error_fields;
  throw error;
}

function sendRequest(url, options) {
  return axios(url, options)
    .then(res => {
      if (__DEV__) {
        let { data } = res;
      }
      return checkServerStatus(res);
    })
    .catch(err => {
      console.log('!!! request error sendRequest: ', url, err);
      throw err;
    });
}

class ChatApi {
    constructor() {
        this.headers = {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        };

        this.request = this.request.bind(this);
    }


    request(url, options) {
        options = options || { method: 'GET' };
        options.headers = Object.assign({}, this.headers, options.headers || {});
        return getInternetCredentials().then(credential => {
            if (credential.username && credential.password) {
                const userInfo = JSON.parse(credential.username);
                options.headers['X-Auth-Token'] = userInfo.authToken;
                options.headers['X-User-Id'] = userInfo.userId;
                console.log(options)
            }
            return sendRequest(url, options).then(json => json).catch(err => {
                console.log(err)
                throw err;
            });
        });
    }

    register(info) {
        const data = JSON.stringify(info);
        const options = {
        method: 'POST',
        headers: this.headers,
        data
        };
        const url = AppConfig.baseChatURL + '/api/v1/users.register';
        return axios(url, options)
        .then(res => {
            return checkServerStatus(res);
        })
        .catch(err => {
            console.log('!!! request error:', err);
            throw err;
        });
    }
    getChatrooms(params) {
        const { page } = params;
        const offset = (page - 1) * 10;
        return this.request(AppConfig.baseChatURL + `/api/v1/groups.list?count=10&offset=${offset}`);
    }
    getChatroom(params) {
        return this.request(AppConfig.baseChatURL + `/api/v1/groups.info?${params}`);
    }
    leaveChatroom(params) {
        const data = JSON.stringify(params);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/groups.delete`, options);
    }
    getChatroomMembers(params) {
        return this.request(AppConfig.baseChatURL + `/api/v1/groups.members?${params}`);
    }
    addChatroom(params) {
        const data = JSON.stringify(params);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        console.log('addChatroom', options);
        return this.request(AppConfig.baseChatURL + `/api/v1/groups.create`, options);
    }
    getVisitor(info) {
        const data = JSON.stringify(info);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/login`, options);
    }
    getProfile() {
        return this.request(AppConfig.baseChatURL + `/api/v1/me`);
    }
    getMessages(params){
        const { page, id } = params;
        const offset = (page - 1) * 100;
        return this.request(AppConfig.baseChatURL + `/api/v1/groups.messages?roomId=${id}&count=100&offset=${offset}&sort={"ts":1}`);
    }
    getMembers(params) {
        const { page, id } = params;
        const uri = 'groups.members';
        return this.request(AppConfig.baseChatURL + `/api/v1/${uri}?roomId=${id}&count=100`);
    }
    addMessage(info){
        const data = JSON.stringify(info);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/chat.postMessage`, options);
    }
    removeMessage(info){
        const data = JSON.stringify(info);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/chat.delete`, options);
    }
    addFile(info){
        return getInternetCredentials().then(credential => {
            if (credential.username && credential.password) {
                const userInfo = JSON.parse(credential.username);
                const uploadUrl = AppConfig.baseChatURL + `/api/v1/rooms.upload/${info.rid}`;

                const xhr = new XMLHttpRequest();
                const formData = new FormData();
                xhr.open('POST', uploadUrl);
                formData.append('file', {
                    uri: Platform.OS === 'android' ? info.photo.uri : info.photo.uri.replace('file://', ''),
                    type: info.photo.type,
                    name: info.photo.uri.substring(info.photo.uri.trim().lastIndexOf("/") + 1)
                });
                xhr.setRequestHeader('X-Auth-Token', userInfo.authToken);
                xhr.setRequestHeader('X-User-Id', userInfo.userId);
                xhr.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        const response = JSON.parse(xhr.response);
                        
                    }
                  };
                xhr.onerror = async(error) => {
                    console.log('error', error)
                }
                xhr.send(formData);
                return JSON.parse(xhr.response);
            }
        });
        
    }
    addFriends(ids) {
        let options = {
            method: 'POST',
            headers: this.headers,
        };
        return getInternetCredentials().then(credential => {
            if (credential.username && credential.password) {
                const userInfo = JSON.parse(credential.username);
                options.headers['X-Auth-Token'] = userInfo.authToken;
                options.headers['X-User-Id'] = userInfo.userId;
                const data = JSON.stringify({userId: userInfo.userId, data: {bio: ids}});
                options.data = data
            }
            return sendRequest(AppConfig.baseChatURL + `/api/v1/users.update`, options).then(json => json).catch(err => {
                alert('Người dùng không tồn tại.');
            });
        });
    }
    addOtherFriends(id, friend) {
        let options = {
            method: 'POST',
            headers: this.headers,
        };
        return getInternetCredentials().then(credential => {
            if (credential.username && credential.password) {
                const userInfo = JSON.parse(credential.username);
                options.headers['X-Auth-Token'] = userInfo.authToken;
                options.headers['X-User-Id'] = userInfo.userId;
                const data = JSON.stringify({userId: id, data: {bio: friend}});
                options.data = data
            }
            return sendRequest(AppConfig.baseChatURL + `/api/v1/users.update`, options).then(json => json).catch(err => {
                throw err;
            });
        });
    }
    getFriendInfo(username) {
        return this.request(AppConfig.baseChatURL + `/api/v1/users.info?username=${username}`).catch(err => {
            alert('Không tồn tại user này.')
        });
    }
    getFriends(ids) {
        return this.request(AppConfig.baseChatURL + `/api/v1/users.list?query={ "username": { "$in": [${ids}] } }`);
    }
    getSuggestFriends(ids) {
        return this.request(AppConfig.baseChatURL + `/api/v1/users.list?query={ "username": { "$nin": [${ids}] } }`);
    }
    registerLiveGuest(parents){
        const data = JSON.stringify(parents);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/livechat:registerGuest`, options);
    }
    addLiveMessage(info){
        const data = JSON.stringify(info);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/sendMessageLivechat`, options);
    }
    getMessageReadReceipts(id) {
        return this.request(AppConfig.baseChatURL + `/api/v1/chat.getMessageReadReceipts?messageId=${id}`);
    }
    updatePushKey(key) {
        let options = {
            method: 'POST',
            headers: this.headers,
        };
        return getInternetCredentials().then(credential => {
            if (credential.username && credential.password) {
                const userInfo = JSON.parse(credential.username);
                options.headers['X-Auth-Token'] = userInfo.authToken;
                options.headers['X-User-Id'] = userInfo.userId;
                const data = JSON.stringify({"type": "apn", "value": key, "appName": "AuLac"});
                options.data = data
            }
            return sendRequest(AppConfig.baseChatURL + `/api/v1/push.token`, options).then(json => console.log('updatePushKey', json)).catch(err => {
                throw err;
            });
        });
    }
    getEmojis() {
        return this.request(AppConfig.baseChatURL + `/api/v1/emoji-custom.list?count=1000`);
    }

    getLivechatVisitor(token) {
        return this.request(AppConfig.baseChatURL + `/api/v1/livechat/visitor/${token}`);
    }
    registerLivechatVisitor(info) {
        const options = {
            method: 'POST',
            headers: this.headers,
            data: JSON.stringify(info)
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/livechat/visitor`, options);
    }
    getLivechatDepartment() {
        return this.request(AppConfig.baseChatURL + `/api/v1/livechat/department`);
    }
    getLivechatList() {
        return this.request(AppConfig.baseChatURL + `/api/v1/livechat/rooms?open=true&count=1`);
    }
    getLivechatRoom(info) {
        let url = AppConfig.baseChatURL + `/api/v1/livechat/room?token=${info.token}`;
        if (info.rid) {
            url += `&rid=${info.rid}`;
        }
        return this.request(url);
    }
    assignLivechatDepartment(info){
        const options = {
            method: 'POST',
            headers: this.headers,
            data: JSON.stringify(info)
        };
        return this.request(AppConfig.baseChatURL + `/api/v1/livechat/room.transfer`, options);
    }
    getLivechatMessages(params) {
        const { page, rid, token } = params;
        const offset = (page - 1) * 100;
        return this.request(AppConfig.baseChatURL + `/api/v1/livechat/messages.history/${rid}?token=${token}&count=100&offset=${offset}&sort={"ts":1}`);
    }
    sendLivechatMessage(info) {
        const data = JSON.stringify(info);
        const options = {
            method: 'POST',
            headers: this.headers,
            data
        };
        const url = info.rid ? `/api/v1/livechat/message` : `/api/v1/livechat/offline.message`;
        return this.request(AppConfig.baseChatURL + url, options);
    }
}

const chatapi = new ChatApi();

export default chatapi;
