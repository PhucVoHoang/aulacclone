import {createApolloFetch} from 'apollo-fetch';
import _ from 'lodash';
import {getUserCredentials} from './storageServices';

const dev = false;
let REACT_APP_API_URL = 'https://pwa.demo.saleor.rocks/graphql/';
if (!dev) {
  REACT_APP_API_URL = 'http://103.221.221.146:8000/graphql/';
}

export const privateRequest = info => {
  const apolloFetch = createApolloFetch({uri: REACT_APP_API_URL});

  apolloFetch.useAfter(({response}, next) => {
    next();
  });
  return getUserCredentials().then(credential => {
    apolloFetch.use(({request, options}, next) => {
      if (!options.headers) {
        options.headers = {}; // Create the headers object if needed.
      }
      options.headers.authorization = 'JWT ' + credential.password;
      next();
    });
    return apolloFetch(info)
      .then(data => {
        if (!_.isEmpty(_.get(data, 'errors'))) {
          throw data;
        }
        return data;
      })
      .catch(err => {
        throw err;
      });
  });
};

export const publicRequest = info => {
  const apolloFetch = createApolloFetch({uri: REACT_APP_API_URL});

  return apolloFetch(info)
    .then(data => {
      if (!_.isEmpty(_.get(data, 'data.tokenCreate.errors'))) {
        throw data;
      }
      return data;
    })
    .catch(err => {
      throw err;
    });
};
