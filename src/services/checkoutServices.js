import {privateRequest} from './httpServices';

const checkoutCreateQuery = `
mutation CheckoutCreate($input: CheckoutCreateInput!) {
  checkoutCreate(input: $input) {
    created
    checkout {
      created
      id
      availableShippingMethods {
        id
        name
      }
      quantity
      lines {
        id
        variant {
          id
          name
          sku
        }
      }
      availablePaymentGateways {
        name
        config {
          field
          value
        }
      }
    }
    errors {
      message
      field
    }
  }
}
`;

export const checkoutCreate = (variables) =>
  privateRequest({
    query: checkoutCreateQuery,
    variables: variables,
});

const checkoutLinesDeleteQuery = `
mutation CheckoutLineDelete($checkoutId: ID!, $lineId: ID) {
  checkoutLineDelete(checkoutId: $checkoutId, lineId: $lineId) {
    checkout {
      created
      id
      availableShippingMethods {
        id
        name
      }
      lines {
        id
        variant {
          id
        }
      }
      availablePaymentGateways {
        name
        config {
          field
          value
        }
      }
    }
    errors {
      field
      message
    }
  }
}
`;

export const checkoutLinesDelete = (variables) =>
  privateRequest({
    query: checkoutLinesDeleteQuery,
    variables: variables,
});

const checkoutLinesAddQuery = `
mutation CheckoutLinesAdd($checkoutId: ID!, $lines: [CheckoutLineInput]!) {
  checkoutLinesAdd(checkoutId: $checkoutId, lines: $lines) {
    checkout {
      created
      id
      availableShippingMethods {
        id
        name
      }
      lines {
        id
        variant {
          id
        }
      }
      availablePaymentGateways {
        name
        config {
          field
          value
        }
      }
    }
    errors {
      field
      message
    }
  }
}
`;

export const checkoutLinesAdd = (variables) =>
  privateRequest({
    query: checkoutLinesAddQuery,
    variables: variables,
});

const checkoutLineUpdateQuery = `
mutation CheckoutLinesUpdate($checkoutId: ID!, $lines: [CheckoutLineInput]!) {
  checkoutLinesUpdate(checkoutId: $checkoutId, lines: $lines) {
    checkout {
      created
      id
      availableShippingMethods {
        id
        name
      }
      lines {
        id
        variant {
          id
        }
      }
      availablePaymentGateways {
        name
        config {
          field
          value
        }
      }
    }
    errors {
      field
      message
    }
  }
}
`;

export const checkoutLinesUpdate = (variables) =>
  privateRequest({
    query: checkoutLineUpdateQuery,
    variables: variables,
});

const checkoutCustomerAttachQuery = `
mutation CheckoutCustomerAttach($checkoutId: ID!, $customerId: ID!) {
  checkoutCustomerAttach(checkoutId: $checkoutId, customerId: $customerId) {
    errors {
      field
      message
    }
  }
}
`;

export const checkoutCustomerAttach = (variables) =>
  privateRequest({
    query: checkoutCustomerAttachQuery,
    variables: variables,
});

const checkoutPaymentQuery = `
mutation CheckoutPaymentCreate($checkoutId: ID!, $input: PaymentInput!) {
  checkoutPaymentCreate(checkoutId: $checkoutId, input: $input) {
    errors {
      field
      message
    }
  }
}
`;

export const checkoutAddPayment = (variables) =>
  privateRequest({
    query: checkoutPaymentQuery,
    variables: variables,
});

const checkoutCompleteQuery = `
mutation CheckoutComplete($checkoutId: ID!, $storeSource: Boolean) {
  checkoutComplete(checkoutId: $checkoutId, storeSource: $storeSource) {
    errors {
      field
      message
    }
    order {
      id
      user {
        firstName
        lastName
      }
    }
  }
}
`;

export const checkoutComplete = (variables) =>
  privateRequest({
    query: checkoutCompleteQuery,
    variables: variables,
});

const checkoutAddShippingMethodQuery = `
mutation CheckoutShippingMethodUpdate($checkoutId: ID, $shippingMethodId: ID!) {
  checkoutShippingMethodUpdate(checkoutId: $checkoutId, shippingMethodId: $shippingMethodId) {
    errors {
      field
      message
    }
  }
}
`;

export const checkoutAddShippingMethod = (variables) =>
  privateRequest({
    query: checkoutAddShippingMethodQuery,
    variables: variables,
  });
// mutation {
//   checkoutPaymentCreate(
//     checkoutId: "Q2hlY2tvdXQ6OTAwZDgzZDAtN2Y3Ni00N2ZhLWEwZDUtYzA1ZTM5MTA3NzZk"
//     input: {
//       gateway: "DUMMY"
//       token: "tokencc_bh_s3bjkh_24smq8_6c6zhq_w4v6b9_8vz"
//       billingAddress: {
//         firstName: "John"
//         lastName: "Doe"
//         streetAddress1: "1470  Pinewood Avenue"
//         city: "Michigan"
//         postalCode: "49855"
//         country: US
//         countryArea: "MI"
//       }
//       # amount: 25.99
//     }
//   ) {
//     payment {
//       id
//       chargeStatus
//     }
//     errors {
//       field
//       message
//     }
//   }
// }
