import {publicRequest} from './httpServices';

const userLogin = `
mutation CreateToken($email: String!, $password: String! ){
  tokenCreate(email: $email, password: $password) {
      token
      user {
        id
        email
        firstName
        lastName
        isStaff
        avatar {
          url
          alt
        }
        addresses {
          id
          firstName
          lastName
          companyName
          streetAddress1
          streetAddress2
          city
          cityArea
          postalCode
          country {
            code
            country
            vat {
              countryCode
              standardRate
              reducedRates {
                rate
                rateType
              }
            }
          }
          countryArea
          phone
        }
        permissions {
          code
          name
        }
      }
      errors {
        field
        message
      }
    }
  }
`;

export const login = variables =>
  publicRequest({
    query: userLogin,
    variables: variables,
  });

const accountRegister = `
  mutation AccountRegister($input: AccountRegisterInput!) {
    accountRegister(input: $input) {
      errors {
        field
        message
      }
      accountErrors {
        field
        message
        code
      }
    }
  }
`;

export const register = variables =>
  publicRequest({
    query: accountRegister,
    variables: variables,
  });
