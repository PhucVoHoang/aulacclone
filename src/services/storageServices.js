import * as Keychain from 'react-native-keychain';

export const saveUserCredentials = (userInfo, token) => {
  return Keychain.setGenericPassword(userInfo, token).then(function() {
    console.log('Credentials saved successfully!');
  });
};

export const setInternetCredentials = (userInfo, token) => {
  return Keychain.setInternetCredentials('Aulac.chat', userInfo, token).then(function() {
    console.log('Aulac.chat Credentials saved successfully!');
  });
};

export const getUserCredentials = () => {
  return Keychain.getGenericPassword()
    .then(function(credentials) {
      return credentials;
    })
    .catch(function(error) {
      console.log("Keychain couldn't be accessed! Maybe no value set?", error);
    });
};

export const getInternetCredentials = () => {
  return Keychain.getInternetCredentials('Aulac.chat')
    .then(function(credentials) {
      return credentials;
    })
    .catch(function(error) {
      console.log("Keychain couldn't be accessed! Maybe no value set?", error);
    });
};

export const resetCredentials = () => {
  Keychain.resetInternetCredentials('Aulac.chat');
  return Keychain.resetGenericPassword();
};
