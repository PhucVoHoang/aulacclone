import {privateRequest} from './httpServices';

const productDetail = `
pageInfo {
  hasNextPage
  hasPreviousPage
  startCursor
  endCursor
}
edges {
  cursor
  node {
    id
    name
    isAvailable
    productType {
      id
      name
    }
    category {
      id
      name
    }
    thumbnail {
      url
      alt
    }
    basePrice {
      amount
      currency
    }
    variants {
      name
      sku
      stockQuantity
      costPrice {
        currency
        amount
      }
    }
    updatedAt
  }
}
totalCount
`;

const productsQuery = `
query ($filter: ProductFilterInput, $sortBy: ProductOrder, $stockAvailability: StockAvailability, $before: String, $after: String, $first: Int, $last: Int) {
  products(first: $first, last: $last, filter: $filter, after: $after, before: $before, sortBy: $sortBy, stockAvailability: $stockAvailability) {
    ${productDetail}
  }  
}
`;

export const getProducts = variables =>
  privateRequest({
    query: productsQuery,
    variables: variables,
  });

const categoriesQuery = `
query ($filter: CategoryFilterInput, $level: Int, $before: String, $after: String, $first: Int, $last: Int) {
    categories(first: $first, last: $last, filter: $filter, after: $after, before: $before, level: $level) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          name
        }
      }
      totalCount
    }
  }
`;

export const getCategories = variables =>
  privateRequest({
    query: categoriesQuery,
    variables: variables,
  });

const productQuery = `
  query ($id: ID!) {
    product(id: $id) {
      id
      name
      pricing {
        onSale
        discount {
          net {
            amount
            currency
          }
        }
      }
      basePrice {
        amount
        currency
      }
      thumbnail(size: 1080) {
        url
        alt
      }
      description
      weight {
        unit
        value
      }
      variants {
        id
        sku
        name
        stockQuantity
      }
    }
  }
`;

export const getProduct = variables =>
  privateRequest({
    query: productQuery,
    variables: variables,
  });
