import { AsyncStorage, Alert } from 'react-native';
import axios from 'axios';
import qs from 'qs';
import _ from 'lodash';
import AppConfig from '../config/AppConfig';
import {getUserCredentials} from './storageServices';

function checkServerStatus(data) {
  if (data.status === 200) {
    return data.data;
  }
  const error = new Error(data.message || 'Network error');
  error.error_fields = data.error_fields;
  throw error;
}

function sendRequest(url, options) {
  return axios(url, options)
    .then(res => {
      if (__DEV__) {
        let { data } = res;
      }
      return checkServerStatus(res);
    })
    .catch(err => {
      console.log('!!! request error sendRequest: ', url, err);
      throw err;
    });
}

class Api {
  constructor() {
    this.headers = {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    this.request = this.request.bind(this);
    this.register = this.register.bind(this);
    this.getCategories = this.getCategories.bind(this);
  }

  setAccessToken(token) {
    this.access_token = token;
  }

  getAccessToken() {
    getUserCredentials().then(credential => {
      if (credential.username && credential.password) {
        this.access_token = credential.password;
      }
    });
  }

  request(url, options) {
    options = options || { method: 'GET' };
    options.headers = Object.assign({}, this.headers, options.headers || {});
    if (this.access_token) {
      options.headers.Authorization = `Bearer ${this.access_token}`;
    }
    return getUserCredentials().then(credential => {
      if (credential.username && credential.password) {
        options.headers.Authorization = `Bearer ${credential.password}`;
      }
      console.log(options);
      return sendRequest(url, options).then(json => json).catch(err => {
        throw err;
      });
    });
  }

  register(info) {
    const data = JSON.stringify(info);
    const options = {
      method: 'POST',
      headers: this.headers,
      data
    };
    const url = AppConfig.baseURL + '/api/customer/register?token=true';
    return axios(url, options)
      .then(res => {
        return checkServerStatus(res);
      })
      .catch(err => {
        console.log('!!! request error:', err);
        throw err;
      });
  }

  login(info) {
    const data = JSON.stringify(info);
    const options = {
      method: 'POST',
      headers: this.headers,
      data
    };
    const url = AppConfig.baseURL + '/api/customer/login?token=true';
    return axios(url, options)
      .then(res => {
        if (res.data) {
          this.access_token = res.data.token;
        }
        return checkServerStatus(res);
      })
      .catch(err => {
        console.log('!!! request error:', err);
        throw err;
      });
  }

  forgotpassword(params) {
      const options = {
        method: 'POST',
        data: JSON.stringify(params)
      };
      return this.request(AppConfig.baseURL + `/api/customer/forgot-password?token=true`, options);
  }

  updateProfile(params) {
    const options = {
      method: 'PUT',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/customer/profile?token=true`, options);
  }

  updateToken(params) {
    const options = {
      method: 'PUT',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/customer/pushtoken?token=true`, options);
  }

  getUserInfo(phone) {
    return this.request(AppConfig.baseURL + `/api/customer/getInfo/${phone}?token=true`);
  }

  sendChatNotification(params) {
    const options = {
      method: 'POST',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseECURL + `/aulacapi/auth/pushmessage`, options);
    // return this.request(AppConfig.baseURL + `/api/customer/pushmessage?token=true`, options);
  }

  getCategories() {
    return this.request(AppConfig.baseURL + '/api/categories?pagination=0&status=1&token=true');
  }

  getProducts(params) {
    const { page, category_id, isNew, isFeature, keyword, price_from, price_to } = params;
    let url = AppConfig.baseURL + `/api/products?page=${page}&limit=10`;
    if (category_id) {
      url += `&category_id=${category_id}`;
    }
    if (isNew) {
      url += `&custom_field_1=1`;
    }
    if (isFeature) {
      url += `&custom_field_2=1`;
    }
    if (keyword) {
      url += `&search=${keyword}`;
    }
    if (price_from && price_from !== '0') {
      url += `&price_from=${price_from}`;
    }
    if (price_to && price_to !== '0') {
      url += `&price_to=${price_to}`;
    }
    console.log('search', url)
    return this.request(url);
  }

  getProduct(id) {
    return this.request(AppConfig.baseURL + `/api/products/${id}?token=true`);
  }
  getCart() {
    console.log(AppConfig.baseURL + `/api/checkout/cart?token=true`);
    return this.request(AppConfig.baseURL + `/api/checkout/cart?token=true`);
  }
  addItemToCart(id, params) {
    const options = {
      method: 'POST',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/checkout/cart/add/${id}?token=true`, options);
  }

  updateItemToCart(params) {
    const options = {
      method: 'PUT',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/checkout/cart/update?token=true`, options);
  }
  removeItemToCart(id) {
    return this.request(AppConfig.baseURL + `/api/checkout/cart/remove-item/${id}?token=true`);
  }

  addItemToWishlist(id) {
    return this.request(AppConfig.baseURL + `/api/wishlist/add/${id}?token=true`);
  }

  getWishlist(params) {
    const { page, pagination } = params;
    let url = AppConfig.baseURL + `/api/wishlist?page=${page}&limit=10&token=true`;
    if(pagination == '0') {
      url = AppConfig.baseURL + `/api/wishlist?token=true`;
    }
    
    return getUserCredentials().then(credential => {
      if (credential.username && credential.password) {
        const userInfo = JSON.parse(credential.username);
        url += `&customer_id=${userInfo.id}`;
      }
      return this.request(url);
    });
  }

  saveAdress(params) {
    const options = {
      method: 'POST',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/checkout/save-address?token=true`, options);
  }
  getAdresses() {
    return this.request(AppConfig.baseURL + `/api/addresses?token=true&pagination=0`);
  }
  addAdress(params) {
    const options = {
      method: 'POST',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/addresses/create?token=true`, options);
  }
  saveShipping(params) {
    const options = {
      method: 'POST',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/checkout/save-shipping?token=true`, options);
  }
  savePayment(params) {
    const options = {
      method: 'POST',
      data: JSON.stringify(params)
    };
    return this.request(AppConfig.baseURL + `/api/checkout/save-payment?token=true`, options);
  }
  saveOrder() {
    const options = {
      method: 'POST'
    };
    return this.request(AppConfig.baseURL + `/api/checkout/save-order?token=true`, options);
  }
  getOrders(params) {
    const { page, catId } = params;
    let status = 'processing,pending';
    if (catId !== '0') {
      status = 'completed,canceled,closed';
    }
    
    let url = AppConfig.baseURL + `/api/orders?page=${page}&limit=10&token=true&status=${status}`;
    return getUserCredentials().then(credential => {
      if (credential.username && credential.password) {
        const userInfo = JSON.parse(credential.username);
        url += `&customer_id=${userInfo.id}`;
      }
      return this.request(url);
    });
  }
  getOrder(id) {
    return this.request(AppConfig.baseURL + `/api/orders/${id}?token=true`);
  }
  cancelOrder(id) {
    const options = {
      method: 'POST',
    };
    return this.request(AppConfig.baseURL + `/api/orders/cancel/${id}?token=true`, options);
  }
  getStores(params) {
    const { page, city, district } = params;
    let url = AppConfig.baseECURL + `/api/business-locations?page=${page}&limit=100`;

    return this.request(url);;
  }

  getBlogPosts(params) {
    const { page, catId } = params;
    let url = AppConfig.baseECURL + `/api/blog/posts?page=${page}&category_id=${catId}`;

    return this.request(url);;
  }
  getBlogPost(id) {
    return this.request(AppConfig.baseECURL + `/api/blog/posts/${id}?token=true`);
  }

  addCouponFromCart(coupon) {
    const options = {
      method: 'POST',
      data: JSON.stringify({ code: coupon })
    };
    return this.request(AppConfig.baseURL + `/api/checkout/cart/coupon?token=true`, options);
  }
  
 removeCouponFromCart(coupon) {
      const options = {
        method: 'DELETE',
        data: JSON.stringify({ code: coupon })
      };
      return this.request(AppConfig.baseURL + `/api/checkout/cart/coupon?token=true`, options);
    }
  
}

const api = new Api();

export default api;
