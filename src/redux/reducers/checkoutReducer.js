import _ from 'lodash';

const initialState = {
  checkout: {},
  address: null,
  shippingMethod: null,
  paymentMethod: null,
  addresses: []
};
const checkoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SAVE_PAYMENT': {
      return {
        ...state,
        paymentMethod: action.paymentMethod,
      };
    }
    case 'SAVE_SHIPPING': {
      return {
        ...state,
        shippingMethod: action.shippingMethod,
      };
    }
    case 'SAVE_ADDRESS': {
      return {
        ...state,
        address: action.address,
      };
    }
    case 'GET_ADDRESSES': {
      return {
        ...state,
        addresses: action.addresses,
      };
    }
    case 'CREATE_CHECKOUT': {
      return {
        ...state,
        checkout: action.checkoutInfo,
      };
    }
    case 'RESET_CHECKOUT': {
      return {
        ...state,
        checkout: initialState.checkout,
      }
    }
    default: {
      return state;
    }
  }
};
// Exports
export default checkoutReducer;
