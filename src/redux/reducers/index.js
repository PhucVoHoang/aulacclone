import { combineReducers } from 'redux';
// Imports: Reducers
import cartReducer from './cartReducer';
import checkoutReducer from './checkoutReducer';
import authReducer from './authReducer';
import productReducer from './productReducer';
import blogReducer from './blogReducer';
import chatReducer from './chatReducer';

/* // Redux: Root Reducer
const rootReducer = combineReducers({
  cartReducer: cartReducer,
  checkoutReducer: checkoutReducer,
});

 */
const appReducer = combineReducers({
  cartReducer: cartReducer,
  checkoutReducer: checkoutReducer,
  authReducer: authReducer,
  productReducer: productReducer,
  blogReducer: blogReducer,
  chatReducer: chatReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    state = undefined;
  }
  return appReducer(state, action);
};

// Exports
export default rootReducer;
