import _ from 'lodash';

const initialState = {
    info: null,
    access_token: null,
};
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REGISTER': {
      return {
        ...state,
        info: action.info,
        access_token: action.token,
      };
    }
    case 'LOGIN': {
        return {
          ...state,
          info: action.info,
          access_token: action.token,
        };
      }
    case 'LOGOUT': {
      return {
        ...state,
        info: initialState.info,
        access_token: initialState.access_token,
      }
    }
    default: {
      return state;
    }
  }
};
// Exports
export default authReducer;
