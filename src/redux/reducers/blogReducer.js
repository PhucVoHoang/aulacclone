import _ from 'lodash';

const initialState = {
    blog: null,
    blog_list: [],
};
const blogReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'BLOG_LIST': {
      return {
        ...state,
        blog_list: action.blog_list,
      };
    }
    case 'BLOG_VIEW': {
        return {
          ...state,
          blog: action.blog,
        };
      }
    
    default: {
      return state;
    }
  }
};
// Exports
export default blogReducer;
