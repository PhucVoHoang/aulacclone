import _ from 'lodash';

// Initial State
let initialState = {
  cartItems: [],
  recentItems: [],
  cartDetail: null
};
// Reducers (Modifies The State And Returns A New State)
const cartReducer = (state = initialState , action) => {
  switch (action.type) {
    case 'REPLACE_CART': {
      return {
        ...state,
        cartDetail: action.cartDetail,
      };
    }
    case 'ADD_TO_CART': {
      return {
        ...state,
        cartItems: [...state.cartItems, action.product],
        cartDetail: action.cartDetail,
        recentItems: state.recentItems
      };
    }
    case 'ADD_TO_RECENT': {
      return {
        ...state,
        recentItems: [action.product, ...state.recentItems],
      };
    }
    case 'REMOVE_FROM_CART': {
      return {
        ...state,
        cartItems: state.cartItems.filter(item => item.id !== action.productId),
        cartDetail: action.cartDetail,
        recentItems: state.recentItems
      }
    }
    case 'UPDATE_CART': {
      return {
        ...state,
        cartItems: state.cartItems.map(item => {
          if (item.id === action.productId) {
            item.qty = action.quantity
          }
          return item;
        }),
        cartDetail: action.cartDetail,
        recentItems: state.recentItems
      }
    }
    case 'RESET_CART': {
      return {
        ...state,
        cartItems: initialState.cartItems,
        cartDetail: initialState.cartDetail,
        recentItems: initialState.recentItems
      }
    }
    default: {
      return state;
    }
  }
};
// Exports
export default cartReducer;
