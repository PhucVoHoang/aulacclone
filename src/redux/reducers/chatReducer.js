import _ from 'lodash';

const initialState = {
    info: null,
    chat_token: null,
    chat_info: null,
    chats: [],
    push: false
};
const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REGISTER_VISTOR': {
      return {
        ...state,
        info: action.info,
        chat_token: action.chat_token,
      };
    }
    case 'GET_CHATROOM': {
      return {
        ...state,
        chat_info: action.info,
      }
    }
    case 'GET_CHATROOMS': {
        return {
          ...state,
          chats: action.data,
        }
      }
    case 'ADD_MESSAGE': {
        return {
          ...state,
          data: action.data,
        }
      }
    case 'PUSH_MESSAGE': {
      console.log('PUSH_MESSAGE',action)
      return {
        ...state,
        push: action.data,
      }
    }
    default: {
      return state;
    }
  }
};
// Exports
export default chatReducer;
