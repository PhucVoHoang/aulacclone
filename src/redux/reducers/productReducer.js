import _ from 'lodash';

const initialState = {
    product: null,
    product_list: [],
    category: null,
    category_list: []
};
const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'PRODUCT_LIST': {
      return {
        ...state,
        product_list: action.product_list,
      };
    }
    case 'PRODUCT_VIEW': {
        return {
          ...state,
          product: action.product,
        };
      }
    case 'CATEGORY_LIST': {
      return {
        ...state,
        category_list: action.categoryItems,
      }
    }
    case 'CATEGORY_VIEW': {
        return {
          ...state,
          category: action.category,
        }
      }
    default: {
      return state;
    }
  }
};
// Exports
export default productReducer;
