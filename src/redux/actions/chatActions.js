export const registerChatAction = (credentials) => ({
    type: 'REGISTER_VISTOR',
    credentials: credentials,
});

export const getChatRoomAction = (info) => ({
    type: 'GET_CHATROOM',
    info,
});

export const getChatRoomsAction = (data) => ({
    type: 'GET_CHATROOMS',
    data,
});

export const addMessageAction = (data) => ({
    type: 'ADD_MESSAGE',
    data,
});

export const pushMessageAction = (data) => ({
    type: 'PUSH_MESSAGE',
    data,
});