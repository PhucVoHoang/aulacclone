export const createCheckout = (checkoutInfo) => ({
  type: 'CREATE_CHECKOUT',
  checkoutInfo: checkoutInfo,
});

export const resetCheckout = () => ({
  type: 'RESET_CHECKOUT',
});

export const saveAddress = (address) => ({
  type: 'SAVE_ADDRESS',
  address: address,
});
export const getAddresses = (addresses) => ({
  type: 'GET_ADDRESSES',
  addresses,
});

export const saveShipping = (shippingMethod) => ({
  type: 'SAVE_SHIPPING',
  shippingMethod: shippingMethod,
});

export const savePayment = (paymentMethod) => ({
  type: 'SAVE_PAYMENT',
  paymentMethod: paymentMethod,
});