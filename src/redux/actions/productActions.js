export const productListAction = (filters) => ({
    type: 'PRODUCT_LIST',
    filters: filters,
});

export const productViewAction = (id) => ({
    type: 'PRODUCT_VIEW',
    id,
});

export const categoryListAction = (data) => ({
    type: 'CATEGORY_LIST',
    categoryItems: data,
});

export const categoryViewAction = (id) => ({
    type: 'CATEGORY_VIEW',
    id,
});