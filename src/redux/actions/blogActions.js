export const blogListAction = (data) => ({
    type: 'BLOG_LIST',
    blog_list: data,
});

export const blogViewAction = (id) => ({
    type: 'BLOG_VIEW',
    id,
});
