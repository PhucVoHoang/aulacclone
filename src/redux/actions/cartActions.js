// Add to cart
export const addToCart = (product, cartDetail) => ({
  type: 'ADD_TO_CART',
  product: product,
  cartDetail: cartDetail
});

export const addToRecent = (product) => ({
  type: 'ADD_TO_RECENT',
  product: product
});

export const removeFromCart = (productId, cartDetail) => ({
  type: 'REMOVE_FROM_CART',
  productId: productId,
  cartDetail: cartDetail
});

export const replaceCart = (cartDetail) => ({
  type: 'REPLACE_CART',
  cartDetail: cartDetail
});

export const updateCart = (productId, quantity, cartDetail) => ({
  type: 'UPDATE_CART',
  productId: productId,
  quantity: quantity,
  cartDetail: cartDetail
});

export const resetCart = () => ({
  type: 'RESET_CART',
});

