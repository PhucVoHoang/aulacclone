export const registerAction = (credentials) => ({
    type: 'REGISTER',
    credentials: credentials,
});

export const loginAction = (credentials) => ({
    type: 'LOGIN',
    credentials: credentials,
});

export const logoutAction = () => ({
    type: 'LOGOUT',
});