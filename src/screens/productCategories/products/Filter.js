import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import FilterHeader from '../../../components/navigation/filterHeader';
import {Link} from 'react-router-native';
import {TextInputMask} from 'react-native-masked-text';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class Filter extends Component {
  state = {
    giavi: true,
    anlien: false,
    trung: false,
    lon: false,
    lanh: false,
    kho: false,
    khat: false,
    nam: true,
    raucu: false,
    dau: false,
    cacrau: false,
    caccu: false,
    cacdau: false,
    catId: 0,
    price_from: '0',
    price_to: '0',
  };

  _reset() {
    this.setState({
      giavi: true,
      anlien: false,
      trung: false,
      lon: false,
      lanh: false,
      kho: false,
      khat: false,
      nam: true,
      raucu: false,
      dau: false,
      cacrau: false,
      caccu: false,
      cacdau: false,
      catId: 0,
      price_from: '0',
      price_to: '0',
    });
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <FilterHeader to={this.props.location.state.go} title="FILTER" reset={() => {this._reset()}}/>
        </View>
        <View
          style={{
            flex: 9,
            marginLeft: 0.04 * screenWidth,
            marginRight: 0.04 * screenWidth,
          }}>
          <Text style={style.headerText}>Danh mục sản phẩm</Text>
          <View
            style={{
              width: '100%',
            }}>
            <View style={style.containerView}>
            {this.props.categoryItems.map(item => (
              <TouchableOpacity
                style={
                  (this.state.catId == item.id) ? style.touchableViewOn : style.touchableView
                }
                onPress={() => {
                  this.setState({
                    catId: item.id
                  });
                }}>
                <Text style={(this.state.catId == item.id) ? style.textOn : style.textOff}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            ))}
              
            </View>
            
          </View>
          <Text style={style.headerText}>Khoảng giá</Text>
          <View
            style={{
              marginTop: 0.015 * screenHeight,
              height: 0.0679 * screenHeight,
              width: '100%',
              backgroundColor: 'rgb(236,236,236)',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 0.415 * screenWidth,
                height: 0.04 * screenHeight,
                borderRadius: 5,
                backgroundColor: 'white',
                alignItems: 'flex-start',
              }}>
              <TextInputMask
                type={'money'}
                options={{
                  precision: 0,
                  separator: ',',
                  delimiter: '.',
                  unit: '₫',
                  suffixUnit: ''
                }}
                placeholder="Tối thiểu"
                placeholderTextColor="rgb(135,132,130)"
                style={{
                  flex: 1,
                  color: 'black',
                  borderRadius: 5,
                  paddingTop: 0,
                  paddingBottom: 0,
                  marginLeft: 5,
                  width: '100%'
                }}
                value={this.state.price_from}
                onChangeText={(text) => this.setState({ price_from: text })}
              />
            </View>
            <View
              style={{
                width: 0.415 * screenWidth,
                height: 0.04 * screenHeight,
                borderRadius: 5,
                backgroundColor: 'white',
                alignItems: 'flex-start',
              }}>
              <TextInputMask
                type={'money'}
                options={{
                  precision: 0,
                  separator: ',',
                  delimiter: '.',
                  unit: '₫',
                  suffixUnit: ''
                }}
                placeholder="Tối đa"
                placeholderTextColor="rgb(135,132,130)"
                style={{
                  flex: 1,
                  color: 'black',
                  borderRadius: 5,
                  paddingTop: 0,
                  paddingBottom: 0,
                  marginLeft: 5,
                  width: '100%'
                }}
                value={this.state.price_to}
                onChangeText={(text) => this.setState({ price_to: text })}
              />
            </View>
          </View>
          <View
            style={{
              marginTop: 0.015 * screenHeight,
              height: 0.0326 * screenHeight,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              onPress={() => this.setState({ price_from: '0', price_to: '100000' })}
              style={{
                width: '22.22%',
                height: 0.0326 * screenHeight,
                backgroundColor: 'rgb(236,236,236)',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 5,
              }}
            >
              <Text>₫0 - ₫100k</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.setState({ price_from: '100000', price_to: '200000' })}
              style={{
                width: '22.22%',
                height: 0.0326 * screenHeight,
                backgroundColor: 'rgb(236,236,236)',
                marginLeft: 0.02 * screenWidth,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 5,
                flexGrow: 1,
              }}
            >
              <Text>₫100k - ₫200k</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.setState({ price_from: '200000', price_to: '300000' })}
              style={{
                width: '22.22%',
                height: 0.0326 * screenHeight,
                backgroundColor: 'rgb(236,236,236)',
                marginLeft: 0.02 * screenWidth,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 5,
                flexGrow: 1,
              }}
            >
              <Text>₫200k - ₫300k</Text>
            </TouchableOpacity>
          </View>
          {/* <View
            style={{
              flexDirection: 'row',
              width: '75%',
              justifyContent: 'space-between',
            }}>
            <Text style={style.headerText}>Đánh giá</Text>
            <Image
              style={{
                width: '40%',
                height: 0.0285 * screenHeight,
                marginTop: 0.0285 * screenHeight,
              }}
              resizeMode="stretch"
              source={require('../../../../assets/images/stars.png')}
            />
          </View> */}
          {/* <Text style={style.headerText}>Nguyên liệu</Text> */}
          {/* <View
            style={{
              width: '100%',
              height: 0.15 * screenHeight,
              flexDirection: 'column',
            }}>
            <View style={style.containerView}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    nam: true,
                    raucu: false,
                    dau: false,
                    cacrau: false,
                    caccu: false,
                    cacdau: false,
                  });
                }}
                style={
                  this.state.nam ? style.touchableViewOn : style.touchableView
                }>
                <Text style={this.state.nam ? style.textOn : style.textOff}>
                  Nấm
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    nam: false,
                    raucu: true,
                    dau: false,
                    cacrau: false,
                    caccu: false,
                    cacdau: false,
                  });
                }}
                style={
                  this.state.raucu ? style.touchableViewOn : style.touchableView
                }>
                <Text style={this.state.raucu ? style.textOn : style.textOff}>
                  Rau củ
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    nam: false,
                    raucu: false,
                    dau: true,
                    cacrau: false,
                    caccu: false,
                    cacdau: false,
                  });
                }}
                style={
                  this.state.dau ? style.touchableViewOn : style.touchableView
                }>
                <Text style={this.state.dau ? style.textOn : style.textOff}>
                  Đậu hủ
                </Text>
              </TouchableOpacity>
            </View> 
            <View style={style.containerView}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    nam: false,
                    raucu: false,
                    dau: false,
                    cacrau: true,
                    caccu: false,
                    cacdau: false,
                  });
                }}
                style={
                  this.state.cacrau
                    ? style.touchableViewOn
                    : style.touchableView
                }>
                <Text style={this.state.cacrau ? style.textOn : style.textOff}>
                  Các loại rau
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    nam: false,
                    raucu: false,
                    dau: false,
                    cacrau: false,
                    caccu: true,
                    cacdau: false,
                  });
                }}
                style={
                  this.state.caccu ? style.touchableViewOn : style.touchableView
                }>
                <Text style={this.state.caccu ? style.textOn : style.textOff}>
                  Các loai củ
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    nam: false,
                    raucu: false,
                    dau: false,
                    cacrau: false,
                    caccu: false,
                    cacdau: true,
                  });
                }}
                style={
                  this.state.cacdau
                    ? style.touchableViewOn
                    : style.touchableView
                }>
                <Text style={this.state.cacdau ? style.textOn : style.textOff}>
                  Các loại đậu
                </Text>
              </TouchableOpacity>
            </View>
          </View> */}
          <Link component={TouchableOpacity} 
            to={{
              pathname: '/products/filter/filterresults',
              state: { filter: this.state },
            }}
            style={{
              marginTop: 0.0434 * screenHeight,
              width: '100%',
              height: 0.076 * screenHeight,
              backgroundColor: 'rgb(255,203,8)',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: 'white',
                fontFamily: 'Roboto-BoldCondensed',
                fontSize: 19,
              }}>
              LỌC KẾT QUẢ
            </Text>
          </Link>
        </View>
      </View>
    );
  }
}
const style = {
  containerView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginTop: 0.015 * screenHeight,
  },
  touchableView: {
    width: '30%',
    height: 0.056 * screenHeight,
    backgroundColor: 'rgb(236,236,236)',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  touchableViewOn: {
    width: '30%',
    height: 0.056 * screenHeight,
    backgroundColor: 'rgb(255,203,8)',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  headerText: {
    marginTop: 0.0285 * screenHeight,
    fontFamily: 'Roboto-BoldCondensed',
    color: 'rgb(109,109,109)',
    fontSize: 16,
  },
  textOn: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: 'white',
  },
  textOff: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: 'rgb(109,109,109)',
  },
};
const mapStateToProps = (state) => {
  return {
    categoryItems: state.productReducer.category_list
  };
};
export default connect(mapStateToProps, null)(Filter);