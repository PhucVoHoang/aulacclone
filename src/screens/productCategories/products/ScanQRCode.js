import React, { Component } from 'react';
 
import {
  StyleSheet,
  View,
  Dimensions
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import NavigationHeader from '../../../components/navigation/navigationHeader';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class ScanQRCode extends Component {
  onSuccess = e => {
    this.props.history.push({
        pathname: '/products',
        state: {
            going: '/',
            productId: e.data
        },
    })
  };
 
  render() {
    return (
     <View style={{flex: 1}}>
         <View style={{flex: 1.1}}>
                <NavigationHeader title="SCAN QRCODE" to={this.props.location.state.go} back={true} />
            </View>
        <View
            style={{
              flex: 8.9,
              width: screenWidth,
            }}>
        <QRCodeScanner
            containerStyle={styles.containerStyle}
            onRead={this.onSuccess}
            cameraStyle={styles.cameraContainer}
            cameraProps={{captureAudio: false}}
            showMarker={true}
        />
        </View>
      </View>
    );
  }
}

export default ScanQRCode;
 
const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 16,
    color: 'black',
    textAlign: 'center'
  },
  buttonTouchable: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    width: '40%',
    backgroundColor: '#ECB22D',
    borderRadius: 40,
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  cameraContainer: {
    height: Dimensions.get('window').height,
    flex: 8.9, flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor:'black'
  }
});