import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  FlatList,
  ScrollView,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';

import Header from '../../../components/navigation/Header';
import Footer from '../../../components/navigation/Footer';
import CategoryToggle from '../../../components/navigation/categoryToggle';
import {RFValue} from 'react-native-responsive-fontsize';
import ProductBlock from '../../../components/landing/productBlock';
import {Link} from 'react-router-native';
import NavigationHeader from '../../../components/navigation/navigationHeader';
import API from '../../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const b5 = require('../../../../assets/images/Landing/b5.png');
const b6 = require('../../../../assets/images/Landing/b6.png');
const b3 = require('../../../../assets/images/Landing/b3.png');
const b4 = require('../../../../assets/images/Landing/b4.png');

function Item(props) {
  return (
    <Link
      component={TouchableOpacity}
      to={{
        pathname: '/products',
        state: {
          going: props.gone,
          productId: props.item.id,
          filter: props.filter,
        },
      }}
      style={{
        flex: 1,
        margin: 15,
        width: props.width * screenWidth,
        height: props.height * screenHeight,
      }}>
        <View style={{flex: 25}}>
        <Image
          style={{
            width: undefined,
            height: undefined,
            flex: 1,
          }}
          source={{uri: props.item.image_url}}
        />
      </View>
      <View style={{flex: 14, justifyContent: 'space-around'}}>
        <Text
          style={{
            color: 'rgb(81,81,81)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
          }}>
          {props.item.name}
        </Text>
        <Text
          style={{
            color: 'rgb(255,30,2)',
            textAlign: 'left',
            fontFamily: 'Roboto-Bold',
          }}>
          {props.item.formated_special_price ? props.item.formated_special_price : props.item.regular_formated_price}
        </Text>
        {props.item.formated_special_price && (<Text
          style={{
            color: 'rgb(160,158,156)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
            textDecorationLine: 'line-through',
          }}>
          {props.item.regular_formated_price}
        </Text>)}
      </View>
    </Link>
  );
}

class filterResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
      loading: true,
      loadingMore: false,
      filtering: false,
      refreshing: false,
      error: null,
      isFlushed: false,
      keyword: null
    };
  }
  componentDidMount() {
    this._fetchAllProducts();
  }
  _fetchAllProducts = () => {
    const { page } = this.state;
    const { catId, price_from, price_to } = this.props.location.state && this.props.location.state.filter ? this.props.location.state.filter : { };
    let postParam = {
      page
    };
    if (catId && catId != 0) {
      if(catId == 'new') {
        postParam['isNew'] = true;
      } else if (catId == 'featured') {
        postParam['isFeature'] = true;
      } else {
        postParam['category_id'] = catId;
      }
    }
    if(price_from && price_to) {
      postParam['price_from'] = price_from;
      postParam['price_to'] = price_to;
    }

    API.getProducts(postParam).then(response => {
      this.setState((prevState, nextProps) => ({
        data:
          page === 1
            ? Array.from(response.data)
            : [...this.state.data, ...response.data],
        loading: false,
        loadingMore: false,
        refreshing: false,
        isFlushed: false
      }));
    })
    .catch(error => {
      this.setState({ error, loading: false });
    });
  };
  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this._fetchAllProducts();
      }
    );
  };
  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        this._fetchAllProducts();
      }
    );
  };

  UNSAFE_componentWillReceiveProps(nextProps){
    if (!this.state.isFlushed && nextProps.location.state === 'flushData') {
      this.setState({
        isFlushed: true,
        loading: true,
      }, () => this._handleRefresh());
    }
  }
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
            <NavigationHeader title="Filter Results" to="/categories/0" back={true} />
        </View>
        <View style={{flex: 8}}>
          <View style={{flex: 1}}>
          {!this.state.loading ? (
              <FlatList
                contentContainerStyle={{
                  flexGrow: 1,
                }}
                numColumns={2}
                data={this.state.data}
                renderItem={({ item }) => (
                  <Item item={item} width={0.42} height={0.27} gone={'/products/filter/filterresults'} filter={this.props.location.state && this.props.location.state.filter ? this.props.location.state.filter : null}/>
                )}
                keyExtractor={item => item.id.toString()}
                onRefresh={this._handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this._handleLoadMore}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
              />
            ) : (
              <View
                style={{flex: 1,
                justifyContent: 'center',
                alignItems:'center'
              }}>
                <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

export default filterResults;
