import React, {Component} from 'react';
import _ from 'lodash';
import {
  View,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import { SafeAreaInsetsContext } from 'react-native-safe-area-context';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import HTML from 'react-native-render-html';

import {addToCart, updateCart, removeFromCart, resetCart, addToRecent} from '../../../redux/actions/cartActions';
import {resetCheckout} from '../../../redux/actions/checkoutActions';
import NavigationProducts from '../../../components/navigation/navigationProducts';
import {RFValue} from 'react-native-responsive-fontsize';
import Description from '../../../components/products/Description';
import Specifications from '../../../components/products/Specifications';
import Recipe from '../../../components/products/Recipe';
import RecentView from './RecentView';
import RelatedView from './RelatedView';
import {Link} from 'react-router-native';
import API from '../../../services/api';
import Icon from 'react-native-vector-icons/AntDesign';
import WebView from "react-native-webview";

import {resetCredentials} from '../../../services/storageServices';
import PopUp from '../../../components/authentication/PopupDialog';
const wrong_img = require('../../../../assets/images/wrong.png');
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const initialState = {
  productLoading: false,
  productDetail: {},
  qty: 1,
  description: true,
  specifications: false,
  recipe: false,
  isWishlisted: 'rgb(204, 204, 204)',
  wishlistItems: [],
  scrollViewWidth: 0,
  scrollViewHeight: 0,
  images: [],
  activeSlide: 0,
  articles: [],
  errorPopup: false
};

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.myscroll = React.createRef();
    this.logout = this.logout.bind(this);
    this.loadArticle = this.loadArticle.bind(this);
  }

  _renderSliderItem = ({item, index}) => {
    {console.log(item)}
    if(item && item.indexOf('youtube')>0) {
      return (
        <View style={{ flex: 1, width: '100%', height: '100%' }}>
          {item && (
              <WebView
                style={{ width: screenWidth, height: 0.25 * screenHeight }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: item && item.replace('watch?v=', 'embed/')}}
              />
            )}
        </View>
      );
    } else {
      return (
        <Image
          style={{
            width: undefined,
            height: undefined,
            resizeMode: 'contain',
            flex: 1
          }}
          source={item && item ? {uri: item} : null} 
        />
      );
    }
  }
  get pagination () {
      const { images, activeSlide } = this.state;
      return (
          <Pagination
            dotsLength={images.length}
            activeDotIndex={activeSlide}
            dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 8,
                backgroundColor: 'rgba(0, 0, 0, 0.92)'
            }}
            inactiveDotStyle={{
                // Define styles for inactive dots here
            }}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
          />
      );
  }
  
  componentDidMount() {
    this.loadProduct(_.get(this.props, 'location.state.productId'));
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if(_.get(this.props, 'location.state.productId') !== _.get(nextProps, 'location.state.productId')) {

      this.setState(initialState);
      this.loadProduct(_.get(nextProps, 'location.state.productId'));
      this.myscroll.scrollTo({x: 0, y: 0, animated: true});
    }
  }
  loadArticle(id) {
    if(id > 0) {
      API.getBlogPost(id).then(res => {
        console.log('loadArticle',res)
        this.setState({ articles: [res] });
      })
    }
  }
  loadProduct(productId) {
    this.setState({
      productLoading: true,
    })
    if (productId) {
      const productInStore = _.find(this.props.cartItems, ['id', productId]);
      if (productInStore) {
        this.loadArticle(productInStore.custom_field_5);
        const productInRecent = _.find(this.props.recentItems, ['id', productId]);
        if (!productInRecent) {
          this.props.reduxAddToRecent(productInStore);
        }
        this.setState({
          productDetail: productInStore,
          productLoading: false,
          qty: productInStore.qty,
        })
      } else {
        API.getProduct(productId).then(res => {
          let images = [res.data.image_url];
          if (res.data.custom_field_3 !== '') {
            images = [res.data.custom_field_3, ...images]
          }
          if (res.data.variation_images) {
            images = [...images, ...JSON.parse(res.data.variation_images)]
          }
          this.loadArticle(_.get(res, 'data.custom_field_5', 0));
          try{
            const productInRecent = _.find(this.props.recentItems, ['id', productId]);
            if (!productInRecent) {
              this.props.reduxAddToRecent(_.get(res, 'data', {}));
            }
            
          } catch(error) {
            console.log('error', error);
          }
          
          this.setState({
            productDetail: _.get(res, 'data', {}),
            images,
            productLoading: false,
          })
        }).catch(err => {
          console.log(err);
        })
      }
      API.getWishlist({pagination: 0}).then(res => {
        const item = _.find(_.get(res, 'data', []), function(o) { return o.product.id == productId; });
        if (item) {
          this.setState({
            isWishlisted: 'rgb(255,30,2)',
            wishlistItems: _.get(res, 'data', [])
          })
        }
      }).catch(err => {
        console.log(err);
      })
    }
  }
  logout = () => {
    this.props.reduxResetCart();
    this.props.reduxResetCheckout();
    resetCredentials();
    this.props.history.push('/');
  }
  onBuyNow = () => {
    this.addToCart(true);
  }
  addToCart = (redirect) => {
    const productId = _.get(this.props, 'location.state.productId');
    console.log(this.props)
    if (_.find(this.props.cartItems, ['id', productId])) {
      API.updateItemToCart({
        qty: { [productId]: this.state.qty }
      }).then(res => {
        this.props.reduxUpdateCart(productId, this.state.qty, _.get(res, 'data'));
        if (redirect) {
          this.props.history.push({
            pathname: '/shopping/cart',
            state: {go: this.props.location.state.going},
          })
        }
      }).catch(err => this.logout())
    } else {
      API.addItemToCart(productId, {
        quantity: this.state.qty,
        product_id: productId
      }).then(res => {
        this.props.reduxAddToCart({...this.state.productDetail, qty: this.state.qty}, _.get(res, 'data'));
        if (redirect) {
          this.props.history.push({
            pathname: '/shopping/cart',
            state: {go: this.props.location.state.going},
          })
        }
      }).catch(err => {
        console.log('addItemToCart',err);
        //this.logout();
      })
    }
  }
  addToWishList = () => {
    const productId = _.get(this.props, 'location.state.productId');
    
    API.addItemToWishlist(productId);
    if (this.state.isWishlisted === 'rgb(204, 204, 204)' ) {
      this.setState({isWishlisted: 'rgb(255,30,2)'})
    } else {
      this.setState({isWishlisted: 'rgb(204, 204, 204)'})
    }
  }
  
  _renderContent(insets) {
    const contentHeight = screenHeight - 120;
    const paddingBottom = insets.bottom;
    if (!this.state.productLoading) {
      return (
        <View style={{flex: 1, paddingBottom: paddingBottom}}>
          {this.state.errorPopup && 
              <PopUp
                visible={true}
                headerOne="CÓ LỖI XẢY RA"
                img_src={wrong_img}
                headerTwo="Tính năng hiện đang phát triển, xin vui lòng thử lại sau"
                buttonOne_text="Đóng"
                buttonOne_bgColor="#6BBD12"
                buttonOne_disable={false}
                buttonOne_link="/landing"
                clicked={() => {
                  this.setState({ errorPopup: false });
                }}
              />
          }
          <View style={{paddingVertical: 20, backgroundColor: '#ECB22D'}} />
          <View style={{height: 60}}>
            <NavigationProducts to={{pathname: `/categories/0`, state: 'flushData'}} parsePath={this.props.location.state.going} filter={this.props.location.state.filter}/>
          </View>
          <ScrollView style={{flexDirection: 'column'}} ref={(ref) => { this.myscroll = ref; }} >
            <View style={{height: contentHeight*0.4, marginBottom: 10}}>
              <Carousel
                ref={(c) => { this._carousel = c; }}
                data={this.state.images}
                renderItem={this._renderSliderItem}
                sliderWidth={screenWidth}
                itemWidth={screenWidth}
                onSnapToItem={(index) => this.setState({ activeSlide: index }) }
              />
              {this.pagination}
            </View>
            <View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                  marginBottom: 20
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginHorizontal: 30
                  }}>
                  <ImageBackground
                    resizeMode="contain"
                    style={{
                      width: 0.0869 * screenWidth,
                      height: 0.0587 * screenHeight,
                    }}
                    source={require('../../../../assets/images/Products/percent.png')}>
                    {
                      (_.get(this.props, 'location.state.productId') || _.get(this.state.productDetail, 'pricing.onSale')) ? (
                        <Text style={{textAlign: 'center', color: 'white', marginTop: 12}}></Text>
                      ) : (
                        <Text style={{textAlign: 'center', color: 'white', marginTop: 12}}>32%</Text>
                      )
                    }
                  </ImageBackground>
                  <View style={{flexDirection: 'column', marginHorizontal: 30}}>
                    <Text
                      style={{
                        fontSize: RFValue(24),
                        fontFamily: 'Roboto-BoldCondensed',
                        color: 'rgb(81,81,81)'
                      }}>
                      {
                        _.get(this.props, 'location.state.productId') ?
                        _.get(this.state.productDetail, 'name') :
                        "Untitled"
                      }
                    </Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text
                        style={{
                          fontSize: RFValue(24),
                          color: 'rgb(173,14,23)',
                          fontFamily: 'Roboto-Regular',
                        }}>
                        {
                          _.get(this.props, 'location.state.productId') ?
                          `${_.get(this.state.productDetail, 'formated_special_price', `${_.get(this.state.productDetail, 'regular_formated_price', '')}`).replace(',00', '')}` :
                          "0 đ"
                        }
                      </Text>
                      <Text
                        style={{
                          marginLeft: 0.02 * screenWidth,
                          color: 'rgb(160,158,156)',
                          fontSize: RFValue(14),
                          textDecorationLine: 'line-through',
                          fontFamily: 'Roboto-Regular',
                        }}>
                          {
                            _.get(this.state.productDetail, 'formated_special_price') ?
                            `${_.get(this.state.productDetail, 'regular_formated_price', '')}`:
                            ''
                          }
                      </Text>
                    </View>    
                  </View>
                  <TouchableOpacity 
                    style={{ paddingTop: 5 }}
                    onPress={this.addToWishList}>
                    <Icon component={Icon} size={26} color={this.state.isWishlisted} name="heart" /> 
                    </TouchableOpacity>  
                </View>
              </View>
              <View style={{ height: 60, flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({description: true});
                    this.setState({specifications: false});
                    this.setState({recipe: false});
                  }}
                  style={
                    this.state.description
                      ? style.touchableViewsOn
                      : style.touchableViews
                  }>
                  <Text style={style.textTouchable}>Mô tả</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({description: false});
                    this.setState({specifications: true});
                    this.setState({recipe: false});
                  }}
                  style={
                    this.state.specifications
                      ? style.touchableViewsOn
                      : style.touchableViews
                  }>
                  <Text style={style.textTouchable}>Chi tiết sản phẩm</Text>
                </TouchableOpacity>
                {
                  _.get(this.state.productDetail, 'custom_field_5') !== '' && (
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({description: false});
                        this.setState({specifications: false});
                        this.setState({recipe: true});
                      }}
                      style={
                        this.state.recipe
                          ? style.touchableViewsOn
                          : style.touchableViews
                      }>
                      <Text style={style.textTouchable}>Công thức</Text>
                    </TouchableOpacity>
                  )
                }
              </View>
              <View>
                    {this.state.description ? <Description text={_.get(this.state.productDetail, 'description')} renderText={_.get(this.props, 'location.state.productId')} /> : null}
                    {this.state.specifications ? <Specifications weight={_.get(this.state.productDetail, 'weight')} /> : null}
                    {this.state.recipe ? <Recipe articles={this.state.articles} gone={this.props.location.state.going}/> : null}
              </View>
            </View>
            {_.get(this.state.productDetail, 'custom_field_4') !== '' && <RelatedView ids={_.get(this.state.productDetail, 'custom_field_4')}/>}
            <RecentView productItems={this.props.recentItems}/>
            <View style={{height: 0.01 * screenHeight}} />
          </ScrollView>
            <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => {
                let limit = 1;
                if (_.find(this.props.cartItems, ['id', _.get(this.state.productDetail, 'id')])) {
                  limit = 0
                }
                if (this.state.qty > limit) {
                  this.setState({qty: this.state.qty - 1});
                }
              }}
              style={{
                flex: 0.5,
                backgroundColor: 'rgb(231,231,231)',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 5
              }}>
              <Text>-</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{flex: 0.5, justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'Roboto-BoldCondensed'}}>
                {this.state.qty}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({qty: this.state.qty + 1});
              }}
              style={{
                flex: 0.5,
                backgroundColor: 'rgb(231,231,231)',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 5
              }}>
              <Text>+</Text>
            </TouchableOpacity>
            {/* <Link
              component={TouchableOpacity}
              to={{
                pathname: '/livechat',
                state: {go: this.props.location.state.going, back: true},
              }}
              style={{
                flex: 1,
                backgroundColor: 'rgb(236,178,45)',
                justifyContent: 'center',
                alignItems: 'center',
                flexGrow: 1,
                paddingVertical: 5
              }}>
              <Image
                style={{
                  height: 0.0349 * screenHeight,
                  width: 0.0809 * screenWidth,
                }}
                source={require('../../../../assets/images/chat.png')}
                resizeMode="contain"
              />
              <Text style={style.btnText}>CHAT NGAY</Text>
            </Link> */}
            <TouchableOpacity 
              onPress={()=>{ this.setState(()=>({errorPopup: true}))}}
              style={{
                flex: 1,
                backgroundColor: 'rgb(236,178,45)',
                justifyContent: 'center',
                alignItems: 'center',
                flexGrow: 1,
                paddingVertical: 5
              }} 
            >
              <Image
                  style={{
                    height: 0.0349 * screenHeight,
                    width: 0.0809 * screenWidth,
                  }}
                  source={require('../../../../assets/images/chat.png')}
                  resizeMode="contain"
                />
                <Text style={style.btnText}>CHAT NGAY</Text>
            </TouchableOpacity>
            {
              _.find(this.props.cartItems, ['id', _.get(this.state.productDetail, 'id')]) ? (
                <TouchableOpacity
                  onPress={() => {this.addToCart()}}
                  style={{
                    flex: 1,
                    flexGrow: 1,
                    backgroundColor: 'rgb(236,178,45)',
                    borderLeftWidth: 1,
                    borderColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 5
                  }}>
                  <Text style={style.btnText}>CẬP NHẬT</Text>
                  <Text style={style.btnText}>GIỎ HÀNG</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => {this.addToCart()}}
                  style={{
                    flex: 1,
                    flexGrow: 1,
                    backgroundColor: 'rgb(236,178,45)',
                    borderLeftWidth: 1,
                    borderColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 5
                  }}>
                  <Text style={style.btnText}>THÊM VÀO</Text>
                  <Text style={style.btnText}>GIỎ HÀNG</Text>
                </TouchableOpacity>
              )
            }
            <TouchableOpacity
              disabled={this.state.qty === 0}
              onPress={this.onBuyNow}
              // to={{
              //   pathname: '/shopping/cart',
              //   state: {go: this.props.location.state.going},
              // }}
              style={{
                flex: 1,
                backgroundColor: this.state.qty === 0 ? '#9e9e9e' : 'rgb(173,14,23)',
                justifyContent: 'center',
                alignItems: 'center',
                flexGrow: 1,
              }}>
              <Text style={style.btnText}>MUA NGAY</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1, paddingBottom: paddingBottom}}>
          <View style={{paddingVertical: 20, backgroundColor: '#ECB22D'}} />
            <View style={{height: 60}}>
              <NavigationProducts to={{pathname: `/categories/0`, state: 'flushData'}} parsePath={this.props.location.state.going} />
            </View>
            <View style={{height: 0.8 * screenHeight, width: '100%'}}>
              <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
          </View>
        </View>
      );
    }
  }

  render() {
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => this._renderContent(insets)}
      </SafeAreaInsetsContext.Consumer>
    );
  }
}

const style = {
  touchableViews: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'rgb(216,216,216)',
    borderBottomColor: 'rgb(216,216,216)',
    borderBottomWidth: 1,
  },
  touchableViewsOn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'rgb(216,216,216)',
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  textTouchable: {
    color: 'rgb(81,81,81)',
    fontFamily: 'Roboto-Regular',
    fontWeight: 'normal',
    fontSize: RFValue(15),
    textAlign: 'center'
  },
  btnText: {
    color: 'white',
    fontSize: RFValue(15),
    fontFamily: 'Roboto-BoldCondensed',
  },
};
const mapStateToProps = (state) => {
  return {
    cartItems: state.cartReducer.cartItems,
    recentItems: state.cartReducer.recentItems,
    cartDetail: state.cartReducer.cartDetail,
    checkout: state.checkoutReducer.checkout,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxAddToCart: (product, cartDetail) => dispatch(addToCart(product, cartDetail)),
    reduxAddToRecent: (product) => dispatch(addToRecent(product)),
    reduxUpdateCart: (productId, quantity, cartDetail) => dispatch(updateCart(productId, quantity, cartDetail)),
    reduxRemoveFromCart: (productId, cartDetail) => dispatch(removeFromCart(productId, cartDetail)),
    reduxResetCart: () => dispatch(resetCart()),
    reduxResetCheckout: () => dispatch(resetCheckout())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Products);
