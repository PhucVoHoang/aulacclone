import React, {Component} from 'react';
import {View, ScrollView, FlatList, TouchableOpacity, Dimensions, Image, Text} from 'react-native';
import {Link} from 'react-router-native';
import _ from 'lodash';
import {RFValue} from 'react-native-responsive-fontsize';
import ProductBlock from '../../../components/landing/productBlock';

import API from '../../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class RecentView extends Component {
  state = {
    productItems: [],
  };
  componentDidMount() {
    API.getProducts({page: 1, category_id: this.props.catId, isNew: this.props.isNew, isFeature: this.props.isFeature, keyword: null}).then(res => {
      this.setState({
        productItems: _.get(res, 'data', {})
      })
    }).catch(err => {
      console.log(err);
    })
  }
  render() {
    const productList = <View style={{flex: 1, flexDirection: 'row'}}>{this.props.productItems && this.props.productItems.map(item => (
        <ProductBlock
            title={item.name}
            img={item.image_url}
            width={0.4}
            height={0.23}
            topMargin={0.02}
            leftMargin={0.04}
            gone="/landing"
            price={item.regular_formated_price}
            special_price={item.formated_special_price}
            productId={item.id}
            key={item.id}
        />
      ))}</View>;
    return (
        <View
              style={{
                height: 0.32 * screenHeight,
                width: '100%',
                justifyContent: 'space-around',
                flexDirection: 'column',
              }}>
              <View
                style={{
                  // height: 0.06 * screenHeight,
                  marginLeft: 0.04 * screenWidth,
                  flexDirection: 'row',
                }}>
              <View
                  style={{
                    width: 0.73 * screenWidth,
                    flexDirection: 'row',
                    flexGrow: 1,
                    flex: 1,
                  }}>
                  <Text
                    style={{
                      color: 'rgb(173,14,23)',
                      fontSize: RFValue(16),
                      marginTop: 0.02 * screenHeight,
                      fontFamily: 'Roboto-Bold',
                    }}>
                    SẢN PHẨM ĐÃ XEM
                  </Text>
                </View>
                </View>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}> 
                {productList}
            </ScrollView>
            {/* <View style={{height: 0.03 * screenHeight}} /> */}
        </View>
    )
  }
};
export default RecentView;