import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import Header from '../../../components/navigation/Header';
import Footer from '../../../components/navigation/Footer';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';

import API from '../../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function Item(props) {
  return (
    <Link
      component={TouchableOpacity}
      to={{
        pathname: '/products',
        state: {
          going: props.gone,
          productId: props.item.id,
        },
      }}
      style={{
        flex: 1,
        margin: 15,
        width: props.width * screenWidth,
        height: props.height * screenHeight,
      }}>
        <View style={{flex: 25}}>
        <Image
          style={{
            width: undefined,
            height: undefined,
            flex: 1,
          }}
          source={{uri: props.item.image_url}}
        />
      </View>
      <View style={{flex: 14, justifyContent: 'space-around'}}>
        <Text
          style={{
            color: 'rgb(81,81,81)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
          }}>
          {props.item.name}
        </Text>
        <Text
          style={{
            color: 'rgb(255,30,2)',
            textAlign: 'left',
            fontFamily: 'Roboto-Bold',
          }}>
          {props.item.formated_special_price ? props.item.formated_special_price : props.item.regular_formated_price}
        </Text>
        {props.item.formated_special_price && (<Text
          style={{
            color: 'rgb(160,158,156)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
            textDecorationLine: 'line-through',
          }}>
          {props.item.regular_formated_price}
        </Text>)}
      </View>
    </Link>
  );
}

class Tatca extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
      loading: true,
      loadingMore: false,
      filtering: false,
      refreshing: false,
      error: null,
      isFlushed: false,
      keyword: null
    };
  }
  
  componentDidMount() {
    this._fetchAllProducts();
  }
  _fetchAllProducts = () => {
    const { page, keyword } = this.state;
    const { catId } = this.props.match.params;
    let postParam = {
      page
    };
    if (catId && catId != 0) {
      if(catId == 'isNew') {
        postParam['isNew'] = true;
      } else if (catId == 'isFeature') {
        postParam['isFeature'] = true;
      }
    }

    API.getProducts(postParam).then(response => {
      this.setState((prevState, nextProps) => ({
        data:
          page === 1
            ? Array.from(response.data)
            : [...this.state.data, ...response.data],
        loading: false,
        loadingMore: false,
        refreshing: false,
        isFlushed: false
      }));
    })
    .catch(error => {
      this.setState({ error, loading: false });
    });
  };
  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this._fetchAllProducts();
      }
    );
  };
  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        this._fetchAllProducts();
      }
    );
  };

  UNSAFE_componentWillReceiveProps(nextProps){
    console.log('UNSAFE_componentWillReceiveProps', nextProps)
    console.log('UNSAFE_componentWillReceiveProps', this.props)
    if (!this.state.isFlushed && nextProps.location.state === 'flushData') {
      this.setState({
        isFlushed: true,
        loading: true,
      }, () => this._handleRefresh());
    }
  }

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <Header bgColor="#ECB22D" go={`/featurecategories/${this.props.match.params.catId}`} />
        </View>
        <View style={{flex: 8}}>
          <View style={{flex: 28.65}}>
            <Link
              component={TouchableOpacity}
              to={{
                pathname: '/products/filter',
                state: {go: `/featurecategories/${this.props.match.params.catId}`},
              }}
              style={{
                marginTop: 0.0326 * screenHeight,
                flexDirection: 'row',
                marginLeft: 0.04 * screenWidth,
                marginRight: 0.04 * screenWidth,
                justifyContent: 'flex-end',
              }}>
              <Image
                resizeMode="contain"
                style={{
                  width: RFValue(16.9),
                  height: RFValue(15.6),
                }}
                source={require('../../../../assets/images/Landing/filter.png')}
              />
              
            </Link>
            {!this.state.loading ? (
              <FlatList
                contentContainerStyle={{
                  flexGrow: 1,
                }}
                numColumns={2}
                data={this.state.data}
                renderItem={({ item }) => (
                  <Item item={item} width={0.42} height={0.27} gone={`/featurecategories/${this.props.match.params.catId}`}/>
                )}
                keyExtractor={item => item.id.toString()}
                onRefresh={this._handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this._handleLoadMore}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
              />
            ) : (
              <View
                style={{flex: 1,
                justifyContent: 'center',
                alignItems:'center'
              }}>
                <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
              </View>
            )}
          </View>
        </View>
        <View style={{flex: 1}}>
          <Footer
            home={false}
            square={true}
            comments={false}
            alarm={false}
            profile={false}
          />
        </View>
      </View>
    );
  }
}

export default Tatca;
