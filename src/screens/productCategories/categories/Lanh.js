import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import Header from '../../../components/navigation/Header';
import Footer from '../../../components/navigation/Footer';
import CategoryToggle from '../../../components/navigation/categoryToggle';
import {RFValue} from 'react-native-responsive-fontsize';
import ProductBlock from '../../../components/landing/productBlock';
import {Link} from 'react-router-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const b3 = require('../../../../assets/images/Landing/b3.png');
const b4 = require('../../../../assets/images/Landing/b4.png');
const i5 = require('../../../../assets/images/Landing/i5.png');
const i7 = require('../../../../assets/images/Landing/i7.png');

class Lanh extends Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <Header bgColor="#ECB22D" go="/categories/lanh" />
        </View>
        <View style={{flex: 8}}>
          <View style={{flex: 2.5}}>
            <CategoryToggle
              tatca={false}
              giavi={false}
              anlien={false}
              tiet={false}
              kho={false}
              lon={false}
              lanh={true}
              khat={false}
            />
          </View>
          <View style={{flex: 28.65}}>
            <Link
              component={TouchableOpacity}
              to={{
                pathname: '/products/filter',
                state: {go: '/categories/lanh'},
              }}
              style={{
                marginTop: 0.0326 * screenHeight,
                flexDirection: 'row',
                marginLeft: 0.04 * screenWidth,
                marginRight: 0.04 * screenWidth,
                justifyContent: 'flex-end',
              }}>
              <Image
                resizeMode="contain"
                style={{
                  width: RFValue(16.9),
                  height: RFValue(15.6),
                }}
                source={require('../../../../assets/images/Landing/filter.png')}
              />
              <Text
                style={{
                  marginLeft: 5,
                  color: 'black',
                  fontSize: RFValue(14),
                  fontFamily: 'Roboto-Bold',
                }}>
                Bộ lọc
              </Text>
            </Link>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                marginTop: 0.019 * screenHeight,
              }}>
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginRight: 0.04 * screenWidth,
                    flexDirection: 'column',
                  }}>
                  <ProductBlock
                    title="Gỏi cuốn chay - vegan spring rolls"
                    width={0.42}
                    height={0.27}
                    topMargin={0.03}
                    leftMargin={0.04}
                    gone="/categories/lanh"
                    img={i5}
                    price="75.000 đ"
                  />
                  <ProductBlock
                    title="Gỏi cuốn chay - vegan spring rolls"
                    width={0.42}
                    height={0.27}
                    topMargin={0.03}
                    leftMargin={0.04}
                    gone="/categories/lanh"
                    img={i7}
                    price="75.000 đ"
                  />
                </View>
                <View
                  style={{
                    marginRight: 0.04 * screenWidth,
                    flexDirection: 'column',
                  }}>
                  <ProductBlock
                    title="Gỏi cuốn chay - vegan spring rolls"
                    width={0.42}
                    height={0.27}
                    topMargin={0.03}
                    leftMargin={0.04}
                    gone="/categories/lanh"
                    img={b3}
                    price="75.000 đ"
                  />
                  <ProductBlock
                    title="Gỏi cuốn chay - vegan spring rolls"
                    width={0.42}
                    height={0.27}
                    topMargin={0.03}
                    leftMargin={0.04}
                    gone="/categories/lanh"
                    img={b4}
                    price="75.000 đ"
                  />
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
        <View style={{flex: 1}}>
          <Footer
            home={false}
            square={true}
            comments={false}
            alarm={false}
            profile={false}
          />
        </View>
      </View>
    );
  }
}

export default Lanh;
