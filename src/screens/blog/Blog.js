import React, { Component } from "react";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Text,
  TextInput,
  Image,
  ScrollView,
  ActivityIndicator,
  FlatList,
} from "react-native";
import NavigationHeader from "../../components/navigation/navigationHeader";
import { Link } from "react-router-native";
import API from "../../services/api";
import _ from "lodash";
import moment from "moment";

const screenWidth = Math.round(Dimensions.get("window").width);
const screenHeight = Math.round(Dimensions.get("window").height);

function Item(props) {
  return (
    <View>
      <View style={blogStyle.touchableStyleContainer}>
        <Link
          component={TouchableOpacity}
          to={{
            pathname: "/profile/blogpost",
            state: {
              go: props.gone,
              postId: props.item.id,
              data: props.item,
              headline: props.headline,
            },
          }}
          style={blogStyle.touchableStyle}
        >
          <Image
            resizeMode="cover"
            style={{ width: "100%", height: "51%" }}
            source={{ uri: props.item.image_thumbnail }}
          />
          <Text style={blogStyle.headersmallText}>{props.headline}</Text>
          <Text style={blogStyle.headerStyle}>{props.item.title}</Text>
          <Text style={blogStyle.timeStyle}>
            {moment(props.item.posted_at).format("DD/MM/YYYY")}
          </Text>
          <Text style={blogStyle.maintextStyle}>
            {props.item.short_description
              ? props.item.short_description
              : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the simply dummy text of the printing and typesetting industry..."}
          </Text>
        </Link>
        <View style={blogStyle.shadowDivider} />
      </View>
    </View>
  );
}

class Blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
      loading: true,
      loadingMore: false,
      filtering: false,
      refreshing: false,
      error: null,
      isFlushed: false,
    };
  }

  componentDidMount() {
    this._fetchAllPosts();
  }
  _fetchAllPosts = () => {
    const { page } = this.state;
    const { catId } = this.props.match.params;
    let postParam = {
      page,
      catId,
    };

    API.getBlogPosts(postParam)
      .then((response) => {
        this.setState((prevState, nextProps) => ({
          data:
            page === 1
              ? Array.from(response.data)
              : [...this.state.data, ...response.data],
          loading: false,
          loadingMore: false,
          refreshing: false,
          isFlushed: false,
        }));
      })
      .catch((error) => {
        this.setState({ error, loading: false });
      });
  };
  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true,
      },
      () => {
        this._fetchAllPosts();
      }
    );
  };
  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true,
      }),
      () => {
        this._fetchAllPosts();
      }
    );
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1.1 }}>
          <NavigationHeader
            title={_.get(
              this.props,
              "location.state.headline",
              "CHUYÊN MỤC NẤU ĂN"
            )}
            to={_.get(this.props, "location.state.go", "/profile")}
            back={true}
          />
        </View>
        <View style={{ flex: 8.4 }}>
          {!this.state.loading ? (
            <FlatList
              contentContainerStyle={{
                flexGrow: 1,
              }}
              numColumns={1}
              data={this.state.data}
              renderItem={({ item }) => (
                <Item
                  item={item}
                  gone={`/profile/blog/${this.props.match.params.catId}`}
                  headline={_.get(
                    this.props,
                    "location.state.headline",
                    "CHUYÊN MỤC NẤU ĂN"
                  )}
                />
              )}
              keyExtractor={(item) => item.id.toString()}
              onRefresh={this._handleRefresh}
              refreshing={this.state.refreshing}
              onEndReached={this._handleLoadMore}
              onEndReachedThreshold={0.5}
              initialNumToRender={10}
            />
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <ActivityIndicator style={{ flex: 1, alignSelf: "center" }} />
            </View>
          )}
        </View>
      </View>
    );
  }
}
const blogStyle = {
  shadowDivider: {
    // shadowColor: "black",
    // // backgroundColor: 'grey',
    // shadowOpacity: 0.2,
    // opacity: 0.2,
    // shadowRadius: 4,
    // elevation: 6,
    // width: "100%",
    // height: 0.02 * screenHeight,
    // padding: 20,
  },
  touchableStyle: {
    width: "100%",
    height: 0.4 * screenHeight,
    padding: 20,
    backgroundColor: "white",
    borderWidth: 0.5,
    borderColor: "lightGrey",
    
  },
  touchableStyleContainer: {
    width: "100%",
    height: 0.5 * screenHeight,
    border: 1,
    borderColor: "black",
    padding: 20,
  },
  maintextStyle: {
    fontFamily: "Roboto-Regular",
    marginTop: "1%",
    marginLeft: "2%",
    marginRight: "2%",
    fontSize: 14,
    color: "rgb(109,109,109)",
  },
  timeStyle: {
    fontFamily: "Roboto-Regular",
    marginTop: "1%",
    marginLeft: "2%",
    fontSize: 10,
    color: "rgb(160,158,156)",
  },
  headerStyle: {
    fontFamily: "Roboto-Regular",
    marginTop: "1%",
    marginLeft: "2%",
    fontSize: 20,
    color: "black",
  },
  imgStyle: {
    height: 0.0269 * screenHeight,
    width: 0.048 * screenWidth,
  },
  headersmallText: {
    fontFamily: "Roboto-Regular",
    marginTop: "2%",
    marginLeft: "2%",
    fontSize: 10,
    color: "rgb(160,158,156)",
  },
};

export default Blog;
