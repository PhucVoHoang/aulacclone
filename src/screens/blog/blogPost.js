import React, { Component } from "react";
import { View, Dimensions, Text, Image, ScrollView, Linking } from "react-native";
import NavigationHeader from "../../components/navigation/navigationHeader";
import moment from "moment";
import HTML from "react-native-render-html";
import WebView from "react-native-webview";
import API from "../../services/api";
import _ from "lodash";

const screenWidth = Math.round(Dimensions.get("window").width);
const screenHeight = Math.round(Dimensions.get("window").height);

class blopPost extends Component {
  state = {
    loading: false,
    data: {},
  };
  componentDidMount() {
    this.setState({
      loading: true,
    });
    const postId = _.get(this.props, "location.state.postId");
    if (postId) {
      const { data } = this.props.location.state;
      if (data) {
        this.setState({
          data,
        });
      } else {
        API.getBlogPost(postId)
          .then((res) => {
            this.setState({
              data: res,
              loading: false,
            });
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  }

  render() {
    const { data } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1.1 }}>
          <NavigationHeader
            title={data.title}
            to={this.props.location.state.go}
            back={true}
          />
        </View>

        <View style={{ flex: 8.9 }}>
          <ScrollView>
            {data.subtitle && (
              <WebView
                style={{ width: screenWidth, height: 0.25 * screenHeight }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: data.subtitle && data.subtitle.replace('watch?v=', 'embed/')}}
              />
            )}
            {!data.subtitle && (
              <Image
                resizeMode="cover"
                style={{ width: screenWidth, height: 0.25 * screenHeight }}
                source={{ uri: data.image_large }}
              />
            )}

            <Text style={blogStyle.headersmallText}>
              {this.props.location.state.headline}
            </Text>
            <Text style={blogStyle.timeStyle}>
              {moment(data.posted_at).format("DD/MM/YYYY")}
            </Text>

            {data.short_description && (
              <Text style={blogStyle.maintextStyle}>
                {data.short_description}
              </Text>
            )}
            <View style={blogStyle.maintextStyle}>
              {data.post_body && (
                <HTML
                  html={data.post_body}
                  style={{ fontFamily: "Roboto-Regular" }}
                  imagesMaxWidth={Dimensions.get("window").width}
                  onLinkPress={(evt, href) => {
                    Linking.openURL(href);
                  }}
                />
              )}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
const blogStyle = {
  shadowDivider: {
    shadowColor: "black",
    backgroundColor: "grey",
    shadowOpacity: 0.2,
    opacity: 0.2,
    shadowRadius: 4,
    elevation: 6,
    width: "100%",
    height: 0.02 * screenHeight,
  },
  touchableStyle: {
    width: "100%",
    height: 0.53 * screenHeight,
    flexGrow: 1,
    shadowColor: "black",
    backgroundColor: "white",
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 6,
  },
  maintextStyle: {
    fontFamily: "Roboto-Bold",
    fontSize: 14,
    color: "rgb(109,109,109)",
    marginLeft: "2%",
    marginRight: "2%",
    marginTop: 5,
  },
  timeStyle: {
    fontFamily: "Roboto-Regular",
    marginTop: "1%",
    marginLeft: "2%",
    fontSize: 10,
    color: "rgb(160,158,156)",
  },
  headerStyle: {
    fontFamily: "Roboto-Regular",
    marginTop: "1%",
    marginLeft: "2%",
    fontSize: 20,
    color: "black",
  },
  imgStyle: {
    width: "100%",
    height: "51%",
  },
  headersmallText: {
    fontFamily: "Roboto-Regular",
    marginTop: "2%",
    marginLeft: "2%",
    fontSize: 10,
    color: "rgb(160,158,156)",
  },
};

export default blopPost;
