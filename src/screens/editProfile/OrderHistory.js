import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  ScrollView,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';
import _ from 'lodash';
import moment from "moment";

import API from '../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function TabBar(props) {
  return (
    <View style={{flex: 1, flexGrow: 1}}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} scrollEnabled={false}>
          <Link
            component={TouchableOpacity}
            to={{pathname: `/profile/orderhistory/0`, state: 'flushData'}}
            style={(props.active != 1) ? styles.touchableViewOn : styles.touchableView}>
            <Text style={(props.active != 1) ? styles.textOn : styles.textOff}>
              Trạng thái đơn hàng
            </Text>
          </Link>
          <Link
            component={TouchableOpacity}
            to={{pathname: `/profile/orderhistory/1`, state: 'flushData'}}
            style={(props.active == 1) ? styles.touchableViewOn : styles.touchableView}>
            <Text style={(props.active == 1) ? styles.textOn : styles.textOff}>
              Lịch sử đơn hàng
            </Text>
          </Link>
        </ScrollView>
      </View>
  )
}

function Item(props) {
  let statusColor = 'rgb(236, 178, 45)';
  let statusLabelColor = 'white';
  let statusLabel = 'Đang xử lý';
  if (props.item.status == 'canceled') {
    statusColor = 'rgb(173, 14, 23)';
    statusLabel = 'Đã huỷ';
  }
  if (props.item.status == 'closed') {
    statusColor = 'rgb(255, 227, 228)';
    statusLabelColor = 'rgb(173, 14, 23)';
    statusLabel = 'Không thành công';
  }
  if (props.item.status == 'completed') {
    statusColor = 'rgb(107, 189, 18)';
    statusLabelColor = 'white';
    statusLabel = 'Đã hoàn thành';
  }
  if (props.item.status == 'processing') {
    statusLabel = 'Đang giao';
  }
  return (
    <Link
      component={TouchableOpacity}
      to={{
        pathname: '/profile/orders',
        state: {
          go: props.go,
          orderId: props.item.id,
        },
      }}
      style={{
        flex: 1,
        marginTop: 10,
      }}>

      <View style={{backgroundColor: 'white', padding: 20}}>
        <View style={{justifyContent: 'center', flexDirection: 'row', marginBottom: 20}}>
          <Text
            style={{
              flex: 1,
              color: 'rgb(81,81,81)',
              textAlign: 'left',
              fontFamily: 'Roboto-Bold',
              justifyContent: 'flex-start'
            }}>
            #{props.item.id}
          </Text>
          <Text
            style={{
              flex: 1,
              color: statusLabelColor,
              textAlign: 'center',
              fontFamily: 'Roboto-Regular',
              justifyContent: 'flex-end',
              backgroundColor: statusColor,
              padding: 5,
              borderRadius: 10,
              overflow: 'hidden'
            }}>
            {statusLabel}
          </Text>
        </View>
        <View style={{justifyContent: 'center', flexDirection: 'row', marginBottom: 20, borderBottomColor: 'rgb(243, 243, 243)', borderBottomWidth: 2, paddingBottom: 5}}>
          <Text
            style={{
              flex: 1,
              color: 'rgb(151,151,151)',
              textAlign: 'left',
              fontFamily: 'Roboto-Regular',
              justifyContent: 'flex-start'
            }}>
            Ngày đặt hàng
          </Text>
          <Text
            style={{
              flex: 1,
              color: 'rgb(49, 51, 50)',
              textAlign: 'right',
              fontFamily: 'Roboto-Regular',
              justifyContent: 'flex-end',
            }}>
            {moment(props.item.created_at).format('h:mm A DD/MM/YYYY')}
          </Text>
        </View>
        <View style={{justifyContent: 'center', flexDirection: 'row', marginBottom: 20, borderBottomColor: 'rgb(243, 243, 243)', borderBottomWidth: 2, paddingBottom: 5}}>
          <Text
            style={{
              flex: 1,
              color: 'rgb(151,151,151)',
              textAlign: 'left',
              fontFamily: 'Roboto-Regular',
              justifyContent: 'flex-start'
            }}>
            Ngày giao hàng
          </Text>
          <Text
            style={{
              flex: 1,
              color: 'rgb(49, 51, 50)',
              textAlign: 'right',
              fontFamily: 'Roboto-Regular',
              justifyContent: 'flex-end',
            }}>
            {(props.item.status == 'processing' || props.item.status == 'completed') && moment(props.item.updated_at).format('h:mm A DD/MM/YYYY')}
            {(props.item.status !== 'processing' && props.item.status !== 'completed') && statusLabel}
          </Text>
        </View>
        <View style={{justifyContent: 'center', flexDirection: 'row', marginBottom: 20}}>
          <Text
            style={{
              flex: 1,
              color: 'rgb(151,151,151)',
              textAlign: 'left',
              fontFamily: 'Roboto-Regular',
              justifyContent: 'flex-start'
            }}>
            Tổng đơn hàng
          </Text>
          <Text
            style={{
              flex: 1,
              color: 'rgb(49, 51, 50)',
              textAlign: 'right',
              fontFamily: 'Roboto-Regular',
              justifyContent: 'flex-end',
            }}>
            {props.item.formated_base_grand_total}
          </Text>
        </View>
      </View>
    </Link>
  );
}

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 2,
      loading: true,
      loadingMore: false,
      filtering: false,
      refreshing: false,
      error: null,
      isFlushed: false
    };
  }
  
  componentDidMount() {
    this._fetchAllOrders();
    this._handleRefresh()
  }
  _fetchAllOrders = () => {
    const { page } = this.state;
    let postParam = {
      page,
      catId: this.props.match.params.catId
    };
    API.getOrders(postParam).then(response => {
      this.setState((prevState, nextProps) => ({
        data:
          page === 1
            ? Array.from(response.data)
            : [...this.state.data, ...response.data],
        loading: false,
        loadingMore: false,
        refreshing: false,
        isFlushed: false
      }));
    })
    .catch(error => {
      console.log('getOrders', error)
      this.setState({ error, loading: false });
    });
  };
  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this._fetchAllOrders();
      }
    );
  };
  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        this._fetchAllOrders();
      }
    );
  };

  UNSAFE_componentWillReceiveProps(nextProps){
    if (!this.state.isFlushed && nextProps.location.state === 'flushData') {
      this.setState({
        isFlushed: true,
        loading: true,
      }, () => this._handleRefresh());
    }
  }

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader title="LỊCH SỬ" to={'/profile'} back={true} />
        </View>
        <View style={{flex: 8, backgroundColor: 'rgb(243, 243, 243)'}}>
          <View style={{flex: 2.5}}>
            <TabBar active={this.props.match.params.catId}/>
          </View>
          <View style={{flex: 28.65}}>
            {!this.state.loading ? (
              <FlatList
                contentContainerStyle={{
                  flexGrow: 1,
                }}
                numColumns={1}
                data={this.state.data}
                renderItem={({ item }) => (
                  <Item item={item} width={0.42} height={0.27} go={`/profile/orderhistory/${this.props.match.params.catId}`}/>
                )}
                keyExtractor={item => item.id.toString()}
                onRefresh={this._handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this._handleLoadMore}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
              />
            ) : (
              <View
                style={{flex: 1,
                justifyContent: 'center',
                alignItems:'center'
              }}>
                <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  touchableView: {
    width: 0.5 * screenWidth,
    height: '100%',
    justifyContent: 'flex-end',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 4,
    paddingBottom: 15
  },
  touchableViewOn: {
    width: 0.5 * screenWidth,
    height: '100%',
    justifyContent: 'flex-end',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(173,14,23)',
    paddingBottom: 15
  },
  textOn: {
    textAlign: 'center',
    fontSize: RFValue(14),
    fontFamily: 'Roboto-Regular',
  },
  textOff: {
    textAlign: 'center',
    fontSize: RFValue(14),
    fontFamily: 'Roboto-Regular',
  },
};
export default OrderHistory;
