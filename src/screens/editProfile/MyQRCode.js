import React, { Component } from 'react';
 
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  Dimensions
} from 'react-native';

import QRCode from 'react-native-qrcode-svg';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {getInternetCredentials} from '../../services/storageServices';
import CHATAPI from '../../services/ChatApi';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class MyQRCode extends Component {
    state = {
        visitor_token: null,
        visitor_username: 'noname',
      };
    componentDidMount() {
        getInternetCredentials().then(credential => {
            if(credential.username) {
              const userInfo = JSON.parse(credential.username);
              this.setState({ visitor_token: userInfo.authToken, visitor_username: userInfo.me.username });
            }
        });
    }
 
    render() {
        let logoFromFile = require('../../../assets/images/logo_aulac.png');
        return (
        <View style={{flex: 1}}>
            <View style={{flex: 1.1}}>
                    <NavigationHeader title="QR CODE" to={{pathname: "/addfriend/qrcode", state: { go: "/addfriend", tab: 'contacts' }}} back={true} />
                </View>
            <View
                style={{
                flex: 8.9,
                width: screenWidth,
                alignItems: 'center',
                paddingTop: 40
                }}>
                    <QRCode
                        value={this.state.visitor_username}
                        logo={logoFromFile}
                        size={screenWidth*0.8}
                        logoSize={screenWidth*0.2}
                        />
            </View>
        </View>
        );
    }
}

export default MyQRCode;
 