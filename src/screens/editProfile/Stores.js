import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  ScrollView,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';
import Icon from 'react-native-vector-icons/Entypo';
import _ from 'lodash';

import API from '../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function Item(props) {
  return (
    <Link
      component={TouchableOpacity}
      to={{
        pathname: '/profile/storedetail',
        state: {
          go: props.go,
          storeId: props.item.id,
          storeDetail: props.item,
        },
      }}
      style={{
        flex: 1,
        marginTop: 10,
      }}>

      <View style={{flex: 1, backgroundColor: 'white', padding: 20, flexDirection: 'row'}}>
        <View style={{flex: 90 }}>
            <View style={{justifyContent: 'center', flexDirection: 'row', marginBottom: 20}}>
            <Text
                style={{
                flex: 1,
                color: 'rgb(81,81,81)',
                textAlign: 'left',
                fontFamily: 'Roboto-Bold',
                justifyContent: 'flex-start'
                }}>
                {props.item.name}
            </Text>
            </View>
            <View style={{justifyContent: 'flex-start', flexDirection: 'column' }}>
                <View style={{flex: 1, justifyContent: 'flex-start', marginBottom: 20, flexDirection: 'row'}}>
                    <Icon component={Icon} size={16} color="rgb(173, 14, 23)" name="location-pin" iconStyle={{ marginRight: 10 }}/> 
                    <Text
                        style={{
                            color: 'rgb(49, 51, 50)',
                            textAlign: 'left',
                            fontFamily: 'Roboto-Regular',
                            marginLeft: 10
                        }}>
                        {props.item.landmark}
                    </Text>
                </View>
                <View style={{flex: 1, justifyContent: 'flex-start', marginBottom: 10, flexDirection: 'row'}}>
                    <Icon component={Icon} size={16} color="rgb(173, 14, 23)" name="phone" iconStyle={{ marginRight: 10 }}/> 
                    <Text
                        style={{
                            color: 'rgb(49, 51, 50)',
                            textAlign: 'left',
                            fontFamily: 'Roboto-Regular',
                            marginLeft: 10
                        }}>
                        {props.item.mobile}
                    </Text>
                </View>
            </View>
        </View>
        <View style={{flex: 5, justifyContent: 'center' }}>
            <Icon component={Icon} size={24} color="rgb(160, 158, 156)" name="chevron-right" /> 
        </View>
      </View>
    </Link>
  );
}

class Stores extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
      loading: true,
      loadingMore: false,
      filtering: false,
      refreshing: false,
      error: null,
      isFlushed: false
    };
  }
  
  componentDidMount() {
    this._fetchAllStores();
  }
  _fetchAllStores = () => {
    const { page } = this.state;
    let postParam = {
      page
    };
    API.getStores(postParam).then(response => {
      console.log(response.data)
      this.setState((prevState, nextProps) => ({
        data:
          page === 1
            ? Array.from(response.data.data)
            : [...this.state.data, ...response.data.data],
        loading: false,
        loadingMore: false,
        refreshing: false,
        isFlushed: false
      }));
    })
    .catch(error => {
      this.setState({ error, loading: false });
    });
  };
  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this._fetchAllStores();
      }
    );
  };
  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        this._fetchAllStores();
      }
    );
  };

  UNSAFE_componentWillReceiveProps(nextProps){
    if (!this.state.isFlushed && nextProps.location.state === 'flushData') {
      this.setState({
        isFlushed: true,
        loading: true,
      }, () => this._handleRefresh());
    }
  }

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader title="CHI NHÁNH" to={'/profile'} back={true} />
        </View>
        <View style={{flex: 8, backgroundColor: 'rgb(243, 243, 243)'}}>
          <View style={{flex: 28.65}}>
            {!this.state.loading ? (
              <FlatList
                contentContainerStyle={{
                  flexGrow: 1,
                }}
                numColumns={1}
                data={this.state.data}
                renderItem={({ item }) => (
                  <Item item={item} width={0.42} height={0.27} go={`/profile/orderhistory/${this.props.match.params.catId}`}/>
                )}
                keyExtractor={item => item.id.toString()}
                onRefresh={this._handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this._handleLoadMore}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
              />
            ) : (
              <View
                style={{flex: 1,
                justifyContent: 'center',
                alignItems:'center'
              }}>
                <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  touchableView: {
    width: 0.5 * screenWidth,
    height: '100%',
    justifyContent: 'flex-end',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 4,
    paddingBottom: 15
  },
  touchableViewOn: {
    width: 0.5 * screenWidth,
    height: '100%',
    justifyContent: 'flex-end',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(173,14,23)',
    paddingBottom: 15
  },
  textOn: {
    textAlign: 'center',
    fontSize: RFValue(14),
    fontFamily: 'Roboto-Regular',
  },
  textOff: {
    textAlign: 'center',
    fontSize: RFValue(14),
    fontFamily: 'Roboto-Regular',
  },
};
export default Stores;
