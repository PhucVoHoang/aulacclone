import React, { Component } from 'react';
import _ from 'lodash';
import {
  View,
  ScrollView,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {RFValue} from 'react-native-responsive-fontsize';
import API from '../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class OrderDetail extends Component {
    state = {
      loading: false,
      orderDetail: {}
    };
    componentDidMount() {
        this.setState({
            loading: true,
        })
        const orderId = _.get(this.props, 'location.state.orderId') ? _.get(this.props, 'location.state.orderId') : _.get(this.props, 'match.params.orderId');

        if (orderId) {
            API.getOrder(orderId).then(res => {
                this.setState({
                    orderDetail: _.get(res, 'data', {}),
                    loading: false,
                })
            }).catch(err => {
                console.log(err);
            })
        }
    }
    cancelOrder() {
      const orderId = _.get(this.props, 'location.state.orderId') ? _.get(this.props, 'location.state.orderId') : _.get(this.props, 'match.params.orderId');
      Alert.alert(
        "Huỷ đơn",
        "Bạn có chắc muốn huỷ đơn hàng này?",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK", onPress: () => API.cancelOrder(orderId).then(res => { this.props.history.push(this.props.location.state.go) }) }
        ],
        { cancelable: false }
      );
  
    }
    render() {
        return (
            <View style={{flex: 1}}>
              <View style={{height: 0.1 * screenHeight, width: '100%'}}>
                <NavigationHeader title="CHI TIẾT ĐƠN HÀNG" to={_.get(this.props, 'location.state.go', '/landing')} back={true} />
              </View>
              {!this.state.loading ? (
                <ScrollView style={{height: 0.9 * screenHeight, width: '100%'}}>
                <View
                  contentContainerStyle={{
                    flex: 1,
                  }}>
                    {this.state.orderDetail && _.get(this.state.orderDetail, 'items', []).map(item => (
                        <View style={styles.productView} key={item.id}>
                          <View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-start'}}>
                            <Image
                              style={{height: 0.1 * screenHeight, width: 0.12 * screenWidth}}
                              resizeMode="contain"
                              source={{uri: _.get(item, 'product.image_url')}}
                            />
                            <View style={{flexDirection: 'column'}}>
                              <Text
                                style={{
                                  marginLeft: 0.0338 * screenWidth,
                                  marginTop: 0.0235 * screenHeight,
                                }}>
                                {_.get(item, 'name')}
                              </Text>
                              
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
                              <Text
                                style={{
                                  marginRight: 5,
                                  marginTop: 0.0235 * screenHeight,
                                  color: 'rgb(173, 14, 23)',
                                }}>
                                {_.get(item, 'formated_total')} 
                              </Text>
                              <Text
                                style={{
                                marginTop: 0.0235 * screenHeight,
                                  color: 'rgb(48, 47, 46)',
                                }}>
                                x {_.get(item, 'qty_ordered')}
                              </Text>
                          </View>
                        </View>
                      ))}
                        <View style={styles.productView}>
                          <View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-start'}}>
                            <Text>Tổng tạm tính</Text>
                          </View>
                          <View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
                            <Text>
                                {this.state.orderDetail && this.state.orderDetail.formated_base_sub_total}
                            </Text>
                          </View>
                        </View>
                        {this.state.orderDetail && this.state.orderDetail.formated_base_discount_amount && (<View style={styles.productView}>
                          <View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-start'}}>
                            <Text>Giảm</Text>
                          </View>
                          <View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
                            <Text>
                                - {this.state.orderDetail.formated_base_discount_amount}
                            </Text>
                          </View>
                        </View>)}
                        
                  <View
                    style={{
                      marginTop: 0.0271 * screenHeight,
                      height: 0.267 * screenHeight,
                      flexDirection: 'column',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        shadowColor: '#000',
                        marginLeft: 0.04 * screenWidth,
                        width: 0.92 * screenWidth,
                        
                      }}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{marginTop: RFValue(6)}}>Tổng cộng</Text>
                        <Text style={{fontSize: RFValue(20), color: 'rgb(48, 47, 46)', fontWeight: 'bold'}}>
                        {this.state.orderDetail && this.state.orderDetail.formated_base_grand_total}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        shadowColor: '#000',
                        marginLeft: 0.04 * screenWidth,
                        width: 0.92 * screenWidth,
                      }}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{marginTop: RFValue(6), color: "rgb(160, 158, 156)"}}>Phương thức thanh toán</Text>
                        <Text style={{marginTop: RFValue(6), color: "rgb(160, 158, 156)"}}>
                          {this.state.orderDetail && this.state.orderDetail.payment_title}
                        </Text>
                      </View>
                    </View>
                    {this.state.orderDetail.status == 'pending' && <TouchableOpacity
                      onPress={this.cancelOrder.bind(this)}
                      style={{
                        width: 0.92 * screenWidth,
                        height: 0.076 * screenHeight,
                        marginLeft: 0.04 * screenWidth,
                        marginTop: 0.0421 * screenHeight,
                        backgroundColor: 'rgb(173, 14, 23)',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{color: 'white', fontWeight: 'bold'}}>HỦY ĐƠN HÀNG</Text>

                    </TouchableOpacity>}
                  </View>
                </View>
              </ScrollView>
              ) : (
                    <View
                      style={{flex: 1,
                      justifyContent: 'center',
                      alignItems:'center',
                    }}>
                      <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
                    </View>
              )}
            </View>
          );
    }
  
};
const styles = {
  productView: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 0.04 * screenWidth,
    paddingTop: 0.04 * screenWidth,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(160, 158, 156)',
    marginLeft: 0.04 * screenWidth,
    marginRight: 0.04 * screenWidth,
  },
};

export default OrderDetail;
