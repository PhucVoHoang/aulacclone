import React, {Component} from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {Link} from 'react-router-native';
import {resetCredentials} from '../../services/storageServices';
import {connect} from 'react-redux';
import {resetCart} from '../../redux/actions/cartActions';
import {resetCheckout} from '../../redux/actions/checkoutActions';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class editProfile extends Component {
  onLogout = () => {
    this.props.reduxResetCart();
    this.props.reduxResetCheckout();
    resetCredentials();
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{width: screenWidth, height: 0.11 * screenHeight}}>
          <NavigationHeader title="THIẾT LẬP TÀI KHOẢN" to="/profile" back={true} />
        </View>
        <ScrollView>
          <View style={profileStyle.categoryStyle}>
            <Text style={profileStyle.categorytextStyle}>TÀI KHOẢN</Text>
          </View>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/profile/myprofile',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Hồ sơ của tôi
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/profile/myaddress',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Địa chỉ
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/underconstruction',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Tài khoản / Thẻ ngân hàng
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link>
          <View style={profileStyle.categoryStyle}>
            <Text style={profileStyle.categorytextStyle}>CÀI ĐẶT</Text>
          </View>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/profile/chatsettings',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Cài đặt chat
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/profile/notificationsettings',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Cài đặt thông báo
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link>
          {/* <Link
            component={TouchableOpacity}
            to={{
              pathname: '/profile/privacysettings',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Cài đặt riêng tư
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link> */}
          {/* <Link
            component={TouchableOpacity}
            to={{
              pathname: '/underconstruction',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Danh sách bị chặn
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link> */}
          {/* <Link
            component={TouchableOpacity}
            to={{
              pathname: '/underconstruction',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Ngôn ngữ
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link> */}
          <View style={profileStyle.categoryStyle}>
            <Text style={profileStyle.categorytextStyle}>HỖ TRỢ</Text>
          </View>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/profile/blogpost',
              state: {
                go: '/profile/editprofile',
                postId: 7,
                headline: 'Điều khoản Âu Lạc'
              },
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Điều khoản Âu Lạc
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/underconstruction',
              state: {go: '/profile/editprofile'},
            }}
            style={profileStyle.contentStyle}>
            <Text style={{fontSize: 14, fontFamily: 'Roboto-Regular'}}>
              Xóa tài khoản
            </Text>
            <Image
              resizeMode="contain"
              style={profileStyle.arrowStyle}
              source={require('../../../assets/images/Profile/arrow.png')}
            />
          </Link>
          <View
            style={{
              width: screenWidth,
              height: 0.14 * screenHeight,
              backgroundColor: 'rgb(236,236,236)',
            }}>
            <Link
              component={TouchableOpacity}
              to={{
                pathname: '/',
                state: {go: '/profile/editprofile'},
              }}
              onPress={this.onLogout}
              style={{
                height: 0.0679 * screenHeight,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'rgb(173,14,23)',
                marginTop: 0.027 * screenHeight,
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 19,
                  fontFamily: 'Roboto-BoldCondensed',
                }}>
                ĐĂNG XUẤT
              </Text>
            </Link>
          </View>
        </ScrollView>
      </View>
    )
  }
};
const profileStyle = {
  categoryStyle: {
    backgroundColor: 'rgb(236,236,236)',
    width: screenWidth,
    height: 0.0543 * screenHeight,

    justifyContent: 'center',
  },
  categorytextStyle: {
    fontSize: 16,
    fontFamily: 'Roboto-BoldCondensed',
    marginLeft: 0.04 * screenWidth,
    marginRight: 0.04 * screenWidth,
  },
  contentStyle: {
    width: '92%',
    height: 0.0679 * screenHeight,
    flexDirection: 'row',
    marginLeft: 0.04 * screenWidth,
    marginRight: 0.04 * screenWidth,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(243,243,243)',
  },
  arrowStyle: {
    width: 0.03 * screenWidth,
    height: 0.03 * screenWidth,
  },
};

const mapStateToProps = (state) => {
  return {
    checkoutInfo: state.checkoutReducer.checkout,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxResetCart: () => dispatch(resetCart()),
    reduxResetCheckout: () => dispatch(resetCheckout())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(editProfile);