import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {Link} from 'react-router-native';
import Icon from 'react-native-vector-icons/AntDesign';

import API from '../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function Item(props) {
  return (
    <Link
      component={TouchableOpacity}
      to={{
        pathname: '/products',
        state: {
          going: props.gone,
          productId: props.item.product.id,
        },
      }}
      style={{
        // flex: 1,
        margin: 5,
        width: props.width * screenWidth,
        height: props.height * screenHeight,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        backgroundColor: 'white'
      }}>
        <View style={{flex: 25}}>
        <Image
          style={{
            width: undefined,
            height: undefined,
            flex: 1,
            resizeMode: 'contain',
          }}
          source={{uri: props.item.product.image_url}}
        />
      </View>
      <View style={{flex: 14, justifyContent: 'space-around', margin: 10}}>
        <Text
          style={{
            color: 'rgb(81,81,81)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
          }}>
          {props.item.product.name}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{
              color: 'rgb(255,30,2)',
              textAlign: 'left',
              fontFamily: 'Roboto-Bold',
            }}>
            {props.item.product.formated_price.replace(',00', '')}
          </Text>
          <TouchableOpacity 
            style={{ position: 'absolute', right: 0 }}
            // onPress={props.removed}
            onPress={() => props.removed(props.item.product.id)}
          >
            <Icon component={Icon} size={16} color='rgb(255,30,2)' name="heart" /> 
          </TouchableOpacity>
        </View>
      </View>
      
    </Link>
  );
}

class MyFavorite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
      loading: true,
      loadingMore: false,
      filtering: false,
      refreshing: false,
      error: null,
      isFlushed: false,
    };
  }
  
  componentDidMount() {
    this._fetchAllProducts();
  }

  _fetchAllProducts = () => {
    const { page } = this.state;
    let postParam = {
      page
    };

    API.getWishlist(postParam).then(response => {
      this.setState((prevState, nextProps) => ({
        data:
          page === 1
            ? Array.from(response.data)
            : [...this.state.data, ...response.data],
        loading: false,
        loadingMore: false,
        refreshing: false,
        isFlushed: false
      }));
    })
    .catch(error => {
      this.setState({ error, loading: false });
    });
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this._fetchAllProducts();
      }
    );
  };

  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        this._fetchAllProducts();
      }
    );
  };

  _removeFromWishList(productId) {
    API.addItemToWishlist(productId).then (response => {
      this._handleRefresh()
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps){
    if (!this.state.isFlushed && nextProps.location.state === 'flushData') {
      this.setState({
        isFlushed: true,
        loading: true,
      }, () => this._handleRefresh());
    }
  }

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
            <NavigationHeader title='DANH SÁCH YÊU THÍCH' to={'/profile'} back={true} />
        </View>
        <View style={{flex: 8}}>
          <View>
            <Text
              style={{
                marginTop: 12,
                marginLeft: 12,
                color: 'black',
                fontSize: 14,
                fontFamily: 'Roboto-Regular',
              }}>
              {this.state.data.length} Sản phẩm
            </Text>   
          </View>
          <View style={{flex: 28.65}}>
            {!this.state.loading ? (
              <FlatList
                contentContainerStyle={{
                  flexGrow: 1,
                  margin: 10
                }}
                numColumns={2}
                data={this.state.data}
                renderItem={({ item }) => (
                  <Item 
                    item={item} 
                    width={0.42} 
                    height={0.27} 
                    gone={`/profile/wishlist`}
                    removed={this._removeFromWishList.bind(this)}
                  />
                )}
                keyExtractor={item => item.id.toString()}
                onRefresh={this._handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this._handleLoadMore}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
              />
            ) : (
              <View
                style={{flex: 1,
                justifyContent: 'center',
                alignItems:'center'
              }}>
                <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

export default MyFavorite;
