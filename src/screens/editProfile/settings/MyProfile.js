import React, {Component} from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  Platform,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Keyboard,
  ActivityIndicator,
  TouchableWithoutFeedback
} from 'react-native';
import _ from 'lodash';
import {Dropdown} from 'react-native-material-dropdown';

import NavigationHeader from '../../../components/navigation/navigationHeader';
import {getUserCredentials, saveUserCredentials} from '../../../services/storageServices';
import API from '../../../services/api';
import WrongLogin from '../../../components/authentication/wrongLogin';
const wrong_img = require('../../../../assets/images/wrong.png');

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

let data = [
    {
      value: 'Male',
    },
    {
      value: 'Female',
    },
  ];

class MyProfile extends Component {
    state = {
      normal: true,
      store: false,
      express: false,
      date: new Date(),
      mode: 'date',
      show: false,
      popup: true,
      time: false,
      day: false,
      showDate: false,
      showTime: false,
      dateValue: 'dd/mm/yy',
      timeValue: '',
      address: null,
      city: null,
      first_name: null,
      last_name: null,
      phone: null,
      email: null,
      formLoading: false,
      errorPopup: false,
      profile: {},
      token: null
    };
    setDate = (event, date) => {
      date = date || this.state.date;
      const formattedDate = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
      this.setState({
        show: Platform.OS === 'ios',
        date,
        dateValue: formattedDate,
        showDate: false,
      });
    }
    setTime = (event, time) => {
      time = time || this.state.time;
  
      this.setState({
        show: Platform.OS === 'ios',
        time,
        dateValue: time.toString(),
        showTime: false,
      });
    }
    onSubmit = () => {
      const { last_name, first_name, email, phone, gender, password, password_confirmation } = this.state;
      if(this.state.last_name && this.state.first_name && this.state.email) {
        this.setState({formLoading: true});
        API.updateProfile({ last_name, first_name, email, phone, gender, password, password_confirmation })
            .then(res => {
                saveUserCredentials(
                    JSON.stringify(_.get(res, 'data')),
                    this.state.token,
                  ).then(() => this.setState({formLoading: false}));
            })
            .catch(err => this.setState({errorPopup: true, formLoading: false}))
      } else {
        this.setState({errorPopup: true, formLoading: false});
      }
    }
    componentDidMount() {
        getUserCredentials().then(credential => {
          if (credential.username && credential.password) {
            const profile = JSON.parse(credential.username);
            this.setState({
              first_name: profile.first_name,
              last_name: profile.last_name,
              phone: profile.phone,
              email: profile.email,
              gender: profile.gender,
              token: credential.password
            });
          }
        });
    }

    render() {
      return (
        <View style={{flex: 1, backgroundColor: 'rgb(255,255,255)'}}>
          <View style={{flex: 1.1}}>
            <NavigationHeader title="HỒ SƠ CỦA TÔI" to={{pathname: "/profile/editprofile", state: { go: "/" }}} back={true} />
          </View>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View
            style={{
              flex: 8.9,
              width: screenWidth * 0.92,
              marginLeft: 0.04 * screenWidth,
            }}>
              <KeyboardAvoidingView style={{ flex: 24, flexDirection: 'column',justifyContent: 'center',}} behavior="padding" enabled keyboardVerticalOffset={10}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.viewContainerStyle}>
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="Họ"
                  placeholderTextColor="rgb(151,151,151)"
                  onChangeText={(text) => {this.setState({last_name: text})}}
                  value={this.state.last_name}
                />
              </View>
              <View style={styles.viewContainerStyle}>
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="Tên"
                  placeholderTextColor="rgb(151,151,151)"
                  onChangeText={(text) => {this.setState({first_name: text})}}
                  value={this.state.first_name}
                />
              </View>
              <View style={styles.viewContainerStyle}>
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="Số điện thoại"
                  placeholderTextColor="rgb(151,151,151)"
                  onChangeText={(text) => {this.setState({phone: text})}}
                  value={this.state.phone}
                />
              </View>
              <View style={styles.viewContainerStyle}>
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="Email"
                  placeholderTextColor="rgb(151,151,151)"
                  onChangeText={(text) => {this.setState({email: text})}}
                  value={this.state.email}
                />
              </View>
              <Dropdown
                label="Giới tính"
                data={data}
                fontSize={12}
                containerStyle={styles.dropdownContainerStyle}
                inputContainerStyle={styles.dropdownInputStyle}
                onChangeText={(value, index, data) => {this.setState({gender: value})}}
                value={this.state.gender}
                />
              <View style={styles.viewContainerStyle}>
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="Mật khẩu"
                  placeholderTextColor="rgb(151,151,151)"
                  onChangeText={(text) => {this.setState({password: text})}}
                />
              </View>
              <View style={styles.viewContainerStyle}>
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="Xác nhận mật khẩu"
                  placeholderTextColor="rgb(151,151,151)"
                  onChangeText={(text) => {this.setState({password_confirmation: text})}}
                />
              </View>
              <TouchableOpacity
                onPress={this.onSubmit}
                style={{
                  width: 0.92 * screenWidth,
                  height: 0.076 * screenHeight,
                  marginTop: 0.0421 * screenHeight,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: this.state.formLoading ? '#CCCCCC' : 'rgb(236,178,45)',
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>HOÀN TẤT</Text>
              </TouchableOpacity>
            </ScrollView>
            </KeyboardAvoidingView>
          </View>
          </TouchableWithoutFeedback>
          {this.state.errorPopup ? (
            <WrongLogin
              visible={true}
              headerOne="SAI THÔNG TIN"
              img_src={wrong_img}
              headerTwo="Bạn đã nhập sai thông tin, vui lòng thử lại."
              buttonOne_text="TRỞ LẠI"
              buttonOne_bgColor="#6BBD12"
              buttonOne_disable={false}
              buttonOne_link="/profile/myprofile"
              clicked={() => {
                this.setState({errorPopup: false});
              }}
            />
          ) : null}
          {this.state.formLoading ? (
            <View style={styles.loading}>
              <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
            </View>  
          ) : null}
        </View>
      );
    }
  }
  
  const styles = {
    headerStyle: {
      color: 'rgb(173,14,23)',
      fontFamily: 'Roboto-Regular',
      fontSize: 16,
      marginTop: 0.027 * screenHeight,
    },
    viewContainerStyle: {
      height: 0.076 * screenHeight,
      width: 0.92 * screenWidth,
      backgroundColor: 'rgb(255,255,255)',
      borderRadius: 4,
      borderWidth: 1.3,
      borderColor: 'rgb(236,236,238)',
      marginTop: 0.0135 * screenHeight,
    },
    textInputStyle: {
      marginLeft: 0.04 * screenWidth,
      height: 0.076 * screenHeight,
      color: "#333"
    },
    dropdownInputStyle: {
      marginLeft: 0.04 * screenWidth,
      marginBottom: 0.015 * screenHeight,
      borderBottomColor: 'transparent',
      justifyContent: 'center',
    },
    dropdownContainerStyle: {
      borderRadius: 4,
      borderWidth: 1.3,
      borderColor: 'rgb(236,236,238)',
      height: 0.076 * screenHeight,
      justifyContent: 'center',
      marginTop: 0.0135 * screenHeight,
    },
    imgStyle: {
      height: 0.044 * screenHeight,
      width: 0.086 * screenWidth,
      marginTop: 5,
    },
    dropdownInputStyle: {
        marginLeft: 0.04 * screenWidth,
        marginBottom: 0.015 * screenHeight,
        borderBottomColor: 'transparent',
        justifyContent: 'center',
    },
    dropdownContainerStyle: {
        borderRadius: 4,
        borderWidth: 1.3,
        borderColor: 'rgb(236,236,238)',
        height: 0.076 * screenHeight,
        justifyContent: 'center',
        marginTop: 0.0135 * screenHeight,
    },
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      justifyContent: 'center',
      alignItems:'center',
    },
  };
  
  export default MyProfile;
  