import React, {Component, useState} from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  Platform,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  Switch
} from 'react-native';
import _ from 'lodash';

import NavigationHeader from '../../../components/navigation/navigationHeader';
import {getUserCredentials, saveUserCredentials} from '../../../services/storageServices';
import API from '../../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class NotificationSettings extends Component {
    state = {
        isEnabled1: true,
        isEnabled2: true,
        isEnabled3: true,
        isEnabled4: true,
    };
    
    render() {
      return (
        <View style={{flex: 1, backgroundColor: '#f7f7f7'}}>
          <View style={{flex: 1.1}}>
            <NavigationHeader title="CÀI ĐẶT THÔNG BÁO" to={{pathname: "/profile/editprofile", state: { go: "/" }}} back={true} />
          </View>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View
            style={{
              flex: 8.9,
              width: screenWidth,
            }}>
              <KeyboardAvoidingView style={{ flex: 24, flexDirection: 'column',justifyContent: 'center'}} behavior="padding" enabled keyboardVerticalOffset={10}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={shipping_style.viewContainerStyle}>
                    <Text>Thông báo</Text>
                    <Switch
                        trackColor={{ false: "#767577", true: "green" }}
                        thumbColor={this.state.isEnabled1 ? "white" : "#f4f3f4"}
                        ios_backgroundColor="#fff"
                        onValueChange={(e) => this.setState({ isEnabled1: !this.state.isEnabled1 })}
                        value={this.state.isEnabled1}
                    />
                </View>
                <View style={shipping_style.viewContainerStyle}>
                    <Text>Thông báo Email</Text>
                    <Switch
                        trackColor={{ false: "#767577", true: "green" }}
                        thumbColor={this.state.isEnabled2 ? "white" : "#f4f3f4"}
                        ios_backgroundColor="#fff"
                        onValueChange={(e) => this.setState({ isEnabled2: !this.state.isEnabled2 })}
                        value={this.state.isEnabled2}
                    />
                </View>
                <View style={shipping_style.viewContainerStyle}>
                    <Text>Cập nhật đơn hàng</Text>
                    <Switch
                        trackColor={{ false: "#767577", true: "green" }}
                        thumbColor={this.state.isEnabled2 ? "white" : "#f4f3f4"}
                        ios_backgroundColor="#fff"
                        onValueChange={(e) => this.setState({ isEnabled2: !this.state.isEnabled2 })}
                        value={this.state.isEnabled2}
                    />
                </View>
                <View style={shipping_style.viewContainerStyle}>
                    <Text>Chat</Text>
                    <Switch
                        trackColor={{ false: "#767577", true: "green" }}
                        thumbColor={this.state.isEnabled3 ? "white" : "#f4f3f4"}
                        ios_backgroundColor="#fff"
                        onValueChange={(e) => this.setState({ isEnabled3: !this.state.isEnabled3 })}
                        value={this.state.isEnabled3}
                    />
                </View>
            </ScrollView>
            </KeyboardAvoidingView>
          </View>
          </TouchableWithoutFeedback>
        </View>
      );
    }
  }
  
  const shipping_style = {
    headerStyle: {
      color: 'rgb(173,14,23)',
      fontFamily: 'Roboto-Regular',
      fontSize: 16,
      marginTop: 0.027 * screenHeight,
    },
    viewContainerStyle: {
      width: screenWidth,
      padding: 20,
      backgroundColor: 'white',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginTop: 10
    },
  };
  
  export default NotificationSettings;
  
