import React, {Component, useState} from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  Platform,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  Switch
} from 'react-native';
import _ from 'lodash';

import NavigationHeader from '../../../components/navigation/navigationHeader';
import {getUserCredentials, saveUserCredentials} from '../../../services/storageServices';
import API from '../../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class ChatSettings extends Component {
    state = {
        isEnabled: true
    };
    
    render() {
      return (
        <View style={{flex: 1, backgroundColor: '#f7f7f7'}}>
          <View style={{flex: 1.1}}>
            <NavigationHeader title="CÀI ĐẶT CHAT" to={{pathname: "/profile/editprofile", state: { go: "/" }}} back={true} />
          </View>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View
            style={{
              flex: 8.9,
              width: screenWidth,
            }}>
              <KeyboardAvoidingView style={{ flex: 24, flexDirection: 'column',justifyContent: 'center'}} behavior="padding" enabled keyboardVerticalOffset={10}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={shipping_style.viewContainerStyle}>
                    <Text>Cho Phép Chat từ trang Hồ sơ</Text>
                    <Switch
                        trackColor={{ false: "#767577", true: "green" }}
                        thumbColor={this.state.isEnabled ? "white" : "#f4f3f4"}
                        ios_backgroundColor="#fff"
                        onValueChange={(e) => this.setState({ isEnabled: !this.state.isEnabled })}
                        value={this.state.isEnabled}
                    />
              </View>
              
            </ScrollView>
            </KeyboardAvoidingView>
          </View>
          </TouchableWithoutFeedback>
        </View>
      );
    }
  }
  
  const shipping_style = {
    headerStyle: {
      color: 'rgb(173,14,23)',
      fontFamily: 'Roboto-Regular',
      fontSize: 16,
      marginTop: 0.027 * screenHeight,
    },
    viewContainerStyle: {
      width: screenWidth,
      padding: 20,
      backgroundColor: 'white',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginTop: 10
    },
  };
  
  export default ChatSettings;
  
