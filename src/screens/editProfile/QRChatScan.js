import React, { Component } from 'react';
 
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  Dimensions
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import {Link} from 'react-router-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import CHATAPI from '../../services/ChatApi';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class QRChatScan extends Component {
    state = {
        username: '',
        currentUser: ''
    };
    addFriend() {
        CHATAPI.getProfile().then(data => {
            this.setState({ currentUser: data.username });
            const friends = data.bio ? data.bio.split(',') : [];
            friends.push('"'+this.state.username.toString()+'"');
            CHATAPI.addFriends(friends.join()).then(res => {
                alert('Thêm bạn thành công.');
                this.props.history.push('/chat');
            }).catch(err => {
                console.log(err);
            })
        })
    }
    addForFriend() {
      if(this.state.username) {
        CHATAPI.getFriendInfo(this.state.username).then(data => {
          console.log('getFriendInfo', data);
            const friends = data.user.bio ? data.user.bio.split(',') : [];
            friends.push('"'+this.state.currentUser.toString()+'"');
            CHATAPI.addOtherFriends(data.user._id, friends.join()).then(res => {
                this.addFriend();
            });
        })
      }
  }
  onSuccess = e => {
    this.setState({ username: e.data });
    this.addFriend();
  };
 
  render() {
    return (
     <View style={{flex: 1}}>
         <View style={{flex: 1.1}}>
                <NavigationHeader title="THÊM BẠN" to={{pathname: "/chat", state: { go: "/addfriend", tab: 'contacts' }}} back={true} />
            </View>
        <View
            style={{
              flex: 8.9,
              width: screenWidth,
            }}>
        <QRCodeScanner
            containerStyle={styles.containerStyle}
            onRead={this.onSuccess}
            cameraStyle={styles.cameraContainer}
            cameraProps={{captureAudio: false}}
            showMarker={true}
        />
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 1 }}>
            <Link component={TouchableOpacity} to={'/addfriend/myqrcode'} style={styles.buttonTouchable}>
                <Text style={styles.buttonText}>QRCode của tôi </Text>
            </Link>
        </View>
      </View>
    );
  }
}

export default QRChatScan;
 
const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 16,
    color: 'black',
    textAlign: 'center'
  },
  buttonTouchable: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    width: '40%',
    backgroundColor: '#ECB22D',
    borderRadius: 40,
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  cameraContainer: {
    height: Dimensions.get('window').height,
    flex: 8.9, flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor:'black'
  }
});