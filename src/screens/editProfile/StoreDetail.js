import React, {Component} from 'react';
import MapView, { Marker } from 'react-native-maps';
import {
  View,
  Text,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {RFValue} from 'react-native-responsive-fontsize';
import Icon from 'react-native-vector-icons/Entypo';
import _ from 'lodash';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class StoreDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
        storeLoading: false,
        storeDetail: null,
        region: null
    };
  }
  
  componentDidMount() {
    this.setState({
      storeLoading: true,
    })
    const storeId = _.get(this.props, 'location.state.storeId');
    const storeDetail = _.get(this.props, 'location.state.storeDetail');
    if (storeId) {
        this.setState({
            storeDetail: storeDetail,
            storeLoading: false,
        })
    }  
  }

  render() {
    // console.log('im in render StoreDetails')
    // console.log(this.props.location.state.storeDetail)
    const { storeDetail } = this.state;
    const LATITUDE_DELTA = 0.009;
    const LONGITUDE_DELTA = LATITUDE_DELTA * (screenWidth / screenHeight);
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader title={storeDetail ? storeDetail.name : ''} to={'/profile/stores'} back={true} />
        </View>
        <View style={{flex: 8, backgroundColor: 'rgb(243, 243, 243)'}}>
          <View style={{flex: 28.65}}>
            {storeDetail ? (
                <View style={{ flex: 1 }}>
                    <MapView
                        initialRegion={{
                            latitude: Number(storeDetail.custom_field1),
                            longitude: Number(storeDetail.custom_field2),
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}
                        style={{ width: screenWidth, height: screenHeight/2 }}
                    >
                      <Marker
                        coordinate={{latitude: Number(storeDetail.custom_field1), longitude: Number(storeDetail.custom_field2)}}
                      >

                      </Marker>
                    </MapView>
                    <View style={{flex: 1, margin: 30 }}>
                        <View style={{marginBottom: 20, flexDirection: 'row'}}>
                            <Icon component={Icon} size={16} color="rgb(173, 14, 23)" name="location-pin" iconStyle={{ marginRight: 10 }}/> 
                            <View>
                                <Text
                                    style={{
                                        color: 'rgb(49, 51, 50)',
                                        textAlign: 'left',
                                        fontFamily: 'Roboto-Bold',
                                        marginLeft: 10,
                                        marginBottom: 10
                                    }}>
                                    Địa chỉ
                                </Text>
                                <Text
                                    style={{
                                        color: 'rgb(49, 51, 50)',
                                        textAlign: 'left',
                                        fontFamily: 'Roboto-Regular',
                                        marginLeft: 10
                                    }}>
                                    {storeDetail ? storeDetail.landmark : ''}
                                </Text>
                            </View>
                            
                        </View>
                        <View style={{marginBottom: 20, flexDirection: 'row'}}>
                            <Icon component={Icon} size={16} color="rgb(173, 14, 23)" name="phone" iconStyle={{ marginRight: 10 }}/> 
                            <View>
                                <Text
                                    style={{
                                        color: 'rgb(49, 51, 50)',
                                        textAlign: 'left',
                                        fontFamily: 'Roboto-Bold',
                                        marginLeft: 10,
                                        marginBottom: 10
                                    }}>
                                    Điện thoại
                                </Text>
                                <Text
                                    style={{
                                        color: 'rgb(49, 51, 50)',
                                        textAlign: 'left',
                                        fontFamily: 'Roboto-Regular',
                                        marginLeft: 10
                                    }}>
                                    {storeDetail ? storeDetail.mobile : ''}
                                </Text>
                            </View>
                        </View>
                        {storeDetail && <View style={{marginBottom: 10, flexDirection: 'row'}}>
                            <Icon component={Icon} size={16} color="rgb(173, 14, 23)" name="print" iconStyle={{ marginRight: 10 }}/> 
                            <View>
                                <Text
                                    style={{
                                        color: 'rgb(49, 51, 50)',
                                        textAlign: 'left',
                                        fontFamily: 'Roboto-Bold',
                                        marginLeft: 10,
                                        marginBottom: 10
                                    }}>
                                    Fax
                                </Text>
                                <Text
                                    style={{
                                        color: 'rgb(49, 51, 50)',
                                        textAlign: 'left',
                                        fontFamily: 'Roboto-Regular',
                                        marginLeft: 10
                                    }}>
                                    {storeDetail ? storeDetail.alternate_number : ''}
                                </Text>
                            </View>
                        </View>}
                    </View>
                </View>
              
            ) : (
              <View
                style={{flex: 1,
                justifyContent: 'center',
                alignItems:'center'
              }}>
                <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  touchableView: {
    width: 0.5 * screenWidth,
    height: '100%',
    justifyContent: 'flex-end',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 4,
    paddingBottom: 15
  },
  touchableViewOn: {
    width: 0.5 * screenWidth,
    height: '100%',
    justifyContent: 'flex-end',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(173,14,23)',
    paddingBottom: 15
  },
  textOn: {
    textAlign: 'center',
    fontSize: RFValue(14),
    fontFamily: 'Roboto-Regular',
  },
  textOff: {
    textAlign: 'center',
    fontSize: RFValue(14),
    fontFamily: 'Roboto-Regular',
  },
};
export default StoreDetail;
