import React, {Component} from 'react';
import {View, Alert, Image, Linking, TouchableOpacity, Dimensions, Text, ActivityIndicator, FlatList, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard} from 'react-native';
import moment from "moment";
import {RFValue} from 'react-native-responsive-fontsize';
import OneSignal from 'react-native-onesignal';
import NavigationHeader from '../../components/navigation/navigationHeader';
import Emoji from '../../components/chat/emoji';
import _ from 'lodash';
import CHATAPI from '../../services/ChatApi';
import API from '../../services/api';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import {getInternetCredentials} from '../../services/storageServices';
import emoji from '../../config/emoji';
import ImagePicker from 'react-native-image-picker';
import io from 'socket.io-client';
import Geolocation from '@react-native-community/geolocation';
import ImageContainer from '../../components/chat/Image';
import {connect} from 'react-redux';
import {pushMessageAction} from '../../redux/actions/chatActions';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const URL = 'ws://113.161.94.107:3000/websocket'

const options = {
  title: 'Select Image',
  customButtons: [{ name: 'Image', title: 'Choose Photo' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};


function Item(props) {
  const isMap = props.item.msg.indexOf('map:');
  console.log('isMap====', props.item.attachments);
  return (
    <TouchableOpacity
      style={[{
        width: screenWidth,
        paddingHorizontal: 40,
        paddingVertical: 20,
        borderRadius: 20,
      }, props.userId == props.item.u._id ? {alignItems: 'flex-end'} : {alignItems: 'flex-start'}]}
      onLongPress={() => props.userId == props.item.u._id ? props.removeMessage(props.item._id) : console.log()}
      >
      <View style={{ flexDirection: 'row' }}>
        {props.userId != props.item.u._id && <Icon component={Icon} size={28} color="rgba(34,34,34,0.5)" name="smile" style={{ marginRight: 10 }} />}
        {isMap >= 0 ? (<TouchableOpacity onPress={() => Linking.openURL(`https://www.google.com/maps/search/?api=1&query=${props.item.msg.replace('map:', '')}`)}>
        <View style={[{ maxWidth: screenWidth * 0.7 - 40, padding: 10, borderRadius: 10 }, props.userId == props.item.u._id ? {backgroundColor: 'rgb(236, 178, 45)'} : {backgroundColor: '#efefef'}]}><Text style={props.userId == props.item.u._id ? {color: '#fff'} : {color: 'black'}}>
              Gửi định vị
            </Text></View>
          </TouchableOpacity>) : (
            props.item.attachments && props.item.attachments.length > 0 ? (<ImageContainer
              file={{ image_url: props.item.attachments[0].image_url }}
              imageUrl={props.item.attachments[0].image_url}
              user={{id: props.userId, token: props.authToken}}
            />) : (emoji[props.item.msg] ? (<Image
                  resizeMode="contain"
                  style={{marginRight: 20, height: 150, width: 150}}
                  source={emoji[props.item.msg]}
              />) : (<View style={[{ maxWidth: screenWidth * 0.7 - 40, padding: 10, borderRadius: 10 }, props.userId == props.item.u._id ? {backgroundColor: 'rgb(236, 178, 45)'} : {backgroundColor: '#efefef'}]}>
            <Text style={props.userId == props.item.u._id ? {color: '#fff'} : {color: 'black'}}>
              {props.item.msg}
            </Text>
          </View>))
          )}
      </View>
      <View style={{ width: screenWidth * 0.7 - 40 }}>
        <Text style={[{ color: 'rgba(34,34,34,0.5)', fontSize: RFValue(10) }, , props.userId == props.item.u._id ? {textAlign: 'right'} : {textAlign: 'left'}]}>
          {moment(props.item.ts).isSame(moment(), 'day') ? moment(props.item.ts).format('h:mm a') : moment(props.item.ts).format('DD/MM/YYYY')}
        </Text>
      </View>
    </TouchableOpacity>
  )
}

class ChatScreen extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data: [],
        emojis: [],
        showEmoji: false,
        page: 1,
        loading: true,
        loadingMore: false,
        filtering: false,
        refreshing: false,
        error: null,
        isFlushed: false,
        message: null,
        userId: null,
        authToken: null,
        roomId: null,
        title: '',
        usernName: null,
        ws: new WebSocket(URL),
        members: [],
        myPosition: {
          latitude: 0,
          longitude: 0,
          timestamp: 0,
        },
      };
      this._displayImagePicker = this._displayImagePicker.bind(this);
      this._removeMessage = this._removeMessage.bind(this);
    }

    makeid() {
      var text = '';
      var possible =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  
      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
  
      return text;
    }

    UNSAFE_componentWillReceiveProps(nextProps){
      if (nextProps.chat.push) {
        this._fetchAll();
        this.props.pushMessage(false);
      }
    }
    
    componentDidMount() {
      getInternetCredentials().then(credential => {
        if(credential.username) {
          const userInfo = JSON.parse(credential.username);
          this.setState({ authToken: userInfo.authToken, userId: userInfo.userId, usernName: userInfo.me.username });
          const roomParams = _.get(this.props, 'location.state.friend') ? `roomName=${_.get(this.props, 'location.state.friend')}v${userInfo.me.username}` : `roomId=${this.props.match.params.roomId}`;
          CHATAPI.getChatroom(roomParams).then(res => {
            this.setState({ roomId: res.group._id });
            this._handleWebSocketSetup();
            this._getMembers();
            this._fetchAll();
          }).catch(err => {
            this._addNewRoom();
            this._fetchAll();
          })
          this._getEmojis();
          OneSignal.addEventListener('received', this._fetchAll);
        }
      });
    }

    _displayImagePicker = () => {
      ImagePicker.showImagePicker(options, (response) => {
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };
      
          if (response.uri) {
            const roomId = this.state.roomId ? this.state.roomId : this.props.match.params.roomId;
            const postParam = {
              rid: this.props.match.params.roomId,
              photo: response
            }
            const message = {"_id": "PetkdBgyCPGBNNxDg", "_updatedAt": "2020-06-30T20:10:10.363Z", "attachments": [{ "image_preview": response.data, "image_size": 2317928, "image_type": "image/jpeg", "image_url": "/file-upload/B9ZTb9AESLnnMfL4d/51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "title": "51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "title_link": "/file-upload/B9ZTb9AESLnnMfL4d/51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "title_link_download": true, "ts": "1970-01-01T00:00:00.000Z", "type": "file"}], "channels": [], "file": {"_id": "B9ZTb9AESLnnMfL4d", "name": "51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "type": "image/jpeg"}, "groupable": false, "mentions": [], "msg": "", "rid": "JGqkACw8DLACGQZhX", "ts": "2020-06-30T20:10:10.345Z", "u": {"_id": "ge3avAX9ye2MvQYFf", "name": "", "username": ""}}

            CHATAPI.addFile(postParam);
            this._fetchAll();
            const params = {
              text: "Send a photo",
              members: this.state.members,
              roomId,
            }
            API.sendChatNotification(params);

          }
        }
      });
    }

    _handleWebSocketSetup = () => {
      this.socket = io.connect('http://113.161.94.107:3000/websocket');
      // this.socket.on('connect', function(){
      //   console.log('Open!');
      // });
      // this.socket.on('event', function(data){
      //   console.log('data', data)
      // });
      // this.socket.on('disconnect', function(){
      //   console.log('disconnect!')
      // });

      console.log('_handleWebSocketSetup');
      const ws = new WebSocket(URL)
      ws.onopen = () => console.log('Open!')
      ws.onmessage = (event) => this._handleWSMessage(event, ws)
      ws.onerror = (error) => console.log('error!', error)
      ws.onclose = () => this._handleWebSocketSetup()
      let connectObject = {
          msg: 'connect', 
          version: '1', 
          support: ['1','pre2','pre1']
      }
      
      setTimeout(()=>{
        ws.send(JSON.stringify(connectObject));
      },100)

    }

    _handleWSMessage(e, socket) {
      let response = JSON.parse(e.data);
      console.log('response', response);

      // you have to pong back if you need to keep the connection alive
      // each ping from server need a 'pong' back
      if(response.msg == 'ping'){
          console.log('pong!');
          this._handleRefresh()
          socket.send(JSON.stringify({msg: 'pong'}));
          return;
      }

      // here you receive messages from server //notive the event name is: 'stream-room-messages'
      if(response.msg === 'changed' && response.collection === 'stream-room-messages'){
        console.log('msg received ' ,response.fields.args[0].msg, 'timestamp ', response.fields.args[0].ts.$date, 'from ' + response.fields.args[0].u.name);
        return;
      }

      // receive all messages which will only succeed if you already have messages
      // in the room (or in case you send the first message immediately you can listen for history correctly)
      if(response.msg === 'result' && response.result){
        if(response.result.messages){
          let allMsgs =  response.result.messages;
          console.log('-----previous msgs---------------');
          allMsgs.map(x => console.log(x))
          console.log('---------------------------------')
        }
      }
    }

    _addNewRoom() {
      const params = {
        name: `${_.get(this.props, 'location.state.friend')}v${this.state.usernName}`,
        members: [_.get(this.props, 'location.state.friend')]
      };
      console.log('_addNewRoom', params);
      CHATAPI.addChatroom(params).then(res => {
        this.setState({ roomId: res.group._id });
      }).catch(err => {
        console.log('====== _addNewRoom', err);
      })
    }

    _getMembers = () => {
      const roomParams = _.get(this.props, 'location.state.friend') ? `roomName=${_.get(this.props, 'location.state.friend')}v${this.state.usernName}` : `roomId=${this.props.match.params.roomId}`;
      const that = this;
      CHATAPI.getChatroomMembers(roomParams).then(res => {
        const user = _.filter(res.members, function(o) { return o._id != that.state.userId; });
        this.setState({ title: user[0].name });
      });
    }

    _sendLocation = () => {
      Geolocation.getCurrentPosition(
        position => {
          const initialPosition = position;
          //this.setState({initialPosition});
          if (initialPosition.coords)
            this._sendMessage(`map:${initialPosition.coords.latitude},${initialPosition.coords.longitude}`)
        },
        error => Alert.alert('Error', JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
      );
    }

    _getEmojis = () => {
      CHATAPI.getEmojis().then(res => {
        this.setState({ emojis: res.emojis.update });
      });
    }
    
    _fetchAll = () => {
        const { page } = this.state;
        let postParam = {
            page,
            id: this.state.roomId ? this.state.roomId : this.props.match.params.roomId
        };

        CHATAPI.getMessages(postParam).then(response => {
            CHATAPI.getMembers(postParam).then(res => {
              const that = this;
              this.setState((prevState, nextProps) => ({
                  members: _.map(_.filter(res.members, function(o) { return o._id !== that.state.userId; }), 'username')
              }));
            });
            this.setState((prevState, nextProps) => ({
                data:
                page === 1
                    ? Array.from(response.messages)
                    : [...this.state.data, ...response.messages],
                loading: false,
                loadingMore: false,
                refreshing: false,
                isFlushed: false
            }));
        })
        .catch(error => {
            this.setState({ error, loading: false });
        });
      };
      _handleRefresh = () => {
        this.setState(
          {
            page: 1,
            refreshing: true
          },
          () => {
            this._fetchAll();
          }
        );
      };
    _handleLoadMore = () => {
        this.setState(
          (prevState, nextProps) => ({
            page: prevState.page + 1,
            loadingMore: true
          }),
          () => {
            this._fetchAll();
          }
        );
    };
    _sendMessage(message) {
      if (message) {
          const roomId = this.state.roomId ? this.state.roomId : this.props.match.params.roomId;
          const postParam = {
            roomId,
            text: message,
          }
          CHATAPI.addMessage(postParam).then(response => {
            const { page } = this.state;
            const params = {
              text: message,
              members: this.state.members,
              roomId: roomId,
              screen: `/chat/${roomId}`
            }
            API.sendChatNotification(params);
            this.setState((prevState, nextProps) => ({
                data: [...this.state.data, ...[response.message]],
                loading: false,
                loadingMore: false,
                refreshing: false,
                isFlushed: false,
                message: null,
                showEmoji: false 
            }));
          })
          .catch(error => {
              this.setState({ error, loading: false });
          });
      }
    }

    _removeMessage(msgId) {
      if (msgId) {
        Alert.alert(
          "Thông báo",
          "Bạn muốn xoá tin nhắn này?",
          [
            {
              text: "Huỷ",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "Đồng ý", onPress: () => {
              const roomId = this.state.roomId ? this.state.roomId : this.props.match.params.roomId;
              const postParam = {
                roomId,
                msgId,
              }
              CHATAPI.removeMessage(postParam).then(response => {
                let messages = _.cloneDeep(this.state.data);
                messages = _.filter(messages, function(currentObject) {
                  return currentObject._id !== msgId;
                });
                this.setState((prevState, nextProps) => ({
                    data: messages,
                    loading: false,
                    loadingMore: false,
                    refreshing: false,
                    isFlushed: false,
                    message: null,
                    showEmoji: false 
                }));
              })
              .catch(error => {
                  this.setState({ error, loading: false });
              });
            } }
          ],
          { cancelable: false }
        )
      }
    }
    render() {  
        return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == "ios" ? "padding" : "height"} enabled>
      <View style={{flex: 1}}>
        <View style={{height: RFValue(80)}}>
          <NavigationHeader title={_.get(this.props, 'location.state.data.usersCount', 2) > 2 ? this.props.location.state.data.name : this.state.title} to={{pathname: "/chat", state: { go: "/" }}} back={true} />
        </View>
        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss(); this.setState({ showEmoji: false })}}>
          <>
        <View style={{flex: 8.4}}>
            {!this.state.loading ? (
                <FlatList
                contentContainerStyle={{
                    flexGrow: 1,
                }}
                ref={ref => this.flatList = ref}
                onContentSizeChange={() => this.flatList.scrollToEnd({animated: true})}
                onLayout={() => this.flatList.scrollToEnd({animated: true})}
                numColumns={1}
                data={this.state.data}
                renderItem={({ item }) => (
                    <Item item={item} userId={this.state.userId} authToken={this.state.authToken} width={0.42} height={0.27} removeMessage={this._removeMessage}/>
                )}
                keyExtractor={item => item._id.toString()}
                //onRefresh={this._handleRefresh}
                //refreshing={this.state.refreshing}
                onEndReachedThreshold={0.5}
                />
            ) : (
                <View
                  style={{flex: 1,
                  justifyContent: 'center',
                  alignItems:'center'
                }}>
                  <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
                </View>
            )}
            </View>
            <View
            style={{
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 20,
              borderTopWidth: 1,
              borderTopColor: 'rgba(34,34,34,0.3)',
            }}>
              <TextInput
                style={{width: screenWidth - 150, marginVertical: 10, paddingVertical: 10, color: "#333" }}
                placeholder="Gửi tin nhắn"
                placeholderTextColor="rgb(151,151,151)"
                value={this.state.message}
                onChangeText={(text) => {this.setState({message: text})}}
              />
              <TouchableOpacity onPress={() => this._sendLocation()} style={{ marginRight: 15 }}>
                <Icon component={Icon} size={24} color="rgba(34,34,34,0.5)" name="map-pin" /> 
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._displayImagePicker()} style={{ marginRight: 15 }}>
                <Icon component={Icon} size={24} color="rgba(34,34,34,0.5)" name="paperclip" /> 
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setState({ showEmoji: !this.state.showEmoji })} style={{ marginRight: 15 }}>
                <Icon component={Icon} size={24} color="rgba(34,34,34,0.5)" name="smile" /> 
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._sendMessage(this.state.message)}>
                <Icon component={Icon} size={24} color="rgba(34,34,34,0.5)" name="send" /> 
              </TouchableOpacity>
          </View>
          </>
          </TouchableWithoutFeedback>
          <View
            style={{
              flex: 0.1,
              width: '100%',
              backgroundColor: 'white',
              flexDirection: 'row',
            }}></View>
          
      </View>
      {this.state.showEmoji && <Emoji style={{ height: 200, position: 'absolute', bottom: 0, left: 0}} emojis={this.state.emojis} _sendMessage={this._sendMessage.bind(this)}/>}
       </KeyboardAvoidingView>
        );
      }
};

const mapStateToProps = (state) => ({
  chat: state.chatReducer
});

const mapDispatchToProps = dispatch => ({
  pushMessage: () => {
    return dispatch(pushMessageAction());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatScreen);

