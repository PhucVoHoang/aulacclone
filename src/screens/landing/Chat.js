import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {View, Text, Dimensions, TouchableOpacity, Image} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import Footer from '../../components/navigation/Footer';
import {RFValue} from 'react-native-responsive-fontsize';
import Chatting from '../../components/chat/Chatting';
import Contacts from '../../components/chat/Contacts';
import {getUserCredentials, setInternetCredentials, getInternetCredentials} from '../../services/storageServices';
import CHATAPI from '../../services/ChatApi';
import {registerChatAction} from '../../redux/actions/chatActions';

class Chat extends Component {
  state = {
    chat: true,
    contacts: false,
    visitor_token: null,
    visitor_username: null,
    data: [],
    page: 1,
    loading: true,
    loadingMore: false,
    filtering: false,
    refreshing: false,
    error: null,
    isFlushed: false,
    users: []
  };

  componentDidMount() {
    this.setState({ contacts: _.get(this.props, 'location.state.tab', 'chat') == 'contacts', chat: _.get(this.props, 'location.state.tab', 'chat') != 'contacts' });
    getInternetCredentials().then(credential => {
      if(credential.username) {
        const userInfo = JSON.parse(credential.username);
        this.setState({ visitor_token: userInfo.authToken, visitor_username: userInfo.userId });
        this._fetchAll();
        this._fetchContact();
      } else {
        getUserCredentials().then(credential => {
          if (credential.username && credential.password) {
            const userInfo = JSON.parse(credential.username);
            const visitor =  {
              "name": userInfo.name,
              "email": userInfo.email.toLowerCase(),
              "username": userInfo.email.toLowerCase().split('@')[0].replace(/[^a-zA-Z0-9]/g, ""),
              "pass": userInfo.email.toLowerCase()
            }
            CHATAPI.getVisitor({"user": userInfo.email.toLowerCase(), "password": userInfo.email.toLowerCase()}).then(res => {
              this.setState({
                visitor_token: res.data.authToken,
                visitor_username: res.data.userId
              })
              setInternetCredentials(JSON.stringify(_.get(res, 'data')),res.data.authToken);
              this.props.reduxRegisterChatAction(res.user);
              this._fetchAll();
              this._fetchContact();
            }).catch(err => {
              
              CHATAPI.register(visitor).then(res => {
                console.log('getUserCredentials', res)
              }).catch(err => {
                console.log(err);
              })
            })
          }
        });
      }
    });
    
  }

  _getMembers = async (roomParams) => {
    const that = this;
    return await CHATAPI.getChatroomMembers(`roomId=${roomParams}`).then(res => {
      const user = _.filter(res.members, function(o) { return o._id != that.state.visitor_username; });
      return user[0].name;
    });
  }

  _leaveRoom = (roomId) => {
    CHATAPI.leaveChatroom({roomId}).then(res => {
      this.setState((prevState, nextProps) => ({
        data: _.remove(this.state.data, function(currentObject) {
          return currentObject._id !== roomId;
        }),
        loading: false,
        loadingMore: false,
        refreshing: false,
        isFlushed: false
      }));
    });
  }

  _fetchContact = () => {
    CHATAPI.getProfile().then(data => {
      const friends = data.bio;
      CHATAPI.getFriends(friends).then(res => {
        this.setState({users: res.users});
      }).catch(err => {
        console.log(err);
      })
    }).catch(err => {
      console.log(err);
    })
  }

  _fetchAll = () => {
      const { page } = this.state;
      let postParam = {
        page,
      };
      console.log('getChatrooms ======', postParam)
      CHATAPI.getChatrooms(postParam).then(response => {
        this.setState((prevState, nextProps) => ({
          data:
            page === 1
              ? Array.from(response.groups)
              : [...this.state.data, ...response.groups],
          loading: false,
          loadingMore: false,
          refreshing: false,
          isFlushed: false
        }));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };
  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this._fetchAll();
      }
    );
  };
  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        this._fetchAll();
      }
    );
  };

  _hasBack = () => {
    if (this.props) {
      if (this.props.location.state && this.props.location.state.back) {
        return this.props.location.state.back
      }
    }
    return false;
  }

  _backLink = () => {
    return _.get(this.props, 'location.state.go', '/landing')
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader to={this._backLink()} chat={true} back={this._hasBack()}/>
        </View>
        <View style={{flex: 8}}>
          <View style={{height: 60, flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => {
                this.setState({chat: false});
                this.setState({contacts: true});
              }}
              style={
                this.state.contacts
                  ? chat_style.containerOn
                  : chat_style.containerOff
              }>
              <Text
                style={
                  this.state.contacts
                    ? chat_style.buttonOn
                    : chat_style.buttonOff
                }>
                Danh bạ
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({chat: true});
                this.setState({contacts: false});
              }}
              style={
                this.state.chat
                  ? chat_style.containerOn
                  : chat_style.containerOff
              }>
              <Text
                style={
                  this.state.chat ? chat_style.buttonOn : chat_style.buttonOff
                }>
                Tin nhắn
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{flex: 8}}>
            {this.state.chat ? <Chatting props={this.state} _handleLoadMore={this._handleLoadMore} _handleRefresh={this._handleRefresh} _leaveRoom={this._leaveRoom.bind(this)} _getMembers={this._getMembers.bind(this)}/> : null}
            {this.state.contacts ? <Contacts users={this.state.users} /> : null}
          </View>
        </View>
        <View style={{flex: 1}}>
          <Footer
            home={false}
            square={false}
            comments={true}
            alarm={false}
            profile={false}
          />
        </View>
      </View>
    );
  }
}
const chat_style = {
  containerOn: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderBottomColor: 'rgb(173,14,23)',
    borderBottomWidth: 2,
  },
  containerOff: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderBottomColor: 'rgb(160,158,156)',
    borderBottomWidth: 1,
  },
  buttonOn: {
    fontSize: RFValue(16),
    color: 'rgb(173,14,23)',
    fontFamily: 'Roboto-Regular',
    paddingBottom: 8
  },
  buttonOff: {
    fontSize: RFValue(16),
    color: 'rgb(160,158,156)',
    fontFamily: 'Roboto-Regular',
    paddingBottom: 8
  },
};

const mapStateToProps = (state) => {
  return {
    auth: state.authReducer,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxRegisterChatAction: (info) => dispatch(registerChatAction(info)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);
