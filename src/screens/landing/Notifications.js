import React from 'react';
import {View, TouchableOpacity, Text, Image, Dimensions} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import Footer from '../../components/navigation/Footer';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';
import AppConfig from '../../config/AppConfig';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const Notifications = () => {
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1.1}}>
        <NavigationHeader title="TIN TỨC" to="/landing" back={false} />
      </View>
      <View style={{flex: 8}}>
      {/* <Link
          component={TouchableOpacity}
          to={{
            pathname: `/messages`,
            state: {
              go: '/notification',
              headline: 'Thông báo'
            },
          }}
          style={styles.touchableView}>
          <Image
            resizeMode="contain"
            style={{width: 0.1256 * screenWidth, height: 0.0718 * screenHeight}}
            source={require('../../../assets/images/Landing/gift.png')}
          />
          <View style={styles.textContainer}>
            <Text style={{fontSize: RFValue(16), fontFamily: 'Roboto-Bold'}}>
              Thông báo
            </Text>
            <Text style={{fontSize: RFValue(14), fontFamily: 'Roboto-Regular'}}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut.
            </Text>
          </View>
        </Link> */}
        <Link
          component={TouchableOpacity}
          to={{
            pathname: `/profile/blog/${AppConfig.khuyenmai}`,
            state: {
              go: '/notification',
              headline: 'Khuyến mãi'
            },
          }}
          style={styles.touchableView}>
          <Image
            resizeMode="contain"
            style={{width: 0.1256 * screenWidth, height: 0.0718 * screenHeight}}
            source={require('../../../assets/images/Landing/gift.png')}
          />
          <View style={styles.textContainer}>
            <Text style={{fontSize: RFValue(16), fontFamily: 'Roboto-Bold'}}>
              Khuyến mãi
            </Text>
            <Text style={{fontSize: RFValue(14), fontFamily: 'Roboto-Regular'}}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut.
            </Text>
          </View>
        </Link>
        <Link
          component={TouchableOpacity}
          to={{
            pathname: `/profile/blog/${AppConfig.sanphammoi}`,
            state: {
              go: '/notification',
              headline: 'Sản phẩm mới'
            },
          }}
          style={styles.touchableView}>
          <Image
            resizeMode="contain"
            style={{width: 0.1256 * screenWidth, height: 0.0718 * screenHeight}}
            source={require('../../../assets/images/Landing/product.png')}
          />
          <View style={styles.textContainer}>
            <Text style={{fontSize: RFValue(16), fontFamily: 'Roboto-Bold'}}>
              Sản phẩm mới
            </Text>
            <Text style={{fontSize: RFValue(14), fontFamily: 'Roboto-Regular'}}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut.
            </Text>
          </View>
        </Link>
        <Link component={TouchableOpacity}
              to={{
                pathname: `/profile/blog/${AppConfig.flashsale}`,
                state: {
                  go: '/notification',
                  headline: 'Chương trình Flash sale'
                },
              }}
              style={styles.touchableView}>
          <Image
            resizeMode="contain"
            style={{width: 0.1256 * screenWidth, height: 0.0718 * screenHeight}}
            source={require('../../../assets/images/Landing/flash.png')}
          />
          <View style={styles.textContainer}>
            <Text style={{fontSize: RFValue(16), fontFamily: 'Roboto-Bold'}}>
              Chương trình Flash sale
            </Text>
            <Text style={{fontSize: RFValue(14), fontFamily: 'Roboto-Regular'}}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut.
            </Text>
          </View>
        </Link>
        <Link component={TouchableOpacity}  to={{
          pathname: `/profile/blog/${AppConfig.hoatdong}`,
          state: {
            go: '/notification',
            headline: 'Hoạt động Âu Lạc'
          },
        }}
              style={styles.touchableView}>
          <Image
            resizeMode="contain"
            style={{width: 0.1256 * screenWidth, height: 0.0718 * screenHeight}}
            source={require('../../../assets/images/Landing/aulac.png')}
          />
          <View style={styles.textContainer}>
            <Text style={{fontSize: RFValue(16), fontFamily: 'Roboto-Bold'}}>
              Hoạt động Âu Lạc
            </Text>
            <Text style={{fontSize: RFValue(14), fontFamily: 'Roboto-Regular'}}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut.
            </Text>
          </View>
        </Link>
      </View>
      <View style={{flex: 1}}>
        <Footer
          home={false}
          square={false}
          comments={false}
          alarm={true}
          profile={false}
        />
      </View>
    </View>
  );
};
const styles = {
  touchableView: {
    width: 0.92 * screenWidth,
    height: 0.0848 * screenHeight,
    marginTop: 0.03 * screenHeight,
    marginLeft: 0.04 * screenWidth,
    marginRight: 0.04 * screenWidth,
    flexDirection: 'row',
  },
  textContainer: {
    flexDirection: 'column',
    marginLeft: 0.025 * screenWidth,
    flexGrow: 1,
    flex: 1,
  },
};
export default Notifications;
