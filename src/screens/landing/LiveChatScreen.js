import React, {Component} from 'react';
import {Alert, View, Image, TouchableOpacity, Dimensions, Text, ActivityIndicator, FlatList, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard} from 'react-native';
import moment from "moment";
import {RFValue} from 'react-native-responsive-fontsize';
import NavigationHeader from '../../components/navigation/navigationHeader';
import Emoji from '../../components/chat/emoji';
import _ from 'lodash';
import CHATAPI from '../../services/ChatApi';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import {getInternetCredentials} from '../../services/storageServices';
import emoji from '../../config/emoji';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {pushMessageAction} from '../../redux/actions/chatActions';
import ImageContainer from '../../components/chat/Image';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const URL = 'ws://113.161.94.107:3000/websocket'

const options = {
  title: 'Select Image',
  customButtons: [{ name: 'Image', title: 'Choose Photo' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};


function Item(props) {
  if(props.item.msg == 'connected' || !props.item._id) return null;
  return (
    <TouchableOpacity
      style={[{
        width: screenWidth,
        paddingHorizontal: 40,
        paddingVertical: 20,
        borderRadius: 20,
      }, (props.userId == props.item.token || props.name == props.item.u.name) ? {alignItems: 'flex-end'} : {alignItems: 'flex-start'}]}
      >
      <View style={{ flexDirection: 'row' }}>
        {(props.name !== props.item.u.name) && <Icon component={Icon} size={28} color="rgba(34,34,34,0.5)" name="smile" style={{ marginRight: 10 }} />}
        {props.item.attachments && props.item.attachments.length > 0 ? (
          <ImageContainer
            file={{ image_url: props.item.attachments[0].image_url }}
            imageUrl={props.item.attachments[0].image_url}
            user={{id: props.userId, token: props.authToken}}
          />
        ) : (emoji[props.item.msg] ? (<Image
                resizeMode="contain"
                style={{marginRight: 20, width:50, height: 150, width: 150}}
                source={emoji[props.item.msg]}
            />) : (<View style={[{ maxWidth: screenWidth * 0.7 - 40, padding: 10, borderRadius: 10 }, props.userId == props.item.token ? {backgroundColor: 'rgb(236, 178, 45)'} : {backgroundColor: '#efefef'}]}>
          <Text style={(props.userId == props.item.token || props.name == props.item.u.name) ? {color: '#fff'} : {color: 'black'}}>
            {props.item.msg}
          </Text>
        </View>))}
      </View>
      {props.item._id == '0' && (<View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity onPress={() => props._sendMessage('Về sản phẩm')} style={{ width: screenWidth * 0.7 - 40, borderRadius: 10, borderColor: '#ECB22D', borderWidth: 1, padding: 10, marginTop: 10 }}>
          <Text style={{ color: '#ECB22D', textAlign: 'center' }}>Về sản phẩm</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => props._sendMessage('Về đơn hàng')} style={{ width: screenWidth * 0.7 - 40, borderRadius: 10, borderColor: '#ECB22D', borderWidth: 1, padding: 10, marginTop: 10 }}>
          <Text style={{ color: '#ECB22D', textAlign: 'center' }}>Về đơn hàng</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => props._sendMessage('Về hóa đơn')} style={{ width: screenWidth * 0.7 - 40, borderRadius: 10, borderColor: '#ECB22D', borderWidth: 1, padding: 10, marginTop: 10 }}>
          <Text style={{ color: '#ECB22D', textAlign: 'center' }}>Về hóa đơn</Text>
        </TouchableOpacity>
      </View>)}
    </TouchableOpacity>
  )
}

class LivehatScreen extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data: [{
          "_id": "0",
          "rid": "KuACMJ5MpN6SfAFWg",
          "msg": "Xin chào, tôi có thể hỗ trợ được gì cho bạn?",
          "token": "iNKE8a6k6cjbqWhWd",
          "alias": "Livechat Agent",
          "ts": moment(),
          "u": {
            "_id": "YgEoq2djbGdjjZnsL",
            "username": "guest-4",
            "name": "Livechat Agent"
          },
        }],
        name: '',
        email: '',
        emojis: [],
        showEmoji: false,
        page: 1,
        loading: false,
        loadingMore: false,
        filtering: false,
        refreshing: false,
        error: null,
        isLive: false,
        isFlushed: false,
        message: null,
        userId: null,
        authToken: null,
        roomId: null,
        title: '',
        ws: new WebSocket(URL),
        departments: [],
        credential: null
      };
    }

    UNSAFE_componentWillReceiveProps(nextProps){
      if (nextProps.chat.push) {
        this._fetchAll();
        this.props.pushMessage(false);
      }
    }
    
    componentDidMount() {
      getInternetCredentials().then(credential => {
        if(credential.username) {
          const userInfo = JSON.parse(credential.username);
          this.setState({ authToken: userInfo.authToken, userId: userInfo.userId, name: userInfo.me.name, email: userInfo.me.emails[0].address, credential: credential.username });
          const visitor = { "visitor": {
            "name": userInfo.me.name,
            "email": userInfo.me.emails[0].address,
            "token": userInfo.userId,
            "phone": "55 51 5555-5555",
            "customFields": [{
              "key": "address",
              "value": "Aulac.Chat street",
              "overwrite": true
            }]
          }};
          this._loginLivechat(userInfo.userId, visitor);  
          this._getEmojis();
         
        }
      });
    }

    _displayImagePicker = () => {
      ImagePicker.showImagePicker(options, (response) => {
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };
          console.log('_displayImagePicker ', this.state.roomId);
          if (response.uri) {
            const postParam = {
              rid: this.state.roomId,
              photo: response
            }
            const message = {"_id": "PetkdBgyCPGBNNxDg", "_updatedAt": "2020-06-30T20:10:10.363Z", "attachments": [{ "image_preview": response.data, "image_size": 2317928, "image_type": "image/jpeg", "image_url": "/file-upload/B9ZTb9AESLnnMfL4d/51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "title": "51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "title_link": "/file-upload/B9ZTb9AESLnnMfL4d/51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "title_link_download": true, "ts": "1970-01-01T00:00:00.000Z", "type": "file"}], "channels": [], "file": {"_id": "B9ZTb9AESLnnMfL4d", "name": "51643BAD-2C44-4A79-883C-BDD8AC539A82.jpg", "type": "image/jpeg"}, "groupable": false, "mentions": [], "msg": "", "rid": "JGqkACw8DLACGQZhX", "ts": "2020-06-30T20:10:10.345Z", "u": {"_id": "ge3avAX9ye2MvQYFf", "name": "", "username": ""}}

            CHATAPI.addFile(postParam).then(response => {
              this.setState((prevState, nextProps) => ({
                data: [...this.state.data, ...[message]],
                loading: false,
                loadingMore: false,
                refreshing: false,
                isFlushed: false,
                message: null
            }));
            })
          }
        }
      });
    }

    _loginLivechat(token, visitor){
      CHATAPI.getLivechatVisitor(token).then(res => {
        console.log('getLivechatVisitor', res)
        if(!res.visitor) {
          console.log('registerLivechatVisitor')
          CHATAPI.registerLivechatVisitor(visitor).then(response => {
            console.log('registerLivechatVisitor', response)
            this._loginLivechat(token, visitor);
          }).catch(err => {
            console.log(err);
          })
        } else {
          //get room
          let roomParams = { token };
          this.getItem('livechat').then(res => {
            const rid = res;
            console.log('AsyncStorage', res)
            if(rid) {
              roomParams.rid = rid;
            } 
            if(!roomParams.rid) {
              CHATAPI.getLivechatList().then(response => {
                console.log('getLivechatList', response)
              }).catch(err => {
                console.log('getLivechatList', err);
              })
            }
            console.log('roomParams', roomParams)
            this._addNewRoom(roomParams);
          });

          
        }
      }).catch(err => {
        console.log('getLivechatVisitor err', err)
      })
    }

    _handleWebSocketSetup = () => {
      console.log('_handleWebSocketSetup');
      const ws = new WebSocket(URL)
      ws.onopen = () => console.log('Open!')
      ws.onmessage = (event) => this._handleWSMessage(event, ws)
      ws.onerror = (error) => console.log('error!', error)
      ws.onclose = () => this._handleWebSocketSetup()
      let connectObject = {
          msg: 'connect', 
          version: '1', 
          support: ['1','pre2','pre1']
      }
      
      setTimeout(()=>{
        ws.send(JSON.stringify(connectObject));
      },200)

      this.setState({ws})
    }

    _handleWSMessage(e, socket) {
      let response = JSON.parse(e.data);
      console.log('response', response);

      // you have to pong back if you need to keep the connection alive
      // each ping from server need a 'pong' back
      if(response.msg == 'ping'){
          console.log('pong!');
          this._handleRefresh()
          socket.send(JSON.stringify({msg: 'pong'}));
          return;
      }

      // here you receive messages from server //notive the event name is: 'stream-room-messages'
      if(response.msg === 'changed' && response.collection === 'stream-room-messages'){
        console.log('msg received ' ,response.fields.args[0].msg, 'timestamp ', response.fields.args[0].ts.$date, 'from ' + response.fields.args[0].u.name);
        return;
      }

      // receive all messages which will only succeed if you already have messages
      // in the room (or in case you send the first message immediately you can listen for history correctly)
      if(response.msg === 'result' && response.result){
        if(response.result.messages){
          let allMsgs =  response.result.messages;
          console.log('-----previous msgs---------------');
          allMsgs.map(x => console.log(x))
          console.log('---------------------------------')
        }
      }
    }

    _addNewRoom(params) {
      console.log('_addNewRoom', params)
      CHATAPI.getLivechatRoom(params).then(res => {
        this.setState({ roomId: res.room._id });
        this.setItem('livechat', res.room._id)
        this._handleWebSocketSetup();
        this._fetchAll();
      }).catch(err => {
        console.log('======', err);
      })
    }

    getItem = async(item) => {
        try {
            const value = await AsyncStorage.getItem(item);
            return JSON.parse(value);
        } catch (error) {
            return null;
        }
    };
  
    setItem = async(item,value)=>{
        try {
            await AsyncStorage.setItem(item, JSON.stringify(value));
        } catch (error) {
            console.log("SetItem error ",error)
            return null;
        }
    }

    _getEmojis = () => {
      CHATAPI.getEmojis().then(res => {
        console.log('getEmojis', res)
        this.setState({ emojis: res.emojis.update });
      });
    }
    
    _fetchAll = () => {
        const { page } = this.state;
        let postParam = {
            page,
            rid: this.state.roomId ? this.state.roomId : this.props.match.params.roomId,
            token: this.state.userId
        };

        CHATAPI.getLivechatMessages(postParam).then(response => {

            const firstMessage = {
              "_id": "0",
              "rid": "KuACMJ5MpN6SfAFWg",
              "msg": "Xin chào, tôi có thể hỗ trợ được gì cho bạn?",
              "token": "iNKE8a6k6cjbqWhWd",
              "alias": "Livechat Agent",
              "ts": "2018-09-14T13:31:33.201Z",
              "u": {
                "_id": "YgEoq2djbGdjjZnsL",
                "username": "guest-4",
                "name": "Livechat Agent"
              },
            };
            let messages = response.messages;
            messages.push(firstMessage);
            this.setState((prevState, nextProps) => ({
                data:
                page === 1
                    ? Array.from(_.reverse(messages))
                    : [...this.state.data, ...response.messages],
                loading: false,
                loadingMore: false,
                refreshing: false,
                isFlushed: false,
                isLive: true
            }));
        })
        .catch(error => {
            console.log('voday error', error);
            this.setState({ error, loading: false, isLive: false });
        });
      };
      _handleRefresh = () => {
        this.setState(
          {
            page: 1,
            refreshing: true
          },
          () => {
            this._fetchAll();
          }
        );
      };
    _handleLoadMore = () => {
        this.setState(
          (prevState, nextProps) => ({
            page: prevState.page + 1,
            loadingMore: true
          }),
          () => {
            this._fetchAll();
          }
        );
    };
    _sendMessage(message) {
      if (message) {
        let postParam = {};
          if(this.state.isLive) {
            postParam = {
              rid: this.state.roomId ? this.state.roomId : this.props.match.params.roomId,
              msg: message,
              token: this.state.userId,
            }
          } else {
            postParam = {
              message,
              name: this.state.name,
              email: this.state.email,
            }
          }
          
          CHATAPI.sendLivechatMessage(postParam).then(response => {
            console.log('addLiveMessage======',response);
            const { page } = this.state;
            
            if (response.message) {
              let postMessage = response.message;
              if (!this.state.isLive) {
                postMessage = [{
                  "_id": "1",
                  "rid": "KuACMJ5MpN6SfAFWg",
                  "msg": message,
                  "token": this.state.userId,
                  "alias": "Livechat Agent",
                  "ts": "2018-09-14T13:31:33.201Z",
                  "u": {
                    "_id": "YgEoq2djbGdjjZnsL",
                    "username": "guest-4",
                    "name": "Livechat Agent"
                  },
                },{
                  "_id": "2",
                  "rid": "KuACMJ5MpN6SfAFWg2",
                  "msg": 'Hiện tại các nhân viên đang bận. Xin quý khách vui lòng chờ trong giây lát.',
                  "token": 'KuACMJ5MpN6SfAFWg2',
                  "alias": "Livechat Agent",
                  "ts": "2018-09-14T13:31:33.201Z",
                  "u": {
                    "_id": "YgEoq2djbGdjjZnsL",
                    "username": "guest-4",
                    "name": "Livechat Agent"
                  },
                }];
              } else {
                postMessage = [postMessage];
              }
              this.setState((prevState, nextProps) => ({
                  data: [...this.state.data, ...postMessage],
                  loading: false,
                  loadingMore: false,
                  refreshing: false,
                  isFlushed: false,
                  message: null,
                  showEmoji: false 
              }));
            }
          })
          .catch(error => {
              this.setState({ error, loading: false });
          });
      }
    }

    _removeMessage(msgId) {
      if (msgId) {
        Alert.alert(
          "Thông báo",
          "Bạn muốn xoá tin nhắn này?",
          [
            {
              text: "Huỷ",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "Đồng ý", onPress: () => {
              const roomId = this.state.roomId ? this.state.roomId : this.props.match.params.roomId;
              const postParam = {
                roomId,
                msgId,
              }
              CHATAPI.removeMessage(postParam).then(response => {
                let messages = _.cloneDeep(this.state.data);
                messages = _.filter(messages, function(currentObject) {
                  return currentObject._id !== msgId;
                });
                this.setState((prevState, nextProps) => ({
                    data: messages,
                    loading: false,
                    loadingMore: false,
                    refreshing: false,
                    isFlushed: false,
                    message: null,
                    showEmoji: false 
                }));
              })
              .catch(error => {
                  this.setState({ error, loading: false });
              });
            } }
          ],
          { cancelable: false }
        )
      }
    }

    render() {  
        return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == "ios" ? "padding" : "height"} enabled>
      <View style={{flex: 1}}>
        <View style={{height: RFValue(80)}}>
          <NavigationHeader title={"Tư Vấn Viên"} to={{pathname: "/chat", state: { go: "/" }}} back={true} />
        </View>
        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss(); this.setState({ showEmoji: false })}}>
          <>
        <View style={{flex: 8.4}}>
            {!this.state.loading ? (
                <FlatList
                contentContainerStyle={{
                    flexGrow: 1,
                }}
                ref={ref => this.flatList = ref}
                onContentSizeChange={() => this.flatList.scrollToEnd({animated: true})}
                onLayout={() => this.flatList.scrollToEnd({animated: true})}
                numColumns={1}
                data={this.state.data}
                renderItem={({ item }) => (
                    <Item item={item} userId={this.state.userId} authToken={this.state.authToken} name={this.state.name} width={0.42} height={0.27} _sendMessage={this._sendMessage.bind(this)} removeMessage={this._removeMessage.bind(this)}/>
                )}
                keyExtractor={item => item._id.toString()}
                //onRefresh={this._handleRefresh}
                //refreshing={this.state.refreshing}
                onEndReachedThreshold={0.5}
                />
            ) : (
                <View
                  style={{flex: 1,
                  justifyContent: 'center',
                  alignItems:'center'
                }}>
                  <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
                </View>
            )}
            </View>
            <View
            style={{
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 20,
              borderTopWidth: 1,
              borderTopColor: 'rgba(34,34,34,0.3)',
            }}>
              <TextInput
                style={{width: screenWidth - 150, marginVertical: 10, paddingVertical: 10, color: "#333" }}
                placeholder="Gửi tin nhắn"
                placeholderTextColor="rgb(151,151,151)"
                value={this.state.message}
                onChangeText={(text) => {this.setState({message: text})}}
              />
              <TouchableOpacity onPress={() => this._displayImagePicker()} style={{ marginRight: 15 }}>
                <Icon component={Icon} size={24} color="rgba(34,34,34,0.5)" name="paperclip" /> 
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setState({ showEmoji: !this.state.showEmoji })} style={{ marginRight: 15 }}>
                <Icon component={Icon} size={24} color="rgba(34,34,34,0.5)" name="smile" /> 
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._sendMessage(this.state.message)}>
                <Icon component={Icon} size={24} color="rgba(34,34,34,0.5)" name="send" /> 
              </TouchableOpacity>
          </View>
          </>
          </TouchableWithoutFeedback>
          <View
            style={{
              flex: 0.1,
              width: '100%',
              backgroundColor: 'white',
              flexDirection: 'row',
            }}></View>
          
      </View>
      {this.state.showEmoji && <Emoji style={{ height: 200, position: 'absolute', bottom: 0, left: 0}} emojis={this.state.emojis} _sendMessage={this._sendMessage.bind(this)}/>}
       </KeyboardAvoidingView>
        );
      }
};

const mapStateToProps = (state) => ({
  chat: state.chatReducer
});

const mapDispatchToProps = dispatch => ({
  pushMessage: () => {
    return dispatch(pushMessageAction());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LivehatScreen);
