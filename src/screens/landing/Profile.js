import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Footer from '../../components/navigation/Footer';
import Header from '../../components/navigation/Header';
import AppConfig from '../../config/AppConfig';

import {getUserCredentials} from '../../services/storageServices';
import {Link} from 'react-router-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {
        name: '',
        email: ''
      }
    };
  }

  componentDidMount() {
    getUserCredentials().then(credential => {
      if (credential.username && credential.password) {
        console.log('credential', credential);
        this.setState({
          profile: JSON.parse(credential.username)
        });
      }
    });
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 2.91}}>
          <ImageBackground
            style={{width: undefined, height: undefined, flex: 1}}
            source={require('../../../assets/images/Landing/profile.png')}>
            <View style={{width: screenWidth, height: 0.1 * screenHeight}}>
              <Header go="/profile" />
            </View>
            <View
              style={{
                marginTop: 0.04 * screenHeight,
                width: screenWidth,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                resizeMode="contain"
                style={{width: 0.16 * screenWidth, height: 0.16 * screenWidth}}
                source={require('../../../assets/images/carla.png')}
              />
              <View
                style={{
                  flexDirection: 'column',
                  marginLeft: 0.02 * screenWidth,
                  flexGrow: 1,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontWeight: 'bold',
                    fontSize: 16,
                    fontFamily: 'Roboto-Regular',
                  }}>
                  {this.state.profile.name}
                </Text>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 14,
                    fontFamily: 'Roboto-Regular',
                  }}>
                  {this.state.profile.email}
                </Text> 
              </View>
            </View>
          </ImageBackground>
        </View>
        <ScrollView>
          <View style={{flex: 1.71}}>
            <Image
              resizeMode="contain"
              style={{width: undefined, height: undefined, flex: 1}}
              source={require('../../../assets/images/ranking1.png')}
            />
          </View>
          <View style={{flex: 5.35, flexDirection: 'column'}}>
            <ScrollView>
              <Link
                component={TouchableOpacity}
                to={{
                  pathname: '/profile/blogpost',
                  state: {
                    go: '/profile',
                    postId: AppConfig.companyCatid,
                    headline: 'Về Chúng tôi'
                  },
                }}
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p1.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Về Chúng tôi
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link>
              <Link
                component={TouchableOpacity}
                to={{
                  pathname: '/profile/wishlist',
                  state: {go: '/profile'},
                }}
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p2.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Danh sách yêu thích
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link>
              {/* <Link
                component={TouchableOpacity}
                to={{
                  pathname: '/underconstruction',
                  state: {go: '/profile'},
                }}
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p3.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Ví voucher
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link> */}
              <Link
                component={TouchableOpacity}
                to={{
                  pathname: `/profile/blog/${AppConfig.cookingCatid}`,
                  state: {go: '/profile'},
                }}
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p4.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Chuyên mục nấu ăn
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link>
              <Link
                component={TouchableOpacity}
                to={{
                  pathname: '/profile/orderhistory/0',
                  state: {go: '/profile'},
                }}
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p5.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Lịch sử mua hàng
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link>
              <Link
                component={TouchableOpacity}
                to="/profile/editprofile"
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p6.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Thiết lập tài khoản
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link>
              {/* <Link
                component={TouchableOpacity}
                to={{
                  pathname: '/underconstruction',
                  state: {go: '/profile'},
                }}
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p7.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Trung tâm trợ giúp - FAQ
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link> */}
              <Link
                component={TouchableOpacity}
                to={{
                  pathname: '/profile/stores',
                  state: {go: '/profile'},
                }}
                style={profile_styles.containerStyle}>
                <View style={{flexDirection: 'row', width: 0.7 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.imageStyle}
                    source={require('../../../assets/images/Profile/p8.png')}
                  />
                  <Text
                    style={{
                      marginLeft: 0.05 * screenWidth,
                      fontFamily: 'Roboto-Regular',
                      alignSelf: 'center',
                    }}>
                    Chi nhánh
                  </Text>
                </View>
                <View style={{width: 0.3 * screenWidth}}>
                  <Image
                    resizeMode="contain"
                    style={profile_styles.arrowStyle}
                    source={require('../../../assets/images/Profile/arrow.png')}
                  />
                </View>
              </Link>
            </ScrollView>
          </View>
        </ScrollView>
        <View style={{flex: 1.13}}>
          <Footer
            home={false}
            square={false}
            comments={false}
            alarm={false}
            profile={true}
          />
        </View>
      </View>
    );
  }
  
};
const profile_styles = {
  containerStyle: {
    height: 0.07 * screenHeight,
    width: screenWidth,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(243,243,243)',
    flexDirection: 'row',
  },
  imageStyle: {
    width: 0.04 * screenHeight,
    height: 0.04 * screenHeight,
    marginLeft: 0.04 * screenWidth,
  },
  arrowStyle: {
    width: 0.03 * screenWidth,
    height: 0.03 * screenWidth,
    marginLeft: 0.2 * screenWidth,
  },
};
export default Profile;
