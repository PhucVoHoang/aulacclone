import React from 'react';
import {View, Text} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';

const underConstruction = props => {
    console.log('underConstruction',props.location.state.go)

    return (
    <View style={{flex: 1}}>
      <View style={{flex: 1.14}}>
        <NavigationHeader to={props.location.state.go} back={true} />
      </View>
      <View
        style={{
          flex: 11,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'grey',
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: 24,
            fontFamily: 'Roboto-Bold',
          }}>
          Under Construction
        </Text>
      </View>
    </View>
  );
};

export default underConstruction;
