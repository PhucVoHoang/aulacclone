import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import OneSignal from 'react-native-onesignal';
import {connect} from 'react-redux';

import Footer from '../../components/navigation/Footer';
import Header from '../../components/navigation/Header';
import LoadCategory from '../../components/landing/loadCategory';
import LoadProducts from '../../components/landing/loadProducts';
import {RFValue} from 'react-native-responsive-fontsize';
import ProductBlock from '../../components/landing/productBlock';
import LiveBlock from '../../components/landing/liveBlock';
import Icon from 'react-native-vector-icons/Feather';
import {Link} from 'react-router-native';
import AppConfig from '../../config/AppConfig';
import CHATAPI from '../../services/ChatApi';
import API from '../../services/api';
import {getUserCredentials} from '../../services/storageServices';
import {pushMessageAction} from '../../redux/actions/chatActions';


const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

var requiresPrivacyConsent = true;
var appId = '307d8561-c29b-4f06-9539-bf88b8b23d97';

class Landing extends Component {
  constructor(properties) {
    super(properties);

    this.state = {
        // OneSignal states
        // Privacy Consent states
        hasPrivacyConsent: false, // App starts without privacy consent
        isPrivacyConsentLoading: requiresPrivacyConsent,

        // Device states
        userId: '',
        pushToken: '',

        // Subscription states
        isSubscribed: false,
        isSubscriptionLoading: false,

        // External User Id states
        externalUserId: '',
        isExternalUserIdLoading: false,

        // Email states
        email: '',
        isEmailLoading: false,

        // In-App Messaging states
        iam_paused: false,

        // Add more states here...

        // Demo App states
        debugText: ''
    };

    // Log level logcat is 6 (VERBOSE) and log level alert is 0 (NONE)
    OneSignal.setLogLevel(6, 0);

    // Share location of device
    OneSignal.setLocationShared(true);

    OneSignal.setRequiresUserPrivacyConsent(requiresPrivacyConsent);
    OneSignal.provideUserConsent(true);
    OneSignal.setSubscription(true);
    OneSignal.init(appId, {
        kOSSettingsKeyAutoPrompt: true,
    });

    // Notifications will display as NOTIFICATION type
    OneSignal.inFocusDisplaying(2);
    
  }
    componentDidMount() {
      getUserCredentials().then(credential => {
        if (credential.username && credential.password) {
          const userInfo = JSON.parse(credential.username);
          this.setState({ email: userInfo.email });
          OneSignal.addEventListener('received', this.onNotificationReceived);
          OneSignal.addEventListener('opened', this.onNotificationOpened);
          OneSignal.addEventListener('ids', this.onIdsAvailable);
          OneSignal.sendTag("email", userInfo.email);
          console.log('ada=====', userInfo)
        }
      });
  }

  /**
     When a notification is received this will fire
     */
    onNotificationReceived = (notification) => {
      console.log('Notification received: ', notification);

      if (notification.payload.additionalData && notification.payload.additionalData.roomId) {
        this.props.pushMessage(true);
      }

      let debugMsg = 'RECEIVED: \n' + JSON.stringify(notification, null, 2);
      this.setState({debugText:debugMsg}, () => {
          console.log("Debug text successfully changed!");
      });
  }

  /**
   When a notification is opened this will fire
   The openResult will contain information about the notification opened
   */
  onNotificationOpened = (openResult) => {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
      if (openResult.notification.payload.additionalData) {
        if(openResult.notification.payload.additionalData.order_id) {
          this.props.history.push(`/profile/orders/${openResult.notification.payload.additionalData.order_id}`);
        }
        if(openResult.notification.payload.additionalData.roomId) {
          if (openResult.notification.payload.additionalData.is_live == 1) {
            this.props.history.push(`/livechat/`);
          } else
            this.props.history.push(`/chat/${openResult.notification.payload.additionalData.roomId}`);
        }
      }

      let debugMsg = 'OPENED: \n' + JSON.stringify(openResult.notification, null, 2);
      this.setState({debugText:debugMsg}, () => {
          console.log("Debug text successfully changed!");
      });
  }

  /**
   Once the user is registered/updated the onIds will send back the userId and pushToken
      of the device
   */
  onIdsAvailable = (device) => {
      console.log('Device info: ', device);
      this.setState({
          userId: device.userId,
          pushToken: device.pushToken
      });
      API.updateToken({ custom_field_4: device.userId, custom_field_3: this.state.email.split('@')[0].replace(/[^a-zA-Z0-9]/g, "") });
  }
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <Header bgColor="#ECB22D" go="/landing" props={this.props}/>
        </View>
        <View style={{flex: 8}}>
          <ScrollView>
            <View style={{height: 0.6489 * screenWidth, width: '100%'}}>
              <Image
                resizeMode="contain"
                style={{width: undefined, height: undefined, flex: 1}}
                source={require('../../../assets/images/Landing/landing_background.png')}
              />
            </View>
            <View style={{width: '100%'}}>
              <LoadCategory />
            </View>
            <View
              style={{
                height: 0.21 * screenHeight,
                width: '100%',
                marginTop: 10
              }}>
              <Image
                resizeMode="contain"
                style={{width: undefined, height: undefined, flex: 1}}
                source={require('../../../assets/images/Landing/sanpham.png')}
              />
              <View
                style={{
                  position: 'absolute',
                  width: '85%',
                  height: '100%',
                }}>
                <View
                  style={{
                    flex: 1,
                    flexGrow: 1,
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                  }}>
                  <Text style={style.textStyleDeal}>SẢN PHẨM GIA VỊ</Text>
                  <Text
                    style={[
                      style.textStyleDeal,
                      {
                        fontSize: RFValue(26),
                        fontFamily: 'Eurostile-BoldOblique',
                      },
                    ]}>
                    DEAL DƯỚI 199K
                  </Text>
                  <View
                    style={{
                      width: 0.253 * screenWidth,
                      height: 0.032 * screenHeight,
                      borderRadius: 5,
                      backgroundColor: 'rgb(236,178,45)',
                    }}>
                        <Link
                        component={TouchableOpacity}
                        to={{
                          pathname: '/products/filter/filterresults',
                          state: {go: '/landing', filter: {catId: AppConfig.giaviId, price_from: '0', price_to: '200000'}},
                        }}
                        style={{
                          flex: 1,
                          flexGrow: 1,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                      <Text
                        style={{
                          fontFamily: 'Roboto-Regular',
                          color: 'white',
                        }}>
                        Xem ngay
                      </Text>
                      <Icon
                        component={Icon}
                        color="white"
                        name="chevron-right"
                      />
                    </Link>
                  </View>
                </View>
              </View>
            </View>
            <View
              style={{
                height: 0.36 * screenHeight,
                width: '100%',
                justifyContent: 'space-around',
                flexDirection: 'column',
              }}>
              <View
                style={{
                  height: 0.06 * screenHeight,
                  marginLeft: 0.04 * screenWidth,
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    width: 0.73 * screenWidth,
                    flexDirection: 'row',
                    flexGrow: 1,
                    flex: 1,
                  }}>
                  <Text
                    style={{
                      color: 'rgb(173,14,23)',
                      fontSize: RFValue(16),
                      marginTop: 0.02 * screenHeight,
                      fontFamily: 'Roboto-Bold',
                    }}>
                    FLASH SALE
                  </Text>
                </View>
                <View
                  style={{
                    flexGrow: 1,
                    flex: 1,
                    marginRight: 0.04 * screenWidth,
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                  }}>
                  <Link
                  component={TouchableOpacity}
                  to={`/featurecategories/isFeature`}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                    <Text
                      style={{
                        marginLeft: 0,
                        color: 'rgb(135,132,130)',
                        fontFamily: 'Roboto-Regular',
                      }}>
                      Xem tất cả
                    </Text>
                  </Link>
                </View>
              </View>
              <LoadProducts isFeature={true} style={{flex: 1, flexDirection: 'row'}}/>
              <View style={{height: 0.03 * screenHeight}} />
            </View>
            <View
              style={{
                height: 0.36 * screenHeight,
                width: '100%',
                justifyContent: 'space-around',
                flexDirection: 'column',
              }}>
              <View
                style={{
                  height: 0.06 * screenHeight,
                  marginLeft: 0.04 * screenWidth,
                  marginRight: 0.04 * screenWidth,
                  justifyContent: 'space-between',
                  flex: 1,
                  flexGrow: 1,
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    color: 'rgb(173,14,23)',
                    fontSize: RFValue(16),
                    fontFamily: 'Roboto-Bold',
                  }}>
                  TÌM KIẾM HÀNG ĐẦU
                </Text>
                <Link
                  component={TouchableOpacity}
                  to={`/featurecategories/isNew`}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                    <Text
                    style={{
                      color: 'rgb(135,132,130)',
                      fontFamily: 'Roboto-Regular',
                    }}>
                    Xem tất cả
                  </Text>
                  </Link>
              </View>
              <LoadProducts isNew={true} style={{flex: 1, flexDirection: 'row'}}/>
            </View>
            <View
              style={{
                height: 0.21 * screenHeight,
                width: '100%',
              }}>
              <Image
                resizeMode="stretch"
                style={{width: '100%', height: 0.21 * screenHeight}}
                source={require('../../../assets/images/Landing/nauan.png')}
              />
              <View
                style={{
                  position: 'absolute',
                  width: '95%',
                  marginLeft: '5%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: RFValue(15),
                    fontFamily: 'Roboto-Bold',
                  }}>
                  Chuyên Mục
                </Text>
                <Text
                  style={{
                    fontFamily: 'Roboto-Bold',
                    color: 'white',
                    fontSize: RFValue(46),
                  }}>
                  NẤU ĂN
                </Text>
                <View
                  style={{
                    width: 0.29 * screenWidth,
                    height: 0.032 * screenHeight,
                    borderRadius: 5,
                    backgroundColor: 'rgb(236,178,45)',
                  }}>
                      <Link
                        component={TouchableOpacity}
                        to={{
                          pathname: `/profile/blog/${AppConfig.cookingCatid}`,
                          state: {go: '/landing'},
                        }}
                        style={{
                          flex: 1,
                          flexGrow: 1,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                    <Text
                      style={{
                        fontFamily: 'Roboto-Regular',
                        color: 'white',
                      }}>
                      Tìm hiểu thêm
                    </Text>
                    <Icon component={Icon} color="white" name="chevron-right" />
                  </Link>
                </View>
              </View>
            </View>
            <View style={style.liveBlockStyle}>
              <LiveBlock />
            </View>
            <View
              style={{
                height: screenHeight * 0.8,
                width: '100%',
                flexDirection: 'column',
                marginBottom: 0.02 * screenHeight,
              }}>
              <View
                style={{
                  marginLeft: 0.04 * screenWidth,
                  marginRight: 0.04 * screenWidth,
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    flex: 1,
                    flexGrow: 1,
                    width: 0.62 * screenWidth,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      color: 'rgb(173,14,23)',
                      fontSize: RFValue(16),
                      fontFamily: 'Roboto-Bold',
                    }}>
                    GỢI Ý HÔM NAY
                  </Text>
                </View>
                <View style={{flexDirection: 'row', width: 0.3 * screenWidth}}>
                  <Link
                    component={TouchableOpacity}
                    to={{
                      pathname: '/products/filter',
                      state: {go: '/Landing'},
                    }}
                    style={{
                      flex: 1,
                      flexGrow: 1,
                      flexDirection: 'row',
                      justifyContent: 'flex-end',
                      alignItems: 'center',
                    }}>
                    <Image
                      resizeMode="contain"
                      style={{
                        width: RFValue(16.9),
                        height: RFValue(15.6),
                      }}
                      source={require('../../../assets/images/Landing/filter.png')}
                    />
                    <Text
                      style={{
                        marginLeft: 5,
                        color: 'black',
                        fontFamily: 'Roboto-Bold',
                      }}>
                      Bộ lọc
                    </Text>
                  </Link>
                </View>
              </View>
              <LoadProducts flat={true} style={{ width: "50%" }}/>
            </View>
            <View style={{height: 0.7 * screenHeight}}/>
          </ScrollView>
        </View>
        <View style={{flex: 1}}>
          <Footer
            home={true}
            square={false}
            comments={false}
            alarm={false}
            profile={false}
          />
        </View>
      </View>
    );
  }
}
const style = {
  textStyle: {
    position: 'absolute',
    marginTop: 0.15 * screenHeight,
    width: 0.3 * screenWidth,
    marginLeft: 0.05 * screenWidth,
    fontSize: 19,
    color: 'black',
  },
  textStyleDeal: {
    fontSize: RFValue(20),
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'Eurostile-BoldOblique',
  },
  liveBlockStyle: {
    height: 0.326 * screenHeight,
    width: '92%',
    marginLeft: 0.04 * screenWidth,
    marginRight: 0.04 * screenWidth,
  },
};
const mapStateToProps = (state) => ({
  chat: state.chatReducer
});

const mapDispatchToProps = dispatch => ({
  pushMessage: (push) => {
    return dispatch(pushMessageAction(push));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Landing);

export const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}