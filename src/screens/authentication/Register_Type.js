import React from 'react';
import {View, ImageBackground} from 'react-native';
import PopupDialog from '../../components/authentication/PopupDialog';

const Register_Type = () => {
  const imgsrc = require('../../../assets/images/logo_aulac.png');
  return (
    <ImageBackground
      style={styles.imgStyle}

      source={require('../../../assets/images/register_type.png')}>
      <PopupDialog
        visible={true}
        headerOne="BẠN LÀ KHÁCH SỈ HAY KHÁCH LẺ ?"
        img_src={imgsrc}
        buttonOne_text="KHÁCH SỈ"
        buttonOne_bgColor="#ECB22D"
        buttonOne_disable={false}
        buttonOne_link="/landing"
        buttonTwo_text="KHÁCH LẺ"
        buttonTwo_bgColor="#AD0E17"
        buttonTwo_disable={false}
        buttonTwo_link="/landing"
      />
    </ImageBackground>
  );
};

const styles = {
  imgStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
};
export default Register_Type;
