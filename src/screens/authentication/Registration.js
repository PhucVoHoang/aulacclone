import React, {Component, Fragment} from 'react';
import _ from 'lodash';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  ScrollView
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import CustomTextInput from '../../components/authentication/customTextInput';
import CustomButton from '../../components/authentication/customButton';
import {Link} from 'react-router-native';
import { SafeAreaInsetsContext } from 'react-native-safe-area-context';

import API from '../../services/api';
import CHATAPI from '../../services/ChatApi';

import WrongLogin from '../../components/authentication/wrongLogin';
const warning_img = require('../../../assets/images/warning.png');
const wrong_img = require('../../../assets/images/wrong.png');

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {first_name: '', last_name: '', email: '', password: '', conf_password: '', formLoading: false, wrongLogin: false};
  }
  validEmail(email) {
    const regex = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,4})+$/;
    return regex.test(email);
  };
  onSubmit() {
    if (
        this.validEmail(this.state.email) &&
        this.state.password &&
        this.state.password === this.state.conf_password
      ) {
        const variables = {
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          email: this.state.email,
          password: this.state.password,
          password_confirmation: this.state.conf_password
        };
        this.setState({
          formLoading: true,
        });
        API.register(variables).then(res => {
          const visitor =  {
            "name": this.state.first_name + " " + this.state.last_name,
            "email": this.state.email.toLowerCase(),
            "username": this.state.email.toLowerCase().split('@')[0].replace(/[^a-zA-Z0-9]/g, ""),
            "pass": this.state.email.toLowerCase()
          }
          CHATAPI.register(visitor);
          this.props.history.push('/');
        }).catch(err => {
          console.log('err', err);
          this.setState({
            formLoading: false, wrongLogin: true,
          });
        })
      } else {
        this.setState({
          formLoading: false, wrongLogin: true,
        });
      }
  }

  _renderContent = (insets) => {
    return (
      <ImageBackground
        style={styles.imgStyle}
        source={require('../../../assets/images/login_bg.png')}>
        <View style={styles.OnImageView}>
          <View style={styles.flexOne} />
          <KeyboardAvoidingView style={{ flex: 24, flexDirection: 'column',justifyContent: 'center', marginBottom: insets.bottom}} behavior="padding" enabled keyboardVerticalOffset={10}>
            <Link component={TouchableOpacity} to="/" style={styles.registerTextContainer}>
              <Text style={styles.registerText}>ĐĂNG NHẬP</Text>
            </Link>
            <Image
              style={styles.imgStyleLogo}
              resizeMode='contain'
              source={require('../../../assets/images/logo_aulac.png')}
            />
            <ScrollView >
            <View
              style={{
                marginTop: screenHeight * 0.043,
                height: 0.0706 * screenHeight
              }}>
              <CustomTextInput
                secure={false}
                clear={() => {
                  this.setState({last_name: ''});
                }}
                change={event =>
                  this.setState({last_name: event.nativeEvent.text})
                }
                value={this.state.last_name}
                placeholderText="Họ"
              />
            </View>
            <View
              style={{
                marginTop: screenHeight * 0.021,
                height: 0.0706 * screenHeight,
              }}>
              <CustomTextInput
                secure={false}
                clear={() => {
                  this.setState({first_name: ''});
                }}
                change={event =>
                  this.setState({first_name: event.nativeEvent.text})
                }
                value={this.state.first_name}
                placeholderText="Tên"
              />
            </View>
            <View
              style={{
                marginTop: screenHeight * 0.021,
                height: 0.0706 * screenHeight,
              }}>
              <CustomTextInput
                secure={false}
                clear={() => {
                  this.setState({email: ''});
                }}
                change={event =>
                  this.setState({email: event.nativeEvent.text})
                }
                value={this.state.email}
                placeholderText="Email"
              />
            </View>
            <View
              style={{
                marginTop: screenHeight * 0.021,
                height: 0.0706 * screenHeight,
              }}>
              <CustomTextInput
                secure={false}
                placeholderText="Mật khẩu"
                change={event =>
                  this.setState({password: event.nativeEvent.text})
                }
                clear={() => {
                  this.setState({password: ''});
                }}
                value={this.state.password}
              />
            </View>
            <View
              style={{
                marginTop: screenHeight * 0.021,
                height: 0.0706 * screenHeight,
              }}>
              <CustomTextInput
                secure={false}
                placeholderText="Nhập lại mật khẩu"
                change={event =>
                  this.setState({conf_password: event.nativeEvent.text})
                }
                clear={() => {
                  this.setState({conf_password: ''});
                }}
                value={this.state.conf_password}
              />
            </View>
            <View
              style={{
                marginTop: 0.03125 * screenHeight,

                height: 0.0706 * screenHeight,
              }}>
              {this.state.password && this.state.password && this.state.conf_password && !this.state.formLoading ? (
                <CustomButton
                  titleText="ĐĂNG KÝ"
                  bgColor="#FFCB08"
                  disable={false}
                  click={this.onSubmit.bind(this)}
                />
              ) : (
                <CustomButton
                  titleText="ĐĂNG KÝ"
                  bgColor="#CCCCCC"
                  disable={true}
                />
              )}
            </View>
            <Text style={{marginTop: 0.05 * screenHeight, fontSize: RFValue(14), color:'white', textAlign:'center',fontFamily: 'Roboto-BoldCondensed'}}>Bằng việc nhấn vào nút Đăng Ký, bạn đã đồng ý
              </Text>
            <Link component={TouchableOpacity} to='/policy'>
              <Text style={{fontSize: RFValue(14), color:'white', textAlign:'center', textDecorationLine:'underline',fontFamily: 'Lato-Semibold'}}>Điều khoản sử dụng</Text>
            </Link>
            </ScrollView>
          </KeyboardAvoidingView>  
          <View style={styles.flexOne} />
          {this.state.wrongLogin ? (
            <WrongLogin
              visible={true}
              headerOne="SAI THÔNG TIN ĐĂNG KÝ"
              img_src={wrong_img}
              headerTwo="Bạn đã đăng ký sai email hoặc mật khẩu, vui lòng thử lại."
              buttonOne_text="ĐỒNG Ý"
              buttonOne_bgColor="#6BBD12"
              buttonOne_disable={false}
              buttonOne_link="/register"
              clicked={() => {
                this.setState({wrongLogin: false});
              }}
            />
          ) : null}
          {this.state.formLoading ? (
            <View style={styles.loading}>
              <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
            </View>  
          ) : null}
        </View>
      </ImageBackground>
    );
  }

  render() {
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => this._renderContent(insets)}
      </SafeAreaInsetsContext.Consumer>
    );
  }
}

const styles = {
  imgStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  imgStyleLogo: {
    width: screenWidth * 0.39,
    height: screenHeight * 0.23,
    marginTop: screenHeight * 0.035,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  arrowStyle: {
    width: screenWidth * 0.028,
    height: screenHeight * 0.0135,
    resizeMode: 'contain',
    marginTop: 0.01 * screenHeight,
    marginLeft: 0.01 * screenWidth,
  },
  flexOne: {
    flex: 1,
  },
  OnImageView: {
    flex: 1,
    flexDirection: 'row',
  },
  registerText: {
    fontSize: RFValue(21),
    alignSelf: 'flex-end',
    color: 'white',
    fontFamily: 'Roboto-BoldCondensed',
  },
  registerTextContainer: {
    marginTop: 0.05 * screenHeight,
  },
  lineStyle: {
    backgroundColor: 'white',
    height: 1,
    marginTop: RFValue(9.5),
    width: screenWidth * 0.22,
  },
  staticButton: {
    width: 0.44 * screenWidth,
    height: 0.076 * screenHeight,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems:'center',
  },
};
export default Registration;
