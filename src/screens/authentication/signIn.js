import React, {Component, Fragment} from 'react';
import _ from 'lodash';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import {connect} from 'react-redux';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';
import { LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';

import CustomTextInput from '../../components/authentication/customTextInput';
import CustomButton from '../../components/authentication/customButton';
import PopupDialog from '../../components/authentication/PopupDialog';
import {loginAction} from '../../redux/actions/authActions';
import API from '../../services/api';
import CHATAPI from '../../services/ChatApi';
import {replaceCart} from '../../redux/actions/cartActions';

import {saveUserCredentials, getUserCredentials, setInternetCredentials} from '../../services/storageServices';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const warning_img = require('../../../assets/images/warning.png');
const wrong_img = require('../../../assets/images/wrong.png');

import WrongLogin from '../../components/authentication/wrongLogin';

class signIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      popup: false,
      formLoading: false,
      wrongLogin: false,
      isLoggedIn: false,
      profile: null,
      profileImage: null,
      accessToken: null,
      userInfo: null,
      gettingLoginStatus: true
    };
    this.setProfile = this.setProfile.bind(this);
    this.setProfileImage = this.setProfileImage.bind(this);
    this.setLoggedIn = this.setLoggedIn.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this._socialLogin = this._socialLogin.bind(this);
    this.handleFacebookLogin = this.handleFacebookLogin.bind(this);
    this.getPublicProfile = this.getPublicProfile.bind(this);
  }
  handleFacebookLogin () {
    const that = this;
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled')
        } else {
          console.log('Login success with permissions: ' + result.grantedPermissions.toString())
          that.getPublicProfile();
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error)
      }
    )
  }
  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
      this._socialLogin({ 
        username: userInfo.user.email,
        password: userInfo.user.id,
        first_name: userInfo.user.givenName,
        last_name: userInfo.user.familyName
      });
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };
  setProfile(profile) {
    this.setState({ profile });
  }
  setProfileImage(profileImage) {
    this.setState({ profileImage });
  }
  setLoggedIn(isLoggedIn) {
    this.setState({ isLoggedIn });
  }
  componentDidMount() {
    //initial configuration
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId: '171950015487-482r4cbma5i60o52nasvo602jk8969sv.apps.googleusercontent.com',
    });
    getUserCredentials().then(credential => {
      if (credential.username && credential.password) {
        this.props.history.push('/landing');
      }
    });
  }

  getPublicProfile = async () => {

    const infoRequest = new GraphRequest(
      `/me?fields=id,name,picture,email,birthday`,
      null,
      (error, result) => {
        if (error) {
          console.log('Error fetching data: ' + error.toString());
        } else {
          this.setProfile(result);
          this.setProfileImage(result.picture.data.url);
          
          this._socialLogin({ 
            username: result.email,
            password: result.id,
            first_name: result.name,
            last_name: result.name
          });
        }
      }
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  }

  _socialLogin(info) {
    const { username, password, first_name, last_name } = info
    
    API.login({
      email: username,
      password: password,
    })
      .then(res => {
        this.props.loginRequest({
          "token": _.get(res, 'token'),
          "info": _.get(res, 'data')
        })
        const userInfo = _.get(res, 'data');
        const visitor =  {
          "name": userInfo.name,
          "email": userInfo.email.toLowerCase(),
          "username": userInfo.email.toLowerCase().split('@')[0].replace(/[^a-zA-Z0-9]/g, ""),
          "pass": userInfo.email.toLowerCase()
        }
        saveUserCredentials(
          JSON.stringify(_.get(res, 'data')),
          _.get(res, 'token'),
        );
        this._loginChat({"user": visitor.username, "password": userInfo.email.toLowerCase()},visitor);
      })
      .catch(err => {
        console.log('err variables', err);
        const variables = {
          first_name,
          last_name,
          email: username,
          password: password,
          password_confirmation: password
        };
        API.register(variables).then(res => {
          this.setState({ 
            username,
            password
          });
          this.onSubmit();
        }).catch(err => {
          console.log('err', err);
          this.setState({
            formLoading: false, wrongLogin: true,
          });
        })
      });
  }

  onSubmit() {
    this.setState({
      formLoading: true,
    });
    
    API.login({
      email: this.state.username,
      password: this.state.password,
    })
      .then(res => {
        this.props.loginRequest({
          "token": _.get(res, 'token'),
          "info": _.get(res, 'data')
        })
        const userInfo = _.get(res, 'data');
        const visitor =  {
          "name": userInfo.name,
          "email": userInfo.email.toLowerCase(),
          "username": userInfo.email.toLowerCase().split('@')[0].replace(/[^a-zA-Z0-9]/g, ""),
          "pass": userInfo.email.toLowerCase()
        }
        saveUserCredentials(
          JSON.stringify(_.get(res, 'data')),
          _.get(res, 'token'),
        );
        API.getCart().then(json => {
          this.props.reduxReplaceCart(json.data);
        });
        this._loginChat({"user": visitor.username, "password": visitor.pass},visitor);
      })
      .catch(err => {
        console.log('login error');
        this.setState({
          formLoading: false, wrongLogin: true,
        });
      });
  }
  _loginChat(info, visitor) {
    CHATAPI.getVisitor(info).then(response => {
      setInternetCredentials(JSON.stringify(_.get(response, 'data')),response.data.authToken);
      this.props.history.push('/landing');
    }).catch(err => {
      CHATAPI.register(visitor).then(res => {
        this._loginChat(info);
      }).catch(err => {
        console.log(err);
      })
    })
  }
  render() {
    const { isLoggedIn } = this.state;
    return (
      <ImageBackground
        style={styles.imgStyle}
        source={require('../../../assets/images/login_bg.png')}>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.OnImageView}>
          <View style={styles.flexOne} />
          <KeyboardAvoidingView style={{ flex: 24, flexDirection: 'column',justifyContent: 'flex-start',}} behavior="padding" enabled keyboardVerticalOffset={10}>
            <Link
              component={TouchableOpacity}
              to="/register"
              style={styles.registerTextContainer}>
              <Text style={styles.registerText}>ĐĂNG KÝ</Text>


            </Link>
            <Image
              style={styles.imgStyleLogo}
              resizeMode="contain"
              source={require('../../../assets/images/logo_aulac.png')}
            />
            <View
              style={{
                marginTop: screenHeight * 0.043,
                height: 0.0706 * screenHeight,
              }}>
              <CustomTextInput
                secure={false}
                clear={() => {
                  this.setState({username: ''});
                }}
                change={event =>
                  this.setState({username: event.nativeEvent.text})
                }
                value={this.state.username}
                placeholderText="email"
              />
            </View>
            <View
              style={{
                marginTop: screenHeight * 0.019,
                height: 0.0706 * screenHeight,
              }}>
              <CustomTextInput
                secure={true}
                placeholderText="Mật khẩu"
                change={event =>
                  this.setState({password: event.nativeEvent.text})
                }
                value={this.state.password}
              />
            </View>
            <Link component={TouchableOpacity} to="/forgotpassword">
              <Text
                style={{
                  marginTop: 0.01 * screenHeight,
                  fontSize: RFValue(16),
                  textDecorationLine: 'underline',
                  color: '#D8D8D8',
                  fontFamily: 'Roboto-Regular',
                }}>
                Quên mật khẩu?
              </Text>
            </Link>
            <View
              style={{
                marginTop: 0.03125 * screenHeight,

                height: 0.0706 * screenHeight,
              }}>
              {this.state.username && this.state.password && !this.state.formLoading ? (
                <CustomButton
                  click={this.onSubmit}

                  titleText="ĐĂNG NHẬP"
                  bgColor="#FFCB08"
                  disable={false}
                />
              ) : (
                <CustomButton
                  titleText="ĐĂNG NHẬP"
                  bgColor="#CCCCCC"
                  disable={true}
                />
              )}
            </View>
            <View
              style={{
                marginTop: 0.0475 * screenHeight,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={styles.lineStyle} />
              <Text style={{color: 'white', fontSize: RFValue(16), fontFamily: 'Roboto-Regular'}}>
                Hoặc qua mạng xã hội
              </Text>
              <View style={styles.lineStyle} />
            </View>
            <View
              style={{
                marginTop: 0.0353 * screenHeight,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity style={styles.staticButton} onPress={this.handleFacebookLogin}>
                <Image
                    style={styles.imgStyle}
                    resizeMode="contain"
                    source={require('../../../assets/images/facebook.png')}
                  />
              </TouchableOpacity>
              <TouchableOpacity style={styles.staticButton} onPress={this._signIn}>
                <Image
                  style={styles.imgStyle}
                  source={require('../../../assets/images/google.png')}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
            {this.state.popup ? (
              <PopupDialog
                visible={true}
                headerOne="BỎ QUA ĐĂNG NHẬP?"
                img_src={warning_img}
                headerTwo="Chọn đồng ý để sử dụng ứng dụng không cần đăng nhập."
                buttonOne_text="ĐỒNG Ý"
                buttonOne_bgColor="#6BBD12"
                buttonOne_disable={false}
                buttonOne_link="/landing"
                buttonTwo_text="KHÔNG ĐỒNG Ý"
                buttonTwo_bgColor="#AD0E17"
                buttonTwo_disable={false}
                buttonTwo_link="/register"
                clicked={() => {
                  this.setState({popup: false});
                }}
              />
            ) : null}
            {this.state.wrongLogin ? (
              <WrongLogin
                visible={true}
                headerOne="SAI THÔNG TIN ĐĂNG NHẬP"
                img_src={wrong_img}
                headerTwo="Bạn đã đăng nhập sai email hoặc mật khẩu, vui lòng thử lại."
                buttonOne_text="THỬ LẠI"
                buttonOne_bgColor="#FFCB08"
                buttonOne_disable={false}
                buttonOne_link=""
                clicked={() => {
                  this.setState({wrongLogin: false});
                }}
              />
            ) : null}
            {/* <TouchableOpacity
              onPress={() => {
                this.setState({popup: true});
              }}
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                flex: 1,
                marginTop: 0.0502 * screenHeight,
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: RFValue(16),
                  fontFamily: 'Roboto-Regular',
                }}>
                BỎ QUA
              </Text>
              <Image
                style={styles.arrowStyle}
                source={require('../../../assets/images/right-arrow.png')}
                resizeMode="contain"
              />
            </TouchableOpacity> */}
          </KeyboardAvoidingView>
          {this.state.formLoading ? (
            <View style={styles.loading}>
              <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
            </View>  
          ) : null}
          <View style={styles.flexOne} />
        </View>
        </TouchableWithoutFeedback>
      </ImageBackground>
    );
  }
}

const styles = {
  imgStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  imgStyleLogo: {
    width: screenWidth * 0.39,
    height: screenHeight * 0.23,
    marginTop: screenHeight * 0.035,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  arrowStyle: {
    width: screenWidth * 0.028,
    height: screenHeight * 0.0135,
    resizeMode: 'contain',
    marginTop: 0.01 * screenHeight,
    marginLeft: 0.01 * screenWidth,
  },
  flexOne: {
    flex: 1,
  },
  OnImageView: {
    flex: 1,
    flexDirection: 'row',
  },
  registerText: {
    fontSize: RFValue(21),
    alignSelf: 'flex-end',
    color: 'white',
    fontFamily: 'Roboto-BoldCondensed',
  },
  registerTextContainer: {
    marginTop: 0.05 * screenHeight,
  },
  lineStyle: {
    backgroundColor: 'white',
    height: 1,
    marginTop: RFValue(9.5),
    width: screenWidth * 0.22,
  },
  staticButton: {
    width: 0.44 * screenWidth,
    height: 0.076 * screenHeight,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems:'center',
  },
};


const mapStateToProps = (state) => ({
  auth: state.authReducer
});

const mapDispatchToProps = dispatch => ({
  loginRequest: (credentials) => {
    return dispatch(loginAction(credentials));
  },
  reduxReplaceCart: (cartDetail) => dispatch(replaceCart(cartDetail)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(signIn);
