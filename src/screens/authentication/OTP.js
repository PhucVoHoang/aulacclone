import React, {Component} from 'react';
import {View, StyleSheet, Dimensions, Text} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {Link, NativeRouter} from 'react-router-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class OTP extends Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader title="XÁC THỰC TÀI KHOẢN" to="/register" back={true} />
        </View>
        <View style={{flex: 8.9, alignItems: 'center'}}>
          <Text style={{marginTop: 0.15 * screenHeight}}>NHẬP MÃ SỐ CODE</Text>
          <Text style={{marginTop: 0.02 * screenHeight}}>
            Chúng tôi đã mã code đã được gửi vào số ******1537
          </Text>
          <Link to="/">
            <OTPInputView
              style={{width: '80%', height: 0.15 * screenHeight}}
              pinCount={4}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled={code => {
                this.props.history.push('/registerType');
              }}
            />
          </Link>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: 'white',
    backgroundColor: 'white',
  },

  underlineStyleBase: {
    width: 45,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    backgroundColor: '#ECB22D',
    color: 'white',
    borderRadius: 22.5,
  },

  underlineStyleHighLighted: {
    borderColor: '#03DAC6',
  },
});
export default OTP;
