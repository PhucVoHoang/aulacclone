import React from 'react';
import {View, ImageBackground} from 'react-native';
import PopupDialog from '../../components/authentication/PopupDialog';
import {getUserCredentials} from '../../services/storageServices';

class ChangeLocation extends React.Component {
    state = {
      isLoggedIn: true
    }
    componentDidMount() {
        getUserCredentials().then(credential => {
          if (credential.username && credential.password) {
            this.props.history.push('/landing');
          } else {
            this.setState({ isLoggedIn: false });
          }
        });
      }
    render () {
        if (this.state.isLoggedIn) return null;
        const imgsrc = require('../../../assets/images/logo_aulac.png');
        return (
            <ImageBackground
            style={styles.imgStyle}

            source={require('../../../assets/images/register_type.png')}>
            <PopupDialog
                visible={true}
                headerOne="CHỌN KHU VỰC"
                img_src={imgsrc}
                buttonOne_text="HỒ CHÍ MINH"
                buttonOne_bgColor="#ECB22D"
                buttonOne_disable={false}
                buttonOne_link="/signin"
                buttonTwo_text="HÀ NỘI"
                buttonTwo_bgColor="#AD0E17"
                buttonTwo_disable={false}
                buttonTwo_link="/signin"
                buttonThree_text="ĐÀ NẴNG"
                buttonThree_bgColor="#ECB22D"
                buttonThree_disable={false}
                buttonThree_link="/signin"
            />
            </ImageBackground>
        );
    };
}
const styles = {
  imgStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
};
export default ChangeLocation;
