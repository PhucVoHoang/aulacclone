import React, {Component} from 'react';
import {
  Dimensions,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {Link} from 'react-router-native';


const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);

class Phone_Inout extends Component {
  state = {
    btn_disable: true,
  };
  activate_btn(code) {
    if (code.length === 10) {
      this.setState(prevState => ({
        btn_disable: !prevState.btn_disable,
      }));
    }
    if (!this.state.btn_disable && code.length !== 10) {
      this.setState(prevState => ({
        btn_disable: !prevState.btn_disable,
      }));
    }
  }
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader title="XÁC THỰC TÀI KHOẢN" to="/register" back={true} />
        </View>
        <View style={{flex: 8.9, alignItems: 'center'}}>
          <Text
            style={{
              marginTop: 0.15 * screenHeight,
              color: '#515151',
              fontWeight: 'bold',
            }}>
            NHẬP SỐ ĐIỆN THOẠI
          </Text>
          <View
            style={{
              marginTop: 0.04 * screenHeight,
              width: 0.92 * screenWidth,
              height: 0.08 * screenHeight,
              borderWidth: 0.4,
              flexDirection: 'row',
            }}>
            <View
              style={{
                justifyContent: 'center',
                width: 0.15 * screenWidth,
                height: '100%',
                alignItems: 'center',
              }}>
              <Text style={{color: '#979797'}}>+84</Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                width: 0.62 * screenWidth,
                height: '100%',
                alignItems: 'flex-start',
              }}>
              <TextInput
                keyboardType={'phone-pad'}
                placeholder="Số điện thoại"
                placeholderTextColor="#979797"
                style={{color: 'black'}}
                onChangeText={code => this.activate_btn(code)}
              />
            </View>
            <View
              style={{
                justifyContent: 'center',
                width: 0.15 * screenWidth,
                height: '100%',
                alignItems: 'center',
              }}>
              <Link
                component={TouchableOpacity}
                to='/otp'
                disabled={this.state.btn_disable}
                style={
                  this.state.btn_disable
                    ? {
                        backgroundColor: '#CCCCCC',
                        width: 0.15 * screenWidth,
                        height: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }
                    : {
                        backgroundColor: '#ECB22D',
                        width: 0.15 * screenWidth,
                        height: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }
                }>
                <Text style={{color: 'white', fontWeight: 'bold'}}>GỬI</Text>
              </Link>
            </View>
          </View>
          <View
            style={{
              marginLeft: 0.04 * screenWidth,
              marginRight: 0.04 * screenWidth,
              marginTop: 0.04 * screenHeight,
              justifyContent: 'center',
            }}>
            <Text style={{textAlign: 'center', color: '#515151'}}>
              Vui lòng nhập đúng số điện thoại của bạn để nhận được mã xác nhận
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
export default Phone_Inout;
