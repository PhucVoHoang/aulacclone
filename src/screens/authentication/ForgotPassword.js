import React, {Component} from 'react';
import {
  Dimensions,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {Link} from 'react-router-native';
import API from '../../services/api';

import WrongLogin from '../../components/authentication/wrongLogin';
const wrong_img = require('../../../assets/images/wrong.png');

const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);

class ForgotPassword extends Component {
  state = {
    btn_disable: false,
    email: '',
    errorPopup: false,
    popupTitle: '',
    popupMessage: '',
  };

  onSubmit = () => {
    this.setState({
        btn_disable: true,
    });
    API.forgotpassword({
      email: this.state.email,
    }).then(res => {
      this.setState({
        btn_disable: false,
        errorPopup: true,
        popupTitle: 'THÀNH CÔNG',
        popupMessage: 'Vui lòng kiểm tra email để xác nhận.',
        redirect: '/',
    });
    })
      .catch(err => {
        this.setState({
            btn_disable: false,
            errorPopup: true,
            popupTitle: 'SAI THÔNG TIN',
            popupMessage: 'Eamil không tồn tại, vui lòng thử lại.',
            redirect: '/forgotpassword'
        });
      });
  }
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader title="LẤY LẠI MẬT KHẨU" to="/" back={true} />
        </View>
        <View style={{flex: 8.9, alignItems: 'center'}}>
          <Text
            style={{
              marginTop: 0.15 * screenHeight,
              color: '#515151',
              fontWeight: 'bold',
            }}>
            NHẬP EMAIL
          </Text>
          <View
            style={{
              marginTop: 0.04 * screenHeight,
              width: 0.92 * screenWidth,
              height: 0.08 * screenHeight,
              borderWidth: 0.4,
              flexDirection: 'row',
            }}>
            <View
              style={{
                justifyContent: 'center',
                width: 0.768 * screenWidth,
                height: '100%',
                alignItems: 'flex-start',
              }}>
              <TextInput
                placeholder="Email"
                placeholderTextColor="#979797"
                style={{color: 'black', width: '100%'}}
                onChangeText={text => this.setState({ email: text })}
              />
            </View>
            <View
              style={{
                justifyContent: 'center',
                width: 0.15 * screenWidth,
                height: '100%',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={this.onSubmit}
                style={{
                  width: 0.15 * screenWidth,
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: this.state.btn_disable ? '#CCCCCC' : '#ECB22D',
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>GỬI</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              marginLeft: 0.04 * screenWidth,
              marginRight: 0.04 * screenWidth,
              marginTop: 0.04 * screenHeight,
              justifyContent: 'center',
            }}>
            <Text style={{textAlign: 'center', color: '#515151'}}>
              Vui lòng nhập đúng email của bạn để nhận được mã xác nhận
            </Text>
          </View>
        </View>
        {this.state.errorPopup ? (
            <WrongLogin
              visible={true}
              headerOne={this.state.popupTitle}
              img_src={wrong_img}
              headerTwo={this.state.popupMessage}
              buttonOne_text="OK"
              buttonOne_bgColor="#6BBD12"
              buttonOne_disable={false}
              buttonOne_link={this.state.redirect}
              clicked={() => {
                this.setState({errorPopup: false});
              }}
            />
          ) : null}
      </View>
    );
  }
}
export default ForgotPassword;
