import React, {Component} from 'react';
import _ from 'lodash';
import {
  Dimensions,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import PaymentOptions from '../../components/payment/paymentOptions';
import CVC from '../../components/payment/cvc';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
import {Link} from 'react-router-native';
import {connect} from 'react-redux';
import {resetCart} from '../../redux/actions/cartActions';
import {resetCheckout, savePayment, createCheckout} from '../../redux/actions/checkoutActions';
import {checkoutAddPayment, checkoutAddShippingMethod, checkoutComplete} from '../../services/checkoutServices';
import API from '../../services/api';

class Payment extends Component {
  state = {
    cash: true,
    visa: false,
    master: false,
    atm: false,
    expiry: '',
    formLoading: false,
  };
  setCash = () => {
    if (!this.state.cash) {
      this.setState(prevState => ({
        cash: !prevState.cash,
        master: false,
        visa: false,
        atm: false,
        showCVC: false,
      }));
    }
  };
  setVisa = () => {
    if (!this.state.visa) {
      this.setState(prevState => ({
        visa: !prevState.visa,
        master: false,
        cash: false,
        atm: false,
        showCVC: true,
      }));
    }
  };
  setMaster = () => {
    if (!this.state.master) {
      this.setState(prevState => ({
        master: !prevState.master,
        cash: false,
        visa: false,
        atm: false,
        text: '',
        showCVC: true,
      }));
    }
  };
  setAtm = () => {
    if (!this.state.atm) {
      this.setState(prevState => ({
        atm: !prevState.atm,
        master: false,
        visa: false,
        cash: false,
        showCVC: true,
      }));
    }
  };
  handleChange = text => {
    let textTemp = text;
    if (textTemp[0] !== '1' && textTemp[0] !== '0') {
      textTemp = '';
    }

    if (textTemp.length === 2) {
      if (parseInt(textTemp.substring(0, 2)) > 12) {
        textTemp = textTemp[0];
      } else if (this.state.expiry.length === 1) {
        textTemp += '/';
      } else {
        textTemp = textTemp[0];
      }
    }

    this.setState({expiry: textTemp});
  };
  onSubmit = () => {
    this.setState({
      formLoading: true,
    })
    let paymentMethod = 'cashondelivery'
    API.savePayment({"payment": {"method" : paymentMethod}}).then(res => {
      this.props.reduxSavePayment(paymentMethod);
      API.saveOrder().then(res => {
        this.props.reduxSaveOrder(res.data);
        this.props.reduxResetCart();
        this.props.reduxResetCheckout();
        this.props.history.push('/shopping/success');
      }).catch(err => this.setState({
        formLoading: false,
      }))
    }).catch(err => console.log(_.get(err, 'data')))
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'rgb(243,243,243)'}}>
        <View style={{flex: 1.1}}>
          <NavigationHeader
            title="HÌNH THỨC THANH  TOÁN"
            to={{
              pathname: '/shopping/cart',
              state: {go: '/landing'},
            }}
            back={true}
          />
        </View>
        <View
          style={{
            flex: 8.9,
            width: screenWidth * 0.92,
            marginLeft: 0.04 * screenWidth,
          }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Text style={payment_style.txtStyleHeader}>
              Hình thức thanh toán
            </Text>
            <PaymentOptions
              selected={this.state.cash}
              click={this.setCash}
              title="Thanh toán bằng tiền mặt khi nhận hàng"
              imgsrc={require('../../../assets/images/Cart/cash.png')}
            />
            {/* <PaymentOptions
              selected={this.state.visa}
              click={this.setVisa}
              title="Thanh toán 257.000 đ bằng visa"
              imgsrc={require('../../../assets/images/Cart/visa.png')}
            />
            <PaymentOptions
              selected={this.state.master}
              click={this.setMaster}
              title="Thanh toán 257.000 đ bằng master card"
              imgsrc={require('../../../assets/images/Cart/master.png')}
            />
            <PaymentOptions
              selected={this.state.atm}
              click={this.setAtm}
              title="Thanh toán 257.000 đ bằng thẻ nội địa"
              imgsrc={require('../../../assets/images/Cart/atm.png')}
            />
            {this.state.showCVC ? (
              <CVC
                value={this.state.expiry}
                change={event => this.handleChange(event.nativeEvent.text)}
              />
            ) : null} */}
            <TouchableOpacity
              disabled={this.state.formLoading}
              onPress={this.onSubmit}
              style={payment_style.touchableView(this.state.formLoading)}>
              <Text style={payment_style.btnTextStyle}>TIẾP TỤC</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const payment_style = {
  touchableView: (loading) => ({
    height: 0.076 * screenHeight,
    backgroundColor: loading ? '#9e9e9e' : 'rgb(236,178,45)',
    marginTop: 0.027 * screenHeight,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0.027 * screenHeight,
  }),
  btnTextStyle: {
    color: 'white',
    fontFamily: 'Roboto-BoldCondensed',
    justifyContent: 'center',
    fontSize: 19,
  },
  txtStyleHeader: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: 'rgb(215,84,14)',
    marginTop: 0.03 * screenHeight,
  },
};
const mapStateToProps = (state) => {
  return {
    checkoutInfo: state.checkoutReducer.checkout,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxResetCart: () => dispatch(resetCart()),
    reduxResetCheckout: () => dispatch(resetCheckout()),
    reduxSavePayment: (paymentMethod) => dispatch(savePayment(paymentMethod)),
    reduxSaveOrder: (order) => dispatch(createCheckout(order)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Payment);
