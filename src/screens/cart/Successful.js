import React from 'react';
import {View, Text, TouchableOpacity, Image, Dimensions} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {Link} from 'react-router-native';
import {connect} from 'react-redux';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const Success = () => {
  return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      <View style={{flex: 1}}>
        <NavigationHeader title="THÔNG BÁO ĐƠN HÀNG" to="/landing" back={true} />
      </View>
      <View style={{flex: 6.2}}>
        <Image
          style={{height: undefined, width: undefined, flex: 1}}
          resizeMode="contain"
          source={require('../../../assets/images/Cart/success.png')}
        />
      </View>
      <View style={{flex: 2.33, backgroundColor: 'rgb(243,243,243)'}}>
        <Link
          component={TouchableOpacity}
          to="/landing"
          style={{
            backgroundColor: 'rgb(236,178,45)',
            marginLeft: 0.04 * screenWidth,
            width: 0.92 * screenWidth,
            height: 0.0761 * screenHeight,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontWeight: 'bold'}}>
            TIẾP TỤC MUA HÀNG
          </Text>
        </Link>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    checkout: state.checkoutReducer.checkout,
  };
};
export default connect(mapStateToProps, null)(Success);

