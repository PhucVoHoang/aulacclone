import React, { Component } from "react";
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  Platform,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Keyboard,
  ActivityIndicator,
  TouchableWithoutFeedback,
} from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import { CheckBox } from "react-native-elements";

import NavigationHeader from "../../components/navigation/navigationHeader";
import ShippingDropdown from "../../components/shipping/shippingDropdown";
import DeliveryOptions from "../../components/shipping/deliveryOptions";
import { getUserCredentials } from "../../services/storageServices";

import {
  saveAddress,
  saveShipping,
  getAddresses,
} from "../../redux/actions/checkoutActions";
import WrongLogin from "../../components/authentication/wrongLogin";
const wrong_img = require("../../../assets/images/wrong.png");

import API from "../../services/api";

const screenWidth = Math.round(Dimensions.get("window").width);
const screenHeight = Math.round(Dimensions.get("window").height);

let data = [
  {
    value: "Ho Chi Minh",
  },
  {
    value: "Ha Noi",
  },
  {
    value: "Da Nang",
  },
];

class shippingDetails extends Component {
  state = {
    normal: true,
    store: false,
    express: false,
    date: new Date(),
    mode: "date",
    show: false,
    popup: true,
    time: false,
    day: false,
    showDate: false,
    showTime: false,
    dateValue: "Ngày",
    timeValue: "Giờ",
    address: null,
    city: null,
    first_name: null,
    last_name: null,
    phone: null,
    email: null,
    formLoading: false,
    errorPopup: false,
    addresses: [],
    isNewAddress: false,
    storeAddress: null,
    stores: [],
  };
  _selectStore(text) {
    this.setState({ storeAddress: text });
  }
  setDate = (event, date) => {
    date = date || this.state.date;
    const formattedDate = `${date.getDate()}/${date.getMonth() +
      1}/${date.getFullYear()}`;
    this.setState({
      show: Platform.OS === "ios",
      date,
      dateValue: formattedDate,
      showDate: false,
    });
  };
  addZero = (i) => {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  };
  setTime = (event, time) => {
    time = time || this.state.time;
    const formattedDate = `${this.addZero(time.getHours())}:${this.addZero(
      time.getMinutes()
    )}`;
    this.setState({
      show: Platform.OS === "ios",
      time,
      timeValue: formattedDate,
      showTime: false,
    });
  };
  onSubmit = () => {
    if (this.state.phone === "") {
      this.setState({
        errorPopup: true,
      });
      return
    }
    if (this.state.address || this.state.storeAddress) {
      this.setState({
        formLoading: true,
      });
      let address = {
        billing: {
          address1: {
            "0": this.state.store
              ? `Ngày: ${this.state.dateValue}, Giờ: ${
                  this.state.timeValue
                }, Tại: ${this.state.storeAddress}`
              : this.state.address,
          },
          use_for_shipping: "true",
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          email: this.state.email,
          city: "Ho Chi Minh",
          state: "HCM",
          postcode: "700000",
          country: "VN",
          phone: this.state.phone,
        },
        shipping: {
          address1: {
            "0": this.state.store
              ? this.state.storeAddress
              : this.state.address,
          },
          use_for_shipping: "true",
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          email: this.state.email,
          city: "Ho Chi Minh",
          state: "HCM",
          postcode: "700000",
          country: "VN",
          phone: this.state.phone,
        },
      };
      let shippingMethod = "flatrate_flatrate";
      API.saveAdress(address)
        .then((res) => props.reduxSaveAddress(address))
        .catch((err) => console.log(_.get(err, "data")));
      if (this.state.isNewAddress) API.addAdress(address.billing);
      API.saveShipping({ shipping_method: shippingMethod })
        .then((res) => props.reduxSaveShipping(shippingMethod))
        .catch((err) => console.log(_.get(err, "data")));
      this.props.history.push({
        pathname: "/shopping/payment",
        gone: "/shopping/shipping",
      });
    } else {
      this.setState({
        errorPopup: true,
      });
    }
  };
  componentDidMount() {
    API.getAdresses()
      .then((res) => {
        this.setState({ addresses: res.data });
        props.reduxGetAddresses(res.data);
      })
      .catch((err) => console.log(_.get(err, "data")));
    API.getStores({ page: 1 })
      .then((res) => {
        this.setState({ stores: res.data.data });
      })
      .catch((err) => console.log(_.get(err, "data")));
    getUserCredentials().then((credential) => {
      if (credential.username && credential.password) {
        const profile = JSON.parse(credential.username);
        this.setState({
          first_name: profile.first_name,
          last_name: profile.last_name,
          phone: profile.phone,
          email: profile.email,
        });
      }
    });
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "rgb(255,255,255)" }}>
        <View style={{ flex: 1.1 }}>
          <NavigationHeader
            title="THANH TOÁN"
            to={{ pathname: "/shopping/cart", state: { go: "/" } }}
            back={true}
          />
        </View>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View
            style={{
              flex: 8.9,
              width: screenWidth * 0.92,
              marginLeft: 0.04 * screenWidth,
            }}
          >
            <KeyboardAvoidingView
              style={{
                flex: 24,
                flexDirection: "column",
                justifyContent: "center",
              }}
              behavior="padding"
              enabled
              keyboardVerticalOffset={10}
            >
              <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={shipping_style.headerStyle}>
                  Thông tin cá nhân
                </Text>
                <View style={shipping_style.viewContainerStyle}>
                  <TextInput
                    style={shipping_style.textInputStyle}
                    placeholder="Họ"
                    placeholderTextColor="rgb(151,151,151)"
                    value={this.state.last_name}
                    onChangeText={(text) => {
                      this.setState({ last_name: text });
                    }}
                  />
                </View>
                <View style={shipping_style.viewContainerStyle}>
                  <TextInput
                    style={shipping_style.textInputStyle}
                    placeholder="Tên"
                    placeholderTextColor="rgb(151,151,151)"
                    value={this.state.first_name}
                    onChangeText={(text) => {
                      this.setState({ first_name: text });
                    }}
                  />
                </View>
                <View style={shipping_style.viewContainerStyle}>
                  <TextInput
                    style={shipping_style.textInputStyle}
                    placeholder="Số điện thoại"
                    placeholderTextColor="rgb(151,151,151)"
                    value={this.state.phone}
                    onChangeText={(text) => {
                      this.setState({ phone: text });
                    }}
                  />
                </View>
                <View style={shipping_style.viewContainerStyle}>
                  <TextInput
                    style={shipping_style.textInputStyle}
                    placeholder="Email"
                    placeholderTextColor="rgb(151,151,151)"
                    value={this.state.email}
                    onChangeText={(text) => {
                      this.setState({ email: text });
                    }}
                  />
                </View>
                <Text style={shipping_style.headerStyle}>
                  Địa chỉ nhận hàng
                </Text>
                <ShippingDropdown
                  label="Chọn địa chỉ nhận hàng"
                  data={this.state.addresses}
                  selectedValue={this.state.address}
                  onChangeText={(text) => {
                    this.setState({ address: text });
                  }}
                />
                <View style={shipping_style.viewContainerStyle}>
                  <TextInput
                    style={shipping_style.textInputStyle}
                    placeholder="Địa chỉ"
                    placeholderTextColor="rgb(151,151,151)"
                    value={this.state.address}
                    onChangeText={(text) => {
                      this.setState({ address: text });
                    }}
                  />
                </View>
                {/* <ShippingDropdown label="Thành phố, tỉnh" data={data} /> */}
                {/*<View
              style={[
                shipping_style.viewContainerStyle,
                {
                  borderWidth: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                },
              ]}>
              <ShippingDropdown
                label="Thành phố, tỉnh"
                data={data}
                width={0.4}
              />
              <ShippingDropdown
                label="Thành phố, tỉnh"
                data={data}
                width={0.4}
              />
            </View>*/}
                <View
                  style={{
                    marginTop: 5,
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <CheckBox
                    center
                    //checkedIcon="dot-circle-o"
                    //uncheckedIcon="circle-thin"
                    checked={this.state.isNewAddress}
                    onPress={() =>
                      this.setState({ isNewAddress: !this.state.isNewAddress })
                    }
                    onIconPress={() =>
                      this.setState({ isNewAddress: !this.state.isNewAddress })
                    }
                    onLongIconPress={() =>
                      this.setState({ isNewAddress: !this.state.isNewAddress })
                    }
                    checkedColor="rgb(215,84,14)"
                    uncheckedColor="rgb(160,158,156)"
                    containerStyle={{
                      width: 30,
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                    }}
                  />
                  <Text style={{ color: "black" }}>
                    {"Bạn có muốn lưu địa chỉ bạn không?"}
                  </Text>
                </View>
                <Text style={shipping_style.headerStyle}>
                  Hình thức giao hàng
                </Text>

                <DeliveryOptions
                  title1="Giao hàng mặc định"
                  title2="Miễn phí, thời gian giao hàng từ 5 - 7 ngày"
                  date={false}
                  check={this.state.normal}
                  clicked={() => {
                    if (!this.state.normal) {
                      this.setState((prevState) => ({
                        normal: !prevState.normal,
                        store: false,
                        express: false,
                      }));
                    }
                  }}
                  height={0.112}
                />
                <DeliveryOptions
                  title1="Lấy tại cửa hàng"
                  title2="Thời gian làm việc từ 8h00 sáng đến 20h00"
                  date={this.state.popup}
                  setDate={this.setDate}
                  setTime={this.setTime}
                  showdate={this.state.showDate}
                  showtime={this.state.showTime}
                  dateValue={this.state.dateValue}
                  timeValue={this.state.timeValue}
                  stores={this.state.stores}
                  onSelectStore={this._selectStore.bind(this)}
                  showAddress={true}
                  activatedate={() => {
                    this.setState({
                      showDate: true,
                      showTime: false,
                    });
                  }}
                  activatetime={() => {
                    this.setState({
                      showDate: false,
                      showTime: true,
                    });
                  }}
                  check={this.state.store}
                  clicked={() => {
                    if (!this.state.store) {
                      this.setState((prevState) => ({
                        store: !prevState.store,
                        normal: false,
                        express: false,
                      }));
                    }
                  }}
                  height={0.251}
                />
                {/* <DeliveryOptions
              title1="Giao hàng nhanh"
              check={this.state.express}
              title2="phí dịch vụ 15.000 đ, thời gian giao hàng từ 1 - 2 ngày"
              date={false}
              clicked={() => {
                if (!this.state.express){
                  this.setState(prevState => ({
                    express: !prevState.express,
                    normal: false,
                    store: false,
                  }));
                }
              }}
              height={0.112}
            /> */}
                <TouchableOpacity
                  onPress={this.onSubmit}
                  style={{
                    width: 0.92 * screenWidth,
                    height: 0.076 * screenHeight,
                    marginTop: 0.0421 * screenHeight,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: this.state.formLoading
                      ? "#CCCCCC"
                      : "rgb(236,178,45)",
                  }}
                >
                  <Text style={{ color: "white", fontWeight: "bold" }}>
                    TIẾP TỤC
                  </Text>
                </TouchableOpacity>
              </ScrollView>
            </KeyboardAvoidingView>
          </View>
        </TouchableWithoutFeedback>
        {this.state.errorPopup ? (
          <WrongLogin
            visible={true}
            headerOne="CÓ LỖI XẢY RA"
            img_src={wrong_img}
            headerTwo="Bạn đã nhập sai hoặc thiếu thông tin đơn hàng, vui lòng thử lại."
            buttonOne_text="Đóng"
            buttonOne_bgColor="#6BBD12"
            buttonOne_disable={false}
            buttonOne_link="/shopping/shipping"
            clicked={() => {
              this.setState({ errorPopup: false });
            }}
          />
        ) : null}
        {this.state.formLoading ? (
          <View style={shipping_style.loading}>
            <ActivityIndicator style={{ flex: 1, alignSelf: "center" }} />
          </View>
        ) : null}
      </View>
    );
  }
}

const shipping_style = {
  headerStyle: {
    color: "rgb(173,14,23)",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    marginTop: 0.027 * screenHeight,
  },
  viewContainerStyle: {
    height: 0.076 * screenHeight,
    width: 0.92 * screenWidth,
    backgroundColor: "rgb(255,255,255)",
    borderRadius: 4,
    borderWidth: 1.3,
    borderColor: "rgb(236,236,238)",
    marginTop: 0.0135 * screenHeight,
  },
  textInputStyle: {
    marginLeft: 0.04 * screenWidth,
    height: 0.076 * screenHeight,
    color: "#333",
  },
  dropdownInputStyle: {
    marginLeft: 0.04 * screenWidth,
    marginBottom: 0.015 * screenHeight,
    borderBottomColor: "transparent",
    justifyContent: "center",
  },
  dropdownContainerStyle: {
    borderRadius: 4,
    borderWidth: 1.3,
    borderColor: "rgb(236,236,238)",
    height: 0.076 * screenHeight,
    justifyContent: "center",
    marginTop: 0.0135 * screenHeight,
  },
  imgStyle: {
    height: 0.044 * screenHeight,
    width: 0.086 * screenWidth,
    marginTop: 5,
  },
  loading: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
  },
};

const mapStateToProps = (state) => {
  return {
    address: state.checkoutReducer.address,
    addresses: state.checkoutReducer.addresses,
    shippingMethod: state.checkoutReducer.shippingMethod,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxSaveAddress: (address) => dispatch(saveAddress(address)),
    reduxGetAddresses: (addresses) => dispatch(getAddresses(addresses)),
    reduxSaveShipping: (shippingMethod) =>
      dispatch(saveShipping(shippingMethod)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(shippingDetails);
