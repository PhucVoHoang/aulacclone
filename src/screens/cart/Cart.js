import React from 'react';
import _ from 'lodash';
import {
  View,
  ScrollView,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  TextInput
} from 'react-native';
import NavigationHeader from '../../components/navigation/navigationHeader';
import {RFValue} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';
import {addToCart, updateCart, removeFromCart } from '../../redux/actions/cartActions';
import {createCheckout} from '../../redux/actions/checkoutActions';
import API from '../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const Cart = (props) => {
  const [formLoading, setFormLoading] = React.useState(false);
  const [isDiscount, setIsDiscount] = React.useState(false);
  const [coupon, setCoupon] = React.useState(null);
  const [total, setTotal] = React.useState(props.cartDetail ? props.cartDetail.formated_base_grand_total : 0);
  const [subtotal, setSubtotal] = React.useState(props.cartDetail ? props.cartDetail.formated_base_sub_total : 0);
  const [discount, setDiscount] = React.useState(0);

  React.useEffect(() => {
    setTotal(props.cartDetail ? props.cartDetail.formated_base_grand_total : 0);
    setSubtotal(props.cartDetail ? props.cartDetail.formated_base_sub_total : 0);
  }, [props.cartDetail]);

  const onSubmit = () => {
    props.history.push({
      pathname: '/shopping/shipping',
      gone: '/shopping/cart',
    });
  }

  const updateItemCart = (itemId, qty) => {
    const productDetail = _.find(props.cartDetail.items, ['id', itemId]);
    if (productDetail) {
      API.updateItemToCart({
        qty: { [itemId]: qty }
      }).then(res => {
        props.reduxUpdateCart(productDetail.product.id, qty, _.get(res, 'data'));
        setTotal(res.data.formated_base_grand_total);
        setDiscount(res.data.base_sub_total - res.data.base_grand_total);
        setSubtotal(res.data.formated_base_sub_total);
      }).catch(err => console.log(_.get(err, 'data')))
    } 
  }

  const removeItemToCart = (itemId) => {
    const productDetail = _.find(props.cartDetail.items, ['id', itemId]);
    if (productDetail) {
      API.removeItemToCart(itemId).then(res => {
        props.reduxRemoveFromCart(productDetail.product.id, _.get(res, 'data'));
        setTotal(res.data.formated_base_grand_total);
        setDiscount(res.data.base_sub_total - res.data.base_grand_total);
        setSubtotal(res.data.formated_base_sub_total);
      }).catch(err => console.log(_.get(err, 'data')))
    } 
  }

  const applyCoupon = (coupon) => {
    console.log(coupon);
      if(coupon) {
        API.addCouponFromCart(coupon).then(json => {
          console.log('addCouponFromCart', json)
          if(json.data) {
            //(json.data);
            setTotal(json.data.formated_base_grand_total);
            setDiscount(json.data.base_sub_total - json.data.base_grand_total);
            setSubtotal(json.data.formated_base_sub_total);
            setIsDiscount(true);
          }
        });
      } else {
          API.removeCouponFromCart(coupon).then(json => {
            if(json.data) {
              //props.reduxReplaceCart(json.data);
              setTotal(json.data.formated_base_grand_total);
              setDiscount(0);
              setSubtotal(json.data.formated_base_sub_total);
              setIsDiscount(false);
            }
          });
        }
    }
  return (
    <View style={{flex: 1}}>
      <View style={{height: 0.1 * screenHeight, width: '100%'}}>
        <NavigationHeader title="GIỎ HÀNG" to={props.location.state.go} back={true} />
      </View>
      <ScrollView style={{height: 0.9 * screenHeight, width: '100%'}}>
        <ScrollView
          contentContainerStyle={{
            flex: 1,
          }}>
            {
              props.cartDetail && props.cartDetail.items.map(item => (
                <View style={styles.productView} key={item.id}>
                  <View style={{flex: 2, flexDirection: 'row'}}>
                    <Image
                      style={{height: '100%', width: 0.12 * screenWidth}}
                      resizeMode="contain"
                      source={{uri: _.get(item, 'product.image_url')}}
                    />
                    <View style={{flexDirection: 'column'}}>
                      <Text
                        style={{
                          marginLeft: 0.0338 * screenWidth,
                          marginTop: 0.0135 * screenHeight,
                        }}>
                        {_.get(item, 'name')}
                      </Text>
                      <Text
                        style={{
                          marginLeft: 0.0338 * screenWidth,
                          marginTop: 0.0135 * screenHeight,
                          color: 'red',
                        }}>
                        {_.get(item, 'formated_total').replace(',00', '')}
                      </Text>
                    </View>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <TouchableOpacity onPress={() => {
                        if (_.get(item, 'quantity') > 1) {
                          updateItemCart(_.get(item, 'id'),_.get(item, 'quantity') - 1);
                        } else if (_.get(item, 'quantity') == 1){
                          removeItemToCart(_.get(item, 'id'));
                        }
                      }}
                    >
                        <Image
                          style={{
                            marginTop: 0.015 * screenHeight,
                            height: 0.0603 * screenWidth,
                            width: 0.0603 * screenWidth,
                          }}
                          resizeMode="contain"
                          source={require('../../../assets/images/Cart/minus.png')}
                        />
                      </TouchableOpacity>
                      <Text
                        style={{
                          marginTop: 0.02 * screenHeight,
                          marginLeft: 0.0241 * screenWidth,
                        }}>
                        {_.get(item, 'quantity', 0)}
                      </Text>
                      <TouchableOpacity onPress={() => {
                        if (_.get(item, 'quantity')) {
                          updateItemCart(_.get(item, 'id'),_.get(item, 'quantity') + 1);
                        }}}
                      >
                        <Image
                          style={{
                            marginTop: 0.015 * screenHeight,
                            marginLeft: 0.0241 * screenWidth,
                            height: 0.0603 * screenWidth,
                            width: 0.0603 * screenWidth,
                          }}
                          resizeMode="contain"
                          source={require('../../../assets/images/Cart/plus.png')}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => {
                        removeItemToCart(_.get(item, 'id'));
                      }}
                      >
                        <Image
                          style={{
                            marginTop: 0.015 * screenHeight,
                            marginLeft: 0.0406 * screenWidth,
                            height: 0.0603 * screenWidth,
                            width: 0.0603 * screenWidth,
                          }}
                          resizeMode="contain"
                          source={require('../../../assets/images/Cart/del.png')}
                        />
                      </TouchableOpacity>
                    </View>
                </View>
              ))
            }
          <Text
            style={{
              marginTop: 0.0475 * screenHeight,
              marginLeft: 0.04 * screenWidth,
            }}>
            Nhập mã giảm giá
          </Text>
          <TextInput
            placeholder="Nhập mã giảm giá"
            placeholderTextColor = "rgba(0,0,0,0.3)"
            onChangeText={text => applyCoupon(text)}
            style={{
              height: 0.076 * screenHeight,
              marginTop: 0.0122 * screenHeight,
              borderWidth: 1.3,
              borderColor: 'rgb(236,236,238)',
              marginLeft: 0.04 * screenWidth,
              width: 0.92 * screenWidth,
              color: "#333"
            }}
          />
          <View
            style={{
              width: screenWidth,
              flexDirection: 'row',
              marginTop: 0.0203 * screenHeight,
              height: 0.0733 * screenHeight,
              justifyContent: 'space-between',
              alignItems: 'center',

              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.18,
              shadowRadius: 1.0,

              elevation: 0.3,
            }}>
            <Text style={{marginLeft: 0.04 * screenWidth}}>Tạm tính</Text>
            <Text style={{marginRight: 0.04 * screenWidth}}>
              {props.cartDetail && subtotal.replace(',00', '')}
            </Text>
          </View>
          {isDiscount && (<View
            style={{
              width: screenWidth,
              flexDirection: 'row',
              marginTop: 0.0203 * screenHeight,
              height: 0.0733 * screenHeight,
              justifyContent: 'space-between',
              alignItems: 'center',

              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.18,
              shadowRadius: 1.0,

              elevation: 0.3,
            }}>
            <Text style={{marginLeft: 0.04 * screenWidth}}>Giảm</Text>
            <Text style={{marginRight: 0.04 * screenWidth}}>
              - {discount.toString().replace(',00', '')} đ
            </Text>
          </View>)}
          <View
            style={{
              marginTop: 0.0271 * screenHeight,
              height: 0.267 * screenHeight,
              flexDirection: 'column',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                shadowColor: '#000',
                marginLeft: 0.04 * screenWidth,
                width: 0.92 * screenWidth,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{marginTop: RFValue(6)}}>Tổng cộng</Text>
                <Text style={{fontSize: RFValue(25), color: 'rgb(173,14,23)'}}>
                {props.cartDetail && total.replace(',00', '')}
                </Text>
              </View>
            </View>
            <TouchableOpacity
              disabled={formLoading}
              onPress={onSubmit}
              style={{
                width: 0.92 * screenWidth,
                height: 0.076 * screenHeight,
                marginLeft: 0.04 * screenWidth,
                marginTop: 0.0421 * screenHeight,
                backgroundColor: formLoading ? '#9e9e9e' : 'rgb(236,178,45)',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>ĐẶT HÀNG</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </ScrollView>
      {formLoading ? (
        <View style={styles.loading}>
          <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
        </View>  
      ) : null}
    </View>
  );
};
const styles = {
  productView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 0.04 * screenWidth,
    paddingLeft: 0.04 * screenWidth,
    height: 0.1 * screenHeight,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems:'center',
  },
};
const mapStateToProps = (state) => {
  return {
    cartItems: state.cartReducer.cartItems,
    cartDetail: state.cartReducer.cartDetail,
    checkout: state.checkoutReducer.checkout,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxAddToCart: (product, cartDetail) => dispatch(addToCart(product, cartDetail)),
    reduxUpdateCart: (productId, quantity, cartDetail) => dispatch(updateCart(productId, quantity, cartDetail)),
    reduxRemoveFromCart: (productId, cartDetail) => dispatch(removeFromCart(productId, cartDetail)),
    reduxReplaceCart: (cartDetail) => dispatch(replaceCart(cartDetail)),
    reduxCreateCheckout: (checkoutInfo) => dispatch(createCheckout(checkoutInfo)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
