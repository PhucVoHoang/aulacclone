import { Text } from 'react-native';
import DebugConfig from './DebugConfig';
import AppConfig from './AppConfig';

export default () => {
  if (__DEV__) {
    console.disableYellowBox = !DebugConfig.yellowBox;
  }
  Text.defaultProps.allowFontScaling = AppConfig.allowTextFontScaling;
};
