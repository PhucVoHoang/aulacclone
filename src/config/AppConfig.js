const DEV_URL = 'http://113.161.94.107:8082';
const DEV_EC_URL = 'http://113.161.94.107:8080';
const PROD_URL = '';
const LOCAL_URL = 'http://127.0.0.1:8000';
const CHAT_URL = 'http://113.161.94.107:3000';

export default {
  baseURL: DEV_URL,
  baseECURL: DEV_EC_URL,
  baseChatURL: CHAT_URL,
  allowTextFontScaling: true,
  videoCatid: 2,
  cookingCatid: 1,
  companyCatid: 3,
  faqId: 7,
  giaviId: 1,
  khuyenmai: 8,
  sanphammoi: 9,
  flashsale: 10,
  hoatdong: 11
};

