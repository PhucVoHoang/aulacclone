import React from 'react';
import {View, Text} from 'react-native';

const Specifications = (props) => {
  return (
    <View style={{flex: 1}}>
      <View style={style.greyRow}>
        <View style={style.leftColumn}>
          <Text style={style.greyishBrownText}> Khối lượng tịnh </Text>
        </View>
        <View style={style.rightColumn}>
          <Text style={style.warmGreyText}>
              {props.weight ? `${props.weight.value} ${props.weight.unit}` : "90 gram"}
          </Text>
        </View>
      </View>
      {
        !props.weight && (
          <>
        <View style={style.whiteRow}>
        <View style={style.leftColumn}>
          <Text style={style.greyishBrownText}> Net wt </Text>
        </View>
        <View style={style.rightColumn}>
          <Text style={style.warmGreyText}> 3.15 oz </Text>
        </View>
      </View>
      <View style={style.greyRow}>
        <View style={style.leftColumn}>
          <Text style={style.greyishBrownText}> Khối lượng tịnh </Text>
        </View>
        <View style={style.rightColumn}>
          <Text style={style.warmGreyText}> 90 gram </Text>
        </View>
      </View>
      <View style={style.whiteRow}>
        <View style={style.leftColumn}>
          <Text style={style.greyishBrownText}> Net wt </Text>
        </View>
        <View
          style={style.rightColumn}>
          <Text style={style.warmGreyText}> 3.15 oz </Text>
        </View>
      </View>
          </>
        )
      }
      <View style={{ height: 20 }} />
    </View>
  );
};

const style = {
  greyRow: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgb(247,248,250)',
    height: 60
  },
  whiteRow: {
    flex: 1, 
    flexDirection: 'row',
    backgroundColor: 'white',
    height: 60
  },
  leftColumn: {
    flex: 1,
    borderColor: 'rgb(231,233,234)',
    borderBottomWidth: 1,
    borderRightWidth: 1,
    justifyContent: 'center',
  },
  rightColumn: {
    flex: 2,
    borderColor: 'rgb(231,233,234)',
    borderBottomWidth: 1,
    borderRightWidth: 1,
    justifyContent: 'center',
  },
  greyishBrownText: {
    textAlign: 'center',
    color: 'rgb(81,81,81)',
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
  },
  warmGreyText: {
    textAlign: 'center',
    color: 'rgb(151,151,151)',
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
  }
};

export default Specifications;
