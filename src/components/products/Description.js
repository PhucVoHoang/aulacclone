import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import HTML from 'react-native-render-html';

const Description = (props) => {
  const {
    text='',
    renderText=false,
  } = props;
  let textDescription = text.replace(/Hướng dẫn/g, "<br/><br/>Hướng dẫn")
                 .replace('HƯỚNG DẪN', "<br/><br/>HƯỚNG DẪN")
                 .replace('src="//', 'src="https://')
                 .replace('Chú ý', "<br/><br/>Chú ý")
                 .replace('CHÚ Ý', "<br/><br/>CHÚ Ý")
                 .replace('CH&Uacute; &Yacute;', "<br/><br/>CHÚ Ý")
                 .replace('Ch&uacute; &yacute;', "<br/><br/>Chú ý")
                 .replace('C&Aacute;CH BẢO QUẢN:', "<br/><br/>C&Aacute;CH BẢO QUẢN:")
                 .replace('C&Aacute;CH SỬ DỤNG:', "<br/><br/>C&Aacute;CH SỬ DỤNG:")
                 .replace('Usage:', "<br/><br/>Usage:")
                 .replace('Ingredients:', "<br/><br/>Ingredients:")
                 .replace('INGREDIENTS:', "<br/><br/>INGREDIENTS:")
                 .replace('Note:', "<br/><br/>Note:")
                 .replace('NOTE:', "<br/><br/>NOTE:")
                 .replace('Storage:', "<br/><br/>Storage:")
                 .replace('STORAGE:', "<br/><br/>STORAGE:");

  return (
    <View style={{marginTop: 6, marginBottom: 6, marginLeft: 12, marginRight: 12}}>
      {
        renderText ? (
          <HTML html={textDescription} style={{fontFamily: 'Roboto-Regular'}} imagesMaxWidth={Dimensions.get('window').width} />
        ) : (
          <>
            <Text style={{fontFamily: 'Roboto-Regular'}}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Energistically
            whiteboard compelling core competencies via customized e-tailers.
            </Text>
      
            <Text>
              {' '}
              Energistically whiteboard compelling core competencies via customized
              e-tailers. Monotonectally target market positioning total linkage
              vis-a-vis proactive mindshare.{' '}
            </Text>
          </>
        )
      }

    </View>
  );
};

export default Description;
