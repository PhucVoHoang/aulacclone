import React from 'react';
import {View, Text, Dimensions, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const Recipe = ({ articles, gone }) => {
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          backgroundColor: 'rgb(231,233,234)',
          alignItems: 'center',
          height: 60
        }}>
        <Text  style={{marginLeft: 0.0386 * screenWidth,fontFamily: 'Roboto-BoldCondensed',}}>Danh mục công thức</Text>
      </View>

      {articles.length > 0 && 
      <Link
        component={TouchableOpacity}
        to={{
          pathname: '/profile/blogpost',
          state: {
            go: gone,
            postId: articles[0].id,
            data: articles[0],
            headline: 'Công thức'
          }
        }}>
      <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', height: 60}}>
        <Text style={{marginLeft: 0.0386 * screenWidth, fontFamily: 'Roboto-Regular'}}>
          {articles[0].title}
        </Text>
      </View>
      </Link>
      }
    </View>
  );
};

export default Recipe;
