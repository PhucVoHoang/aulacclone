import React, {Component} from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';

class customButton extends Component {
  render() {
    const {titleText, disable, bgColor, click, link} = this.props;
    return (
      <Link component={TouchableOpacity} to={link} disabled={disable} style={[styles.btnStyle, {backgroundColor: bgColor}]} onPress={click}>
        <Text style={{color: 'white', fontSize: RFValue(19),fontFamily: 'Roboto-BoldCondensed'}}>{titleText}</Text>
      </Link>
    );
  }
}
export default customButton;

const styles = {
  btnStyle: {
    flex: 1,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFCB08',
  },
};
