import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {RFValue} from 'react-native-responsive-fontsize';

class customTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {username: this.props.value, password: this.props.secure};
  }

  render() {
    const {placeholderText, secure} = this.props;

    return (
      <View style={{flex: 1, backgroundColor: 'white', borderRadius: 4}}>
        <View style={styles.flexHeight}>
          <View style={styles.flexOne} />
          <View style={styles.flexWidth}>
            <TextInput
              onChange={this.props.change}
              secureTextEntry={this.state.password}
              value={this.props.value}
              autoCapitalize = 'none'
              placeholderTextColor = "rgba(0,0,0,0.3)"
              style={{
                flex: 1,
                fontFamily: 'Roboto-Regular',
                color: "#333"
              }}
              placeholder={placeholderText}
            />
            {secure ? (
              <Icon
                style={{alignSelf: 'center'}}
                size={RFValue(20)}
                name="eye"
                onPress={() => {
                  this.setState(prevState => ({
                    password: !prevState.password,
                  }));
                }}
              />
            ) : this.props.value.toString().length > 0 ? (
              <Icon
                style={{alignSelf: 'center'}}
                size={RFValue(20)}
                name="x-circle"
                onPress={this.props.clear}
              />
            ) : null}
          </View>
          <View style={styles.flexOne} />
        </View>
      </View>
    );
  }
}
const styles = {
  flexOne: {
    flex: 1,
  },
  flexHeight: {
    flex: 1.5,
    flexDirection: 'row',
  },
  flexWidth: {
    flex: 11.16,
    flexDirection: 'row',
  },
};
export default customTextInput;
