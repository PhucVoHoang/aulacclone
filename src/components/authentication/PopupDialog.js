import React, {Component} from 'react';
import {View, Text, Image, Modal, Dimensions} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import CustomButton from '../../components/authentication/customButton';
import {Link} from 'react-router-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const modalWidth = screenWidth * 0.84;
const modalHeight = screenHeight * 0.47;

class PopupDialog extends Component {
    state = {
    visibleModal: this.props.visible,
  };
  render() {
      const src = this.props.img_src;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.visible}
       >
        <View style={styles.modalView}>
          <View style={styles.modalTop}>
            <View style={styles.modalTopLeft}>
              <View style={styles.opacityView} />
              <View style={styles.nullOpacityView} />
            </View>
            <View
              style={{
                width: modalWidth * 0.22,
                height: modalHeight * 0.18,
                backgroundColor: 'white',
                borderColor: 'white',
                borderTopLeftRadius: (modalHeight * 0.20) / 2,
                borderTopRightRadius: (modalHeight * 0.20) / 2,
              }}>
              <Image
                  resizeMode="contain"
                style={{
                  flex: 1,
                  width: undefined,
                  height: undefined,
                }}
                source={this.props.img_src}
              />
            </View>
            <View style={styles.modalTopLeft}>
              <View style={styles.opacityView} />
              <View style={styles.nullOpacityView} />
            </View>
          </View>
          <View
            style={{
              width: modalWidth,
              height: modalHeight,
              backgroundColor: 'white',
              flexDirection: 'column',
            }}>
            <Text
              style={{
                marginTop: 0.09 * modalHeight,
                textAlign: 'center',
                fontSize: RFValue(19),
                fontWeight: 'bold',
                color: '#515151',
              }}>
              {this.props.headerOne}
            </Text>
            <Text
              style={{
                marginTop: 0.06 * modalHeight,
                marginLeft: 0.07 * modalWidth,
                marginRight: 0.07 * modalWidth,
                textAlign: 'center',
                color: '#979797',
              }}>
              {this.props.headerTwo}
            </Text>
            <View
              style={{
                marginLeft: 0.07 * modalWidth,
                marginRight: 0.07 * modalWidth,
                width: modalWidth * 0.86,
                height: modalHeight * 0.16,
                marginTop: modalHeight * 0.05,
              }}>
              <CustomButton
                titleText={this.props.buttonOne_text}
                bgColor={this.props.buttonOne_bgColor}
                disable={this.props.buttonOne_disable}
                link={this.props.buttonOne_link}
                click={this.props.clicked}
              />
            </View>
            <View
              style={{
                marginLeft: 0.07 * modalWidth,
                marginRight: 0.07 * modalWidth,
                width: modalWidth * 0.86,
                height: modalHeight * 0.16,
                marginTop: modalHeight * 0.04,
              }}>
              <CustomButton
                titleText={this.props.buttonTwo_text}
                bgColor={this.props.buttonTwo_bgColor}
                disable={this.props.buttonTwo_disable}
                click={this.props.clicked}
                link={this.props.buttonTwo_link}

              />
            </View>
            <View
              style={{
                marginLeft: 0.07 * modalWidth,
                marginRight: 0.07 * modalWidth,
                width: modalWidth * 0.86,
                height: modalHeight * 0.16,
                marginTop: modalHeight * 0.04,
              }}>
              <CustomButton
                titleText={this.props.buttonThree_text}
                bgColor={this.props.buttonThree_bgColor}
                disable={this.props.buttonThree_disable}
                click={this.props.clicked}
                link={this.props.buttonThree_link}

              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  modalView: {
    width: modalWidth,
    height: modalHeight,
    alignSelf: 'center',
    top: screenHeight * 0.25,
    borderRadius: 20,
    flexDirection: 'column',
  },
  modalTop: {
    flexDirection: 'row',
    width: modalWidth,
    height: modalHeight * 0.18,
  },
  modalTopLeft: {
    width: modalWidth * 0.41,
    height: modalHeight * 0.18,
    flexDirection: 'column',
  },
  opacityView: {
    width: modalWidth * 0.41,
    height: modalHeight * 0.09,
    opacity: 0,
  },
  nullOpacityView: {
    width: modalWidth * 0.41,
    height: modalHeight * 0.09,
    opacity: 1,
    backgroundColor: 'white',
  },
};

export default PopupDialog;
