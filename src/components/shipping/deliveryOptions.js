import React from 'react';
import {
  Platform,
  Dimensions,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker';
import ShippingDropdown from './shippingDropdown';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const isIOS = Platform.OS === 'ios' 

const deliveryOptions = props => {
  const extraHeight = isIOS && (props.showdate || props.showtime) ? 220 : 0;
  return (
    <TouchableOpacity
      disabled={props.check}
      onPress={props.clicked}
      style={[
        style.viewContainerStyle,
        {
          height: (props.height * screenHeight) + extraHeight,
          flexDirection: 'row',
          alignItems: 'center',
        },
      ]}>
      <CheckBox
        center
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-thin"
        checked={props.check}
        onPress={props.clicked}
        onIconPress={props.clicked}
        onLongIconPress={props.clicked}
        checkedColor="rgb(215,84,14)"
        uncheckedColor="rgb(160,158,156)"
        containerStyle={{
          width: 30,
          justifyContent: 'flex-start',
          alignItems: 'flex-start',
        }}
      />
      <View style={{justifyContent: 'center'}}>
        <Text style={props.check ? style.text1On : style.text1Off}>
          {props.title1}
        </Text>
        <Text style={props.check ? style.text2On : style.text2Off}>
          {props.title2}
        </Text>
        {props.date ? (
          <View>
            <TouchableOpacity
              onPress={props.activatedate}
              disabled={!props.check}
              style={style.touchableView}>
              <Text>{props.dateValue}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={props.activatetime}
              disabled={!props.check}
              style={style.touchableView}>
              <Text>{props.timeValue}</Text>
            </TouchableOpacity>
          </View>
        ) : null}
        
        {props.showdate ? (
          <DateTimePicker
            value={new Date()}
            is24Hour={true}
            display="default"
            show={true}
            onChange={props.setDate}
          />
        ) : null}
      
        {props.showtime ? (
          <DateTimePicker
            value={new Date()}
            mode="time"
            is24Hour={true}
            display="default"
            show={true}
            onChange={props.setTime}
          />
        ) : null}
        {props.showAddress ? (<ShippingDropdown label="Chọn cửa hàng" data={props.stores} onChangeText={props.onSelectStore} />) : null}
      </View>
    </TouchableOpacity>
  );
};

const style = {
  viewContainerStyle: {
    width: 0.92 * screenWidth,
    backgroundColor: 'rgb(255,255,255)',
    borderRadius: 4,
    borderWidth: 1.3,
    borderColor: 'rgb(236,236,238)',
    marginTop: 0.0135 * screenHeight,
  },
  touchableView: {
    backgroundColor: 'rgb(243,243,243)',
    height: 0.038 * screenHeight,
    marginTop: 0.01 * screenHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text1Off: {
    fontSize: 16,
    color: 'rgb(151,151,151)',
  },
  text1On: {
    fontSize: 16,
    color: 'rgb(173,14,23)',
  },
  text2Off: {
    fontSize: 13,
    color: 'rgb(151,151,151)',
  },
  text2On: {
    fontSize: 13,
    color: 'black',
  },
};
export default deliveryOptions;
