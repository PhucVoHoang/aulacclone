import React from 'react';
import {Dimensions} from 'react-native';
import {Dropdown} from 'react-native-material-dropdown';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const shippingDropdown = props => {
  if (props.data.length == 0) return null;
  return (
    <Dropdown
      label={props.label}
      data={props.data}
      onChangeText={props.onChangeText}
      fontSize={12}
      containerStyle={style.dropdownContainerStyle}
      inputContainerStyle={style.dropdownInputStyle}
      valueExtractor={(item) => {
        return item.address1 ? item.address1[0] : (item.landmark ? `${item.name} (${item.landmark})` : item.value);
      }}
    />
  );
};

const style = {
  dropdownInputStyle: {
    marginLeft: 0.04 * screenWidth,
    marginBottom: 0.015 * screenHeight,
    borderBottomColor: 'transparent',
    justifyContent: 'center',
  },
  dropdownContainerStyle: {
    borderRadius: 4,
    borderWidth: 1.3,
    borderColor: 'rgb(236,236,238)',
    height: 0.076 * screenHeight,
    justifyContent: 'center',
    marginTop: 0.0135 * screenHeight,
  },
};
export default shippingDropdown;
