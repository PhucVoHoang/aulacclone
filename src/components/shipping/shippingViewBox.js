import React from 'react';
import {TextInput, View, Dimensions} from 'react-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const shippingViewBox = props => {
  return (
    <View style={style.viewContainerStyle}>
      <TextInput
        style={style.textInputStyle}
        placeholder={props.placeholderText}
        placeholderTextColor="rgb(151,151,151)"
      />
    </View>
  );
};

const style = {
  viewContainerStyle: {
    height: 0.076 * screenHeight,
    width: 0.92 * screenWidth,
    backgroundColor: 'rgb(255,255,255)',
    borderRadius: 4,
    borderWidth: 1.3,
    borderColor: 'rgb(236,236,238)',
    marginTop: 0.0135 * screenHeight,
  },
  textInputStyle: {
    marginLeft: 0.04 * screenWidth,
    color: "#333"
  },
};

export default shippingViewBox;
