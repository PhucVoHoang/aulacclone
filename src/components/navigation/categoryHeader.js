import React from 'react';
import {
  View,
  Dimensions,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Link} from 'react-router-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const Header = props => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
      }}>
      <View style={{flex: 0.8}} />
      <View
        style={{
          flex: 1.2,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          marginLeft: 0.02 * screenWidth,
          marginRight: 0.02 * screenWidth,
        }}>
        <View
          style={{
            width: 0.5 * screenWidth,
            height: '70%',
            borderRadius: 5,
            elevation: 1,
            shadowColor: 'black',
            backgroundColor: 'white',
            shadowOpacity: 0.2,
            shadowRadius: 4,
            paddingTop: 0,
            paddingBottom: 0,
          }}>
          <TextInput
            placeholder="Tìm kiếm..."
            placeholderTextColor = "rgba(0,0,0,0.3)"
            style={{
              width: '96%',
              height: '100%',
              paddingTop: 0,
              paddingBottom: 0,
              marginLeft: '4%',
              color: "#333"
            }}
          />
        </View>
        <View style={{width: 0.06 * screenWidth, height: 0.06 * screenWidth}}>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/shopping/cart',
              state: {go: props.go},
            }}
            style={{flex: 1}}>
            <Image
              resizeMode="stretch"
              style={{width: undefined, height: undefined, flex: 1}}
              source={require('../../../assets/images/cartc.png')}
            />
          </Link>
        </View>
        <Link
          component={TouchableOpacity}
          to={{
            pathname: '/underconstruction',
            state: {go: props.go},
          }}>
          <Image
            style={{
              width: 0.05 * screenWidth,
              height: 0.06 * screenWidth,
              marginTop: 0.01 * screenHeight,
            }}
            reszieMode="stretch"
            source={require('../../../assets/images/qrc.png')}
          />
        </Link>
        <Link
          component={TouchableOpacity}
          to={{
            pathname: '/chat',
            state: {go: props.go},
          }}>
          <Image
            style={{width: 0.06 * screenWidth, height: 0.06 * screenWidth}}
            reszieMode="stretch"
            source={require('../../../assets/images/chatc.png')}
          />
        </Link>
      </View>
    </View>
  );
};
export default Header;
