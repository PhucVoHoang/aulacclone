import React, {Component} from 'react';
import {View, Image, Dimensions, TouchableOpacity, Text} from 'react-native';
import {Link} from 'react-router-native';
import PopUp from '../../components/authentication/PopupDialog'
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const houseOn = require('../../../assets/images/house.png');
const houseOff = require('../../../assets/images/houe_Off.png');
const commentOff = require('../../../assets/images/comment.png');
const commentOn = require('../../../assets/images/commentOn.png');
const squareOff = require('../../../assets/images/square.png');
const squareOn = require('../../../assets/images/squareOn.png');
const alarmOff = require('../../../assets/images/alarm.png');
const alarmOn = require('../../../assets/images/alarmOn.png');
const profileOff = require('../../../assets/images/profile.png');
const profileOn = require('../../../assets/images/profileOn.png');
const wrong_img = require("../../../assets/images/wrong.png");
class Footer extends Component {
  state = {
    home: this.props.home,
    square: this.props.square,
    comment: this.props.comments,
    alarm: this.props.alarm,
    profile: this.props.profile,
    errorPopup: false
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          flexGrow: 1,
          shadowColor: 'black',
          backgroundColor: 'white',
          shadowOpacity: 0.2,
          shadowRadius: 4,
          elevation: 6,
        }}>
          {this.state.errorPopup && 
              <PopUp
                visible={true}
                headerOne="CÓ LỖI XẢY RA"
                img_src={wrong_img}
                headerTwo="Tính năng hiện đang phát triển, xin vui lòng thử lại sau"
                buttonOne_text="Đóng"
                buttonOne_bgColor="#6BBD12"
                buttonOne_disable={false}
                buttonOne_link="/landing"
                clicked={() => {
                  this.setState({ errorPopup: false });
                }}
              />
          }
        <Link component={TouchableOpacity} to="/landing">
          <View style={style.imageContainer}>
            <Image
              style={ style.image }
              source={this.state.home ? houseOn : houseOff}
            />
          </View>
          <Text style={ this.state.home ? style.textOff : style.textOn }>
            Trang chủ
          </Text>
        </Link>
        <Link component={TouchableOpacity} to="/categories/0">
          <View style={style.imageContainer}>
            <Image
              style={ style.image }
              source={this.state.square ? squareOn : squareOff}
            />
          </View>
          <Text style={ this.state.square ? style.textOff : style.textOn }>
            Sản phẩm
          </Text>
        </Link>
        {/* <Link component={TouchableOpacity}  to={{
            pathname: '/chat',
            state: {go: '/landing', back: false},
        }}>
           <View style={style.imageContainer}>
            <Image
              style={ style.image }
              source={this.state.comment ? commentOn : commentOff}
            />
          </View>
          <Text style={ this.state.comment ? style.textOff : style.textOn }>
            Trò chuyện
          </Text>
        </Link> */}
        <TouchableOpacity onPress={()=>{ this.setState(()=>({errorPopup: true}))}}>
        <View style={style.imageContainer}>
            <Image
              style={ style.image }
              source={this.state.comment ? commentOn : commentOff}
            />
          </View>
          <Text style={ this.state.comment ? style.textOff : style.textOn }>
            Trò chuyện
          </Text>
        </TouchableOpacity>
        <Link component={TouchableOpacity} to="/notification">
          <View style={style.imageContainer}>
            <Image
              style={ style.image }
              source={this.state.alarm ? alarmOn : alarmOff}
            />
          </View>
          <Text style={ this.state.alarm ? style.textOff : style.textOn }>
            Tin Tức
          </Text>
        </Link>
        
        <Link component={TouchableOpacity} to="/profile">
          <View style={style.imageContainer}>
            <Image
              style={ style.image }
              source={this.state.profile ? profileOn : profileOff}
            />
          </View>
          <Text style={ this.state.profile ? style.textOff : style.textOn }>
            Tôi
          </Text>
        </Link>
      </View>
    );
  }
}

const style = {
    imageContainer: {
      width: 0.07 * screenWidth,
      height: 0.07 * screenWidth,
      alignSelf: 'center',
    },
    image: {
      flex: 1,
      width: undefined,
      height: undefined, 
      resizeMode: 'contain'
    },
    textOn: {
      textAlign: 'center',
      fontFamily: 'Roboto-Regular',
      color: 'rgb(160,158,156)',
      fontSize: 12,
      paddingTop: 8
    },
    textOff: {
      textAlign: 'center',
      color: 'rgb(236,178,45)',
      fontFamily: 'Roboto-Regular',
      fontSize: 12,
      paddingTop: 8
    },
};
export default Footer;
