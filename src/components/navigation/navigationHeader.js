import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Link} from 'react-router-native';

const NavigationHeader = props => {
  return (
    <View
      style={{flex: 1, flexDirection: 'column', backgroundColor: '#ECB22D'}}>
      <View style={{flex: 1.87}} />
      <View style={{flex: 1.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
        <View style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingStart: 4}}>
          {props.back && 
          <Link component={TouchableOpacity} to={props.to}>
            <Icon
              component={Icon}
              size={RFValue(30)}
              color="white"
              name="chevron-left"
            />
          </Link>}
        </View>
        <View
          style={{
            flex: 4,
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}>
          <Text
            style={{fontSize: RFValue(19), color: 'white', fontFamily: 'Roboto-BoldCondensed', alignSelf:'center'}}>
            {props.title}
          </Text>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end', flexDirection: 'row'}}>
          {props.chat && (<>
            <Link component={TouchableOpacity} to={'/addchatgroup'} style={{marginRight: 12}}>
                <Icon
                  component={Icon}
                  size={RFValue(30)}
                  color="white"
                  name="account-group-outline"
                />
              </Link>
            <Link component={TouchableOpacity} to={'/addfriend'} style={{marginRight: 12}}>
              <Icon
                component={Icon}
                size={RFValue(30)}
                color="white"
                name="account-plus-outline"
              />
            </Link>
          </>)}
        </View>
      </View>
      <View style={{flex: 0.5}} />
    </View>
  );
};
export default NavigationHeader;
