import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  Dimensions,
} from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { Link } from "react-router-native";
import _ from "lodash";
import API from "../../services/api";
import { categoryListAction } from "../../redux/actions/productActions";

const screenWidth = Math.round(Dimensions.get("window").width);

class categoryToggle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryItems: [],
    };
    this._nodes = new Map();
  }

  componentDidMount() {
    console.log("componentDidMount: ", this.props.active);
    this.scrollToInitialPosition();
  }

  scrollToInitialPosition() {
    if (this.props && this.props.active) {
      const index = this.props.categoryItems.findIndex(
        (item) => item.id == this.props.active
      );
      if (index > 2) {
        if (index > this.props.categoryItems.length - 3) {
          setTimeout(() => {
            this.myScroll.scrollToEnd();
          }, 50);
        } else {
          const position = 0.3 * screenWidth * index;
          setTimeout(() => {
            this.myScroll.scrollTo({ x: position, animated: false });
          }, 50);
        }
      }
    }
  }

  render() {
    const { categoryItems } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.shadowBox}>
          <ScrollView
            ref={(ref) => (this.myScroll = ref)}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            scrollToOverflowEnabled={true}
            keyboardShouldPersistTaps={"handled"}
          >
            <Link
              component={TouchableOpacity}
              to={{ pathname: `/categories/0`, state: "flushData" }}
              style={
                this.props.active == 0
                  ? styles.touchableViewOn
                  : styles.touchableView
              }
            >
              <Text
                style={this.props.active == 0 ? styles.textOn : styles.textOff}
              >
                Tất cả
              </Text>
            </Link>
            {categoryItems &&
              categoryItems.map((item) => (
                <Link
                  ref={(ref) => this._nodes.set(item.id, ref)}
                  key={item.id}
                  component={TouchableOpacity}
                  to={{
                    pathname: `/categories/${item.id}`,
                    state: "flushData",
                  }}
                  style={
                    this.props.active == item.id
                      ? styles.touchableViewOn
                      : styles.touchableView
                  }
                >
                  <Text
                    style={
                      this.props.active == item.id
                        ? styles.textOn
                        : styles.textOff
                    }
                  >
                    {item.name}
                  </Text>
                </Link>
              ))}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = {
  touchableView: {
    width: 0.3 * screenWidth,
    height: "100%",
    justifyContent: "flex-end",
    paddingBottom: 3,
  },
  touchableViewOn: {
    width: 0.3 * screenWidth,
    height: "100%",
    justifyContent: "flex-end",
    paddingBottom: 3,
    borderBottomWidth: 2,
    borderBottomColor: "rgb(236,178,45)",
  },
  textOn: {
    textAlign: "center",
    fontSize: RFValue(16),
    color: "rgb(236,178,45)",
    fontFamily: "Roboto-Bold",
  },
  textOff: {
    textAlign: "center",
    fontSize: RFValue(16),
    fontFamily: "Roboto-Regular",
  },
  container: {
    flex: 1,
    flexGrow: 1,
    overflow: "hidden",
    paddingBottom: 10,
  },
  shadowBox: {
    width: "100%",
    height: "100%",
    shadowOffset: { width: 0, height: 1 },
    shadowColor: "black",
    backgroundColor: "white",
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 6,
  },
};
const mapStateToProps = (state) => {
  return {
    categoryItems: state.productReducer.category_list,
  };
};
export default connect(
  mapStateToProps,
  null
)(categoryToggle);
