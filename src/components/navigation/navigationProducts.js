import React from 'react';
import _ from 'lodash';
import {View, Text, TouchableOpacity, Dimensions, Image} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import Icon from 'react-native-vector-icons/Feather';
import {Link} from 'react-router-native';
import {connect} from 'react-redux';
import { ShareDialog } from 'react-native-fbsdk';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

shareLinkWithDialog = async () => {
  const SHARE_LINK_CONTENT = {
    contentType: 'link',
    contentUrl: 'http://www.aulac-vegetarian.com/',
    contentDescription: 'Wow, check out this great site!',
  };
  const canShow = await ShareDialog.canShow(SHARE_LINK_CONTENT);
  if (canShow) {
    try {
      const {isCancelled, postId} = await ShareDialog.show(
        SHARE_LINK_CONTENT,
      );
      if (isCancelled) {
        Alert.alert('Share cancelled');
      } else {
        Alert.alert('Share success with postId: ' + postId);
      }
    } catch (error) {
      Alert.alert('Share fail with error: ' + error);
    }
  }
};

const NavigationProducts = props => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ECB22D',
      }}>
      <View style={{flex: 1}} />
      <View
        style={{
          flex: 3.85,
          flexDirection: 'row',
        }}>
        <View style={{flex: 0.6}} />
        <View
          style={{
            flex: 16.8,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Link component={TouchableOpacity} to={props.parsePath}>
            <Icon size={RFValue(30)} color="white" name="chevron-left" />
          </Link>
          <View style={{width: 0.2 * screenWidth, height: 0.08 * screenWidth, display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Link
              component={TouchableOpacity}
              to={{
                pathname: '/shopping/cart',
                state:  {
                  go: '/categories/tatca',
                }
              }}
              style={{flex: 1}}>
              <Image
                resizeMode="stretch"
                style={{
                  width: 0.065 * screenWidth,
                  height: 0.065 * screenWidth,
                  marginTop: 0.007  * screenHeight,
                }}
                source={require('../../../assets/images/cart.png')}
              />
              {
                props.cartDetail && props.cartDetail.items_qty ? (
                  <View
                    style={{
                      width: 0.04 * screenWidth,
                      height: 0.04 * screenWidth,
                      backgroundColor: 'red',
                      borderRadius: 0.04 * screenWidth / 2,
                      position: 'absolute',
                      top: 0,
                      right: 0,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={{
                      textAlign: 'center',
                      color: '#fff',
                      fontSize: 0.025 * screenWidth,
                      fontWeight: '600',
                      fontFamily: 'Roboto-Bold',
                    }}
                    >
                      {_.round(props.cartDetail.items_qty)}
                    </Text>
                  </View>
                ) : null
              }
            </Link>
            <TouchableOpacity onPress={shareLinkWithDialog}
              style={{marginTop: RFValue(8), marginLeft: RFValue(20)}}
            >
              <Icon size={RFValue(20)}
              color="white"
              name="share-2"/>
              </TouchableOpacity>
            <View style={{flex: 0.6}} />
          </View>
          
          </View>
      </View>
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    cartDetail: state.cartReducer.cartDetail,
  };
};
export default connect(mapStateToProps, null)(NavigationProducts);
