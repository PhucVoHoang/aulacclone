import React, {Component} from 'react';
import _ from 'lodash';
import {
  View,
  Dimensions,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import {Link} from 'react-router-native';
import {connect} from 'react-redux';
import PopUp from '../../components/authentication/PopupDialog'
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const wrong_img = require("../../../assets/images/wrong.png");
class Header extends Component {
  constructor(props){
    super(props)
    this.state = {
      errorPopup: false
    }
  }
  render() {
    const {props} = this;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: props.bgColor,
          flexDirection: 'column',
        }}>
          {this.state.errorPopup && 
              <PopUp
                visible={true}
                headerOne="CÓ LỖI XẢY RA"
                img_src={wrong_img}
                headerTwo="Tính năng hiện đang phát triển, xin vui lòng thử lại sau"
                buttonOne_text="Đóng"
                buttonOne_bgColor="#6BBD12"
                buttonOne_disable={false}
                buttonOne_link="/landing"
                clicked={() => {
                  this.setState({ errorPopup: false });
                }}
              />
          }
        <View style={{flex: 0.8}} />
        <View
          style={{
            flex: 1.2,
            flexDirection: 'row',
            justifyContent: 'space-around',
              alignItems:'center',
              marginLeft: 0.02 * screenWidth,
              marginRight: 0.02 * screenWidth,
          }}>
          <View
            style={{
              width: 0.5 * screenWidth,
              height: '70%',
              backgroundColor: 'white',
              borderRadius: 5,
              paddingTop: 0,
              paddingBottom: 0,
              elevation: 3,
              shadowColor: 'black',
              backgroundColor: 'white',
              shadowOpacity: 0.3,
              shadowRadius: 4,
              shadowOffset: {
                width: 1,
                height: 3
              },
            }}>
            <TextInput
              placeholder="Tìm kiếm..."
              style={{
                width: '96%',
                height: '100%',
                borderRadius: 5,
                backgroundColor: 'white',
                textAlign: 'auto',
                justifyContent: 'center',
                alignItems: 'center',
                fontFamily: 'Roboto-Regular',
                paddingTop: 0,
                paddingBottom: 0,
                marginLeft: '4%',
                color: "#333"
              }}
              onChangeText={props.onChangeText ? props.onChangeText : null}
              onFocus={() => props.props ? props.props.history.push('/categories/0') : null}
            />
          </View>
          <View style={{width: 0.09 * screenWidth, height: 0.08 * screenWidth}}>
            <Link
              component={TouchableOpacity}
              to={{
                pathname: '/shopping/cart',
                state: {go: props.go},
              }}
              style={{flex: 1}}>
              <Image
                resizeMode="stretch"
                style={{
                  width: 0.065 * screenWidth,
                  height: 0.065 * screenWidth,
                  marginTop: 0.002  * screenHeight,
                }}
                source={props.yellowIcons ? require('../../../assets/images/cartc.png') : require('../../../assets/images/cart.png')}
              />
              {
                props.cartDetail && props.cartDetail.items_qty ? (
                  <View
                    style={{
                      width: 0.04 * screenWidth,
                      height: 0.04 * screenWidth,
                      backgroundColor: 'red',
                      borderRadius: 0.04 * screenWidth / 2,
                      position: 'absolute',
                      top: 0,
                      right: 0,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={{
                      textAlign: 'center',
                      color: '#fff',
                      fontSize: 0.025 * screenWidth,
                      fontWeight: '600',
                      fontFamily: 'Roboto-Bold',
                    }}
                    >
                      {_.round(props.cartDetail.items_qty)}
                    </Text>
                  </View>
                ) : null
              }
            </Link>
          </View>
          <Link
            component={TouchableOpacity}
            to={{
              pathname: '/products/qrcode',
              state: {go: props.go},
            }}>
            <Image
              style={{
                width: 0.05 * screenWidth,
                height: 0.05 * screenWidth,
                // marginTop:  * screenHeight,
              }}
              reszieMode="stretch"
              source={props.yellowIcons ? require('../../../assets/images/qrc.png') : require('../../../assets/images/qr.png')}
            />
          </Link>
          {/* <Link
            component={TouchableOpacity}
            to={{
              pathname: '/chat',
              state: {go: props.go},
            }}>
            <Image
              style={{
                width: 0.06 * screenWidth,
                height: 0.05 * screenWidth,
                // marginTop: 0.005 * screenHeight,
              }}
              reszieMode="stretch"
              source={props.yellowIcons ? require('../../../assets/images/chatc.png') : require('../../../assets/images/chat.png')}
            />
          </Link> */}
          <TouchableOpacity onPress={()=>{ this.setState(()=>({errorPopup: true}))}}>
            <Image
              style={{
                width: 0.06 * screenWidth,
                height: 0.05 * screenWidth,
                // marginTop: 0.005 * screenHeight,
              }}
              reszieMode="stretch"
              source={props.yellowIcons ? require('../../../assets/images/chatc.png') : require('../../../assets/images/chat.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
}
const mapStateToProps = (state) => {
  return {
    cartDetail: state.cartReducer.cartDetail,
  };
};
export default connect(mapStateToProps, null)(Header);
