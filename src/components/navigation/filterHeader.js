import React from 'react';
import {View, TouchableOpacity, Text, Dimensions} from 'react-native';
import {Link} from 'react-router-native';
import Icon from 'react-native-vector-icons/Feather';
import {RFValue} from 'react-native-responsive-fontsize';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const filterHeader = props => {
  return (
    <View
      style={{flex: 1, flexDirection: 'column', backgroundColor: '#ECB22D'}}>
      <View style={{flex: 1}} />
      <View
        style={{
          flex: 1.3,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginRight: 0.04 * screenWidth,
          marginLeft: 0.01 * screenWidth,
        }}>
        <Link
          component={TouchableOpacity}
          style={{alignItems: 'flex-end'}}
          to={props.to}>
          <Icon
            style={{alignSelf: 'flex-start'}}
            size={RFValue(30)}
            color="white"
            name="chevron-left"
          />
        </Link>
        <Text
          style={{
            fontSize: 19,
            color: 'white',
            fontFamily: 'Roboto-BoldCondensed',
            alignSelf: 'center',
          }}>
          {props.title}
        </Text>
        <TouchableOpacity      
          onPress={() => {
            props.reset()
            }}>
          <Text style={{fontFamily: 'Roboto-Regular', color: 'white'}}>
            Reset
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default filterHeader;
