import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TouchableOpacity, Image, Text, Dimensions} from 'react-native';
import {Link} from 'react-router-native';
import _ from 'lodash';
import {categoryListAction} from '../../redux/actions/productActions';

import API from '../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class loadCategory extends Component {
  state = {
    categoryItems: [],
  };
  componentDidMount() {
    API.getCategories().then(res => {
      this.props.reduxCategoryList(_.get(res, 'data', {}));
    }).catch(err => {
      console.log(err);
    })
  }
  render() {
    const icons = {
      'giavi': require('../../../assets/images/Landing/giavi.png'),
      'anlien': require('../../../assets/images/Landing/anlien.png'),
      'giaikhat': require('../../../assets/images/Landing/giaikhat.png'),
      'hangkho': require('../../../assets/images/Landing/hangkho.png'),
      'hanglanh': require('../../../assets/images/Landing/hanglanh.png'),
      'hanglon': require('../../../assets/images/Landing/hanglon.png'),
      'tiettrung': require('../../../assets/images/Landing/tiettrung.png'),
    };
    return (
      <View style={{flex: 1}}>
        <View style={style.categoryStyle}>
          {this.props.categoryItems.map(item => (
            <Link
              key={item.id}
              component={TouchableOpacity}
              to={`/categories/${item.id}`}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 0.24 * screenWidth,
                  height: 0.07 * screenHeight,
                  padding: 5,
                  marginTop: 10
                }}>
                <Image
                  style={{width: undefined, height: undefined, flex: 1, resizeMode: 'contain'}}
                  source={icons[item.slug]}
                />
              </View>
              <Text style={style.textStyle}>{item.name}</Text>
            </Link>
          ))}
        </View>
      </View>
    );
  }
};

const style = {
  categoryStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  textStyle: {
    textAlign: 'center',
    marginTop: 0.01 * screenHeight,
    marginLeft: 5,
    color: '#515151',
    fontFamily: 'Roboto-Regular',
  },
};

const mapStateToProps = (state) => {
  return {
    categoryItems: state.productReducer.category_list
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxCategoryList: (data) => dispatch(categoryListAction(data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(loadCategory);
