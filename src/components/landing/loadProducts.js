import React, {Component} from 'react';
import {View, ScrollView, FlatList, TouchableOpacity, Dimensions, Image, Text} from 'react-native';
import {Link} from 'react-router-native';
import ProductBlock from '../../components/landing/productBlock';
import _ from 'lodash';

import API from '../../services/api';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function Item(props) {
    return (
      <Link
        key={props.item.id}
        component={TouchableOpacity}
        to={{
          pathname: '/products',
          state: {
            going: props.gone,
            productId: props.item.id,
          },
        }}
        style={{
          // flex: 1,
          margin: 15,
          width: props.width * screenWidth,
          height: props.height * screenHeight,
        }}>
          <View style={{flex: 25}}>
          <Image
            style={{
              width: undefined,
              height: undefined,
              flex: 1,
              resizeMode:'contain'
            }}
            source={{uri: props.item.image_url}}
          />
        </View>
        <View style={{flex: 14, justifyContent: 'space-around'}}>
          <Text
            style={{
              color: 'rgb(81,81,81)',
              textAlign: 'left',
              fontFamily: 'Roboto-Regular',
            }}>
            {props.item.name}
          </Text>
          <Text
          style={{
            color: 'rgb(255,30,2)',
            textAlign: 'left',
            fontFamily: 'Roboto-Bold',
          }}>
          {props.item.formated_special_price ? props.item.formated_special_price.replace(',00', '') : props.item.regular_formated_price.replace(',00', '')}
        </Text>
        {props.item.formated_special_price && (<Text
          style={{
            color: 'rgb(160,158,156)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
            textDecorationLine: 'line-through',
          }}>
          {props.item.regular_formated_price.replace(',00', '')}
        </Text>)}
        </View>
      </Link>
    );
  }

class loadProducts extends Component {
  state = {
    productItems: [],
  };
  componentDidMount() {
    API.getProducts({page: 1, category_id: this.props.catId, isNew: this.props.isNew, isFeature: this.props.isFeature, keyword: null}).then(res => {
      this.setState({
        productItems: _.get(res, 'data', {})
      })
    }).catch(err => {
      console.log(err);
    })
  }
  render() {
    const productList = <View style={this.props.style}>{this.state.productItems.map(item => (
        <ProductBlock
            title={item.name}
            img={item.image_url}
            width={0.4}
            height={0.23}
            topMargin={0.03}
            leftMargin={0.04}
            gone="/landing"
            price={item.regular_formated_price}
            special_price={item.formated_special_price}
            productId={item.id}
            key={item.id}
        />
      ))}</View>;
    if(this.props.flat) {
        return (
          <View style={{flex: 1}}>
            <View style={style.container}>
              {this.state.productItems.map(item => (
                <Item key={item.id} item={item} width={0.42} height={0.27} gone={`/landing`}/>
              ))}
            </View>
          </View>
            // <FlatList
            //     contentContainerStyle={{
            //       flex: 1,
            //       flexDirection: 'column',
            //     }}
            //     numColumns={2}
            //     data={this.state.productItems}
            //     renderItem={({ item }) => (
            //       <Item item={item} width={0.42} height={0.27} gone={`/landing`}/>
            //     )}
            //     keyExtractor={item => item.id.toString()}
            //     onEndReachedThreshold={0.5}
            //     initialNumToRender={10}
            //   />
        )
    } else {
        return (
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}> 
                {productList}
            </ScrollView>
        )
    }
  }
};

const style = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
};

export default loadProducts;