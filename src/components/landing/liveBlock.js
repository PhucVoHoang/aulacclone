import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TouchableOpacity, Image, Text, Dimensions} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';
import API from '../../services/api';
import AppConfig from '../../config/AppConfig';
import {blogListAction} from '../../redux/actions/blogActions';
import _ from 'lodash';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class liveBlock extends Component {
  componentDidMount() {
    API.getBlogPosts({catId: AppConfig.videoCatid, page: 1}).then(res => {
      this.props.reduxBlogList(_.get(res, 'data', {}));
    }).catch(err => {
      console.log(err);
    })
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 2.7, justifyContent: 'center'}}>
          <Text
            style={{
              color: 'rgb(174,14,23)',
              fontFamily: 'Roboto-Bold',
              fontSize: RFValue(16),
            }}>
            ÂU LẠC VIDEO
          </Text>
        </View>
        <View
          style={{
            flex: 9.7,
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          {this.props.blogItems.length > 0 && this.props.blogItems.map(item => (
            <Link
              key={item.id}
              component={TouchableOpacity}
              to={{
                pathname: '/profile/blogpost',
                state: {
                  go: '/landing',
                  postId: item.id,
                  data: item,
                  headline: 'ÂU LẠC VIDEO'
                },
              }}
              style={{
                width: 0.28 * screenWidth,
                height: 0.2364 * screenHeight,
            }}>
            <Image
              resizeMode="contain"
              style={{width: undefined, height: undefined, flex: 1}}
              source={{uri: item.image_thumbnail}}
            />
            
            {/* <Image
              resizeMode="contain"
              style={{
                position: 'absolute',
                  marginTop: 0.02 * screenHeight,
  
                  width: 0.15 * screenWidth,
                height: 0.018 * screenHeight,
              }}
              source={require('../../../assets/images/Landing/live.png')}
            /> */}
            <Text
              style={{
                position: 'absolute',
                width: '100%',
                marginTop: 0.18 * screenHeight,
                color: 'white',
                textAlign: 'center',
                fontFamily: 'Roboto-Bold',
              }}>
              {item.title}
            </Text>
          </Link>
          ))}
          
        </View>
        <View style={{flex: 1}} />
      </View>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    blogItems: state.blogReducer.blog_list
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    reduxBlogList: (data) => dispatch(blogListAction(data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(liveBlock);
