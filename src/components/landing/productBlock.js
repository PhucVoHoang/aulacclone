import React from 'react';
import {Text, Dimensions, TouchableOpacity, View, Image} from 'react-native';
import {Link} from 'react-router-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const productBlock = props => {
    return (
    <Link
      component={TouchableOpacity}
      to={{
        pathname: '/products',
        state: {
          going: props.gone,
          productId: props.productId,
        },
      }}
      style={{
        marginTop: props.topMargin * screenHeight,
        marginLeft: props.leftMargin * screenWidth,
        width: props.width * screenWidth,
        height: props.height * screenHeight,
        // elevation: 1,
        // shadowColor: 'black',
        // backgroundColor: 'black',
        // shadowOpacity: 0.3,
        // shadowRadius: 4,
        flex: 1,
      }}>
      <View style={{flex: 25}}>
        <Image
          style={{
            width: undefined,
            height: undefined,
            flex: 1,
            resizeMode:'contain'
          }}
          source={{uri: props.img}}
        />
      </View>
      <View style={{flex: 14, justifyContent: 'space-around'}}>
        <Text
          style={{
            color: 'rgb(81,81,81)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
          }}>
          {props.title}
        </Text>
        <Text
          style={{
            color: 'rgb(255,30,2)',
            textAlign: 'left',
            fontFamily: 'Roboto-Bold',
          }}>
          {props.special_price ? props.special_price.replace(',00', '') : props.price.replace(',00', '')}
        </Text>
        {props.special_price && (<Text
          style={{
            color: 'rgb(160,158,156)',
            textAlign: 'left',
            fontFamily: 'Roboto-Regular',
            textDecorationLine: 'line-through',
          }}>
          {props.price.replace(',00', '')}
        </Text>)}
      </View>
    </Link>
  );
};

export default productBlock;
