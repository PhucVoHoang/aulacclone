import React, { useContext } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import equal from 'deep-equal';
import FastImage from '@rocket.chat/react-native-fast-image';
import { createImageProgress } from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import ImageView from 'react-native-image-view';

const formatAttachmentUrl = (attachmentUrl, userId, token, server) => {
	if (attachmentUrl.startsWith('http')) {
		if (attachmentUrl.includes('rc_token')) {
			return encodeURI(attachmentUrl);
		}
		return encodeURI(`${ attachmentUrl }?rc_uid=${ userId }&rc_token=${ token }`);
	}
	return encodeURI(`${ server }${ attachmentUrl }?rc_uid=${ userId }&rc_token=${ token }`);
};

const ImageProgress = createImageProgress(FastImage);

export const MessageImage = React.memo(({ img }) => (
	<ImageProgress
        style={{
            width: '100%',
            minHeight: 200,
            borderRadius: 4,
            borderWidth: 1,
            overflow: 'hidden'
        }}
		source={{ uri: encodeURI(img) }}
		resizeMode={FastImage.resizeMode.cover}
		indicator={Progress.Pie}
	/>
));

const ImageContainer = React.memo(({
	file, imageUrl, user
}) => {
	const [isImageViewVisible, setImageViewVisible] = React.useState(false);
    const baseUrl = 'http://113.161.94.107:3000';
	const img = formatAttachmentUrl(imageUrl, user.id, user.token, baseUrl);
	const images = [
		{
			source: {
				uri: encodeURI(img),
			},
			title: 'image',
		},
	]
	if (!img) {
		return null;
	}

	if (file.description) {
		return (
			<>
				<TouchableOpacity style={{ width: '100%'}} onPress={() => alert(true)}>
				<View>
					<MessageImage img={img} />
					<Text>
                        {file.description}
                    </Text>
				</View>
				</TouchableOpacity>
				<ImageView
                    glideAlways
                    images={images}
                    imageIndex={0}
                    animationType="fade"
                    isVisible={true}
                    onClose={() => setImageViewVisible(false)}
                    onImageChange={index => {
                        console.log(index);
                    }}
                />
			</>
		);
	}

	return (
		<>
			<TouchableOpacity style={{ width: '100%'}} onPress={() => setImageViewVisible(true)}>
			<MessageImage img={img} />
			</TouchableOpacity>
			<ImageView
                    glideAlways
                    images={images}
                    imageIndex={0}
                    animationType="fade"
                    isVisible={isImageViewVisible}
                    onClose={() => setImageViewVisible(false)}
                    onImageChange={index => {
                        console.log(index);
                    }}
                />
		</>
	);
});


export default ImageContainer;
