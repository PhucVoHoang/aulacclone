import React, {Component, useState} from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  TextInput
} from 'react-native';
import _ from 'lodash';
import {Link} from 'react-router-native';
import {RFValue} from 'react-native-responsive-fontsize';
import { ShareDialog } from 'react-native-fbsdk';
import CHATAPI from '../../services/ChatApi';
import API from '../../services/api';

import NavigationHeader from './../navigation/navigationHeader';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class AddFriend extends Component {
    state = {
        username: '',
        currentUser: ''
    };
    addFriend() {
        CHATAPI.getProfile().then(data => {
            this.setState({ currentUser: data.username });
            const friends = data.bio ? data.bio.split(',') : [];
            friends.push('"'+this.state.username.toString()+'"');
            CHATAPI.addFriends(friends.join()).then(res => {
                alert('Thêm bạn thành công.');
                this.props.history.push('/chat');
            }).catch(err => {
                console.log(err);
            })
        })
    }
    addForFriend() {
      if(this.state.username) {
        API.getUserInfo(this.state.username).then(res => {
          if (res.length > 0 && res[0].custom_field_3) {
            let username = res[0].custom_field_3;
            this.setState({username});
            CHATAPI.getFriendInfo(username).then(data => {
              const friends = data.user.bio ? data.user.bio.split(',') : [];
              friends.push('"'+this.state.currentUser.toString()+'"');
              CHATAPI.addOtherFriends(data.user._id, friends.join()).then(res => {
                console.log('addOtherFriends', res)
                  this.addFriend();
              });
          })
          }
          
        }).catch(err => {
          alert('Không tồn tại user có số điện thoại này.')
        })

        
      }
  }
  shareLinkWithDialog = async () => {
    const SHARE_LINK_CONTENT = {
      contentType: 'link',
      contentUrl: 'http://www.aulac-vegetarian.com/',
      contentDescription: 'Wow, check out this great site!',
    };
    const canShow = await ShareDialog.canShow(SHARE_LINK_CONTENT);
    if (canShow) {
      try {
        const {isCancelled, postId} = await ShareDialog.show(
          SHARE_LINK_CONTENT,
        );
        if (isCancelled) {
          Alert.alert('Share cancelled');
        } else {
          Alert.alert('Share success with postId: ' + postId);
        }
      } catch (error) {
        Alert.alert('Share fail with error: ' + error);
      }
    }
  };
    render() {
      return (
        <View style={{flex: 1, backgroundColor: '#f7f7f7'}}>
          <View style={{flex: 1.1}}>
            <NavigationHeader title="THÊM BẠN" to={{pathname: "/chat", state: { go: "/addfriend", tab: 'contacts' }}} back={true} />
          </View>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View
            style={{
              flex: 8.9,
              width: screenWidth,
            }}>
              <KeyboardAvoidingView style={{ flex: 24, flexDirection: 'column',justifyContent: 'center'}} behavior="padding" enabled keyboardVerticalOffset={10}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Text style={{paddingHorizontal: 20, paddingVertical: 10, color: 'rgb(109, 109, 109)'}}>Thêm bạn bằng tên số điện thoại</Text>
              <View style={shipping_style.viewContainerStyle}>
                <TextInput
                    placeholder="Nhập số điện thoại..."
                    placeholderTextColor = "rgba(0,0,0,0.3)"
                    style={{
                        width: '70%',
                        height: '100%',
                        paddingTop: 0,
                        paddingBottom: 0,
                        marginLeft: '4%',
                        color: "#333"
                    }}
                    autoCapitalize='none'
                    onChangeText={text => this.setState({ username: text })}
                />
                <TouchableOpacity onPress={this.addForFriend.bind(this)} style={{ backgroundColor: 'rgb(236, 178, 45)', paddingVertical: 10, borderRadius: 20, paddingHorizontal: 30, marginRight: 10 }}>
                    <Text style={{ color: 'white' }}>Thêm</Text>
                </TouchableOpacity>
              </View>
              <View style={[shipping_style.viewContainerStyle, { marginTop: 10, justifyContent: 'flex-start', paddingVertical: 20, paddingHorizontal: 20, flexDirection: 'column', alignItems: 'flex-start' }]}>
                    <View style={{ 
                        width: '100%',
                        flexDirection: 'row', 
                        paddingBottom: 20,
                        marginBottom: 20,
                        borderBottomColor: 'rgba(34,34,34,0.1)',
                        borderBottomWidth: 1 }}>
                          
                        <Image
                          style={{alignSelf: 'center', width: 40, height: 40, resizeMode: 'contain'}}
                          source={require('../../../assets/images/addFriend_QR_icon.png')}
                        />
                        <Link component={TouchableOpacity} to={'/addfriend/qrcode'} style={{ paddingStart: 12 }}>
                            <Text style={{color: 'rgb(81, 81, 81)', fontSize: RFValue(14)}}>Quét mã QR code</Text>
                            <Text style={{color: 'rgb(204, 204, 204)', fontSize: RFValue(10), paddingTop: 5}}>Thêm bạn bằng mã QR</Text>
                        </Link>
                    </View>
                    <View style={{ 
                        width: '100%',
                        flexDirection: 'row', 
                        paddingBottom: 20,
                        marginBottom: 20,
                        borderBottomColor: 'rgba(34,34,34,0.1)',
                        borderBottomWidth: 1 }}>
                        <Image
                          style={{alignSelf: 'center', width: 40, height: 40, resizeMode: 'contain'}}
                          source={require('../../../assets/images/addFriend_chat_icon.png')}
                        />
                        <TouchableOpacity onPress={this.shareLinkWithDialog} style={{ paddingStart: 12 }}>
                            <Text style={{color: 'rgb(81, 81, 81)', fontSize: RFValue(14)}}>Giới thiệu Âu Lạc cho bạn bè</Text>
                            <Text style={{color: 'rgb(204, 204, 204)', fontSize: RFValue(10), paddingTop: 5}}>Đăng facebook để mời bạn bè</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ 
                        width: '100%',
                        flexDirection: 'row',}}>
                        <Image
                          style={{alignSelf: 'center', width: 40, height: 40, resizeMode: 'contain'}}
                          source={require('../../../assets/images/addFriend_avatar_icon.png')}
                        />
                        <Link component={TouchableOpacity} to={'/suggestfriend'} style={{ paddingStart: 12 }}>
                            <Text style={{color: 'rgb(81, 81, 81)', fontSize: RFValue(14)}}>Có thể bạn quen</Text>
                            <Text style={{color: 'rgb(204, 204, 204)', fontSize: RFValue(10), paddingTop: 5}}>Thêm bạn từ danh sách gợi ý</Text>
                        </Link>
                    </View>
                    {/* <View style={{ flexDirection: 'row' }}>
                        <Image
                          style={{alignSelf: 'center', width: 40, height: 40, resizeMode: 'contain'}}
                          source={require('../../../assets/images/addFriend_config_icon.png')}
                        />
                        <TouchableOpacity onPress={this.addFriend} style={{ paddingStart: 12 }}>
                            <Text style={{color: 'rgb(81, 81, 81)', fontSize: RFValue(14)}}>Quản lý yêu cầu kết bạn</Text>
                            <Text style={{color: 'rgb(204, 204, 204)', fontSize: RFValue(10), paddingTop: 5}}>Thu hồi hoặc gửi lại yêu cầu kết bạn</Text>
                        </TouchableOpacity>
                    </View> */}
              </View>
            </ScrollView>
            </KeyboardAvoidingView>
          </View>
          </TouchableWithoutFeedback>
        </View>
      );
    }
  }
  
  const shipping_style = {
    headerStyle: {
      color: 'rgb(173,14,23)',
      fontFamily: 'Roboto-Regular',
      fontSize: 16,
      marginTop: 0.027 * screenHeight,
    },
    viewContainerStyle: {
      width: screenWidth,
      padding: 10,
      backgroundColor: 'white',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 10
    },
  };
  
  export default AddFriend;
  
