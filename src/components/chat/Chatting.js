import React, { useDebugValue } from 'react';
import {View, StyleSheet, TouchableHighlight, Dimensions, Text, Image, ActivityIndicator} from 'react-native';
import moment from "moment";
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';
import { SwipeListView } from 'react-native-swipe-list-view';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function Item(props) {
  if(!props.item.lastMessage) return null;
  const title = props.item.name;
  console.log('chatting', props.item);
  // let title = 'props.item.name';
  // if (props.item.usersCount <= 2) {
  //   props._getMembers(props.item._id).then(res => {
  //     return title = res;
  //   });
  // }
  return (
    <Link
      component={TouchableHighlight}
      to={{
        pathname: `/chat/${props.item._id}`,
        state: {
          go: props.gone,
          data: props.item,
        }
      }}
      style={styles.rowFront} underlayColor={'#AAA'}>
      <>
        <View style={styles.iconContent}> 
          <Image
            resizeMode="contain"
            style={{marginRight: 16}}
            source={require('../../../assets/images/disc-jockey.png')}
          />
        </View>

        <View style={{ marginStart: 16, width: screenWidth * 0.7 - 56 }}>
          <Text style={{ fontSize: 16, fontWeight: 'bold', marginBottom: 5 }}>
            {title}
          </Text>
          <Text style={{ marginBottom: 10 }}>
            {props.item.lastMessage.msg}
          </Text>
        </View>
        <View style={{ width: screenWidth * 0.3 - 40 }}>
          <Text style={{ fontSize: 14, textAlign: 'right', color: 'rgba(34,34,34,0.5)' }}>
            {moment(props.item.lastMessage.ts).isSame(moment(), 'day') ? moment(props.item.lastMessage.ts).format('h:mm a') : moment(props.item.lastMessage.ts).format('DD/MM/YYYY')}
          </Text>
        </View> 
      </>
    </Link>
  )
}

const Chatting = ({props, _handleRefresh, _handleLoadMore, _leaveRoom, _getMembers}) => {
  console.log('chattign', props.data)
  return (
    <View style={{flex: 1}}>

      {!props.loading ? (
        <SwipeListView
          useFlatList={true}
          contentContainerStyle={{
            flexGrow: 1,
          }}
          numColumns={1}
          data={props.data}
          renderItem={({ item }) => {
            return <Item item={item} width={0.42} height={0.27} title={''}/>;
          }}
          renderHiddenItem={ (data, rowMap) => (
            <TouchableHighlight style={styles.rowBack} onPress={() => _leaveRoom(data.item._id)}>
                <View style={[styles.backRightBtn, styles.backRightBtnRight]}>
                    <Image
                      style={{flex: 1, width: 20, height: 20, resizeMode: 'contain'}}
                      source={require('../../../assets/images/recycling_bin.png')}
                    /> 
                </View>
            </TouchableHighlight>
          )}
          keyExtractor={item => item._id.toString()}
          onRefresh={_handleRefresh}
          refreshing={props.refreshing}
          onEndReached={_handleLoadMore}
          onEndReachedThreshold={0.5}
          initialNumToRender={10}
          rightOpenValue={-Dimensions.get('window').width*0.2}
          useNativeDriver={false}
        />
      ) : (
        <View 
          style={{flex: 1,
            justifyContent: 'center',
            alignItems:'center'
          }}>
          <ActivityIndicator style={{flex: 1, alignSelf:'center'}} />
        </View>
      )}
    </View>
  );
};
export default Chatting;

const styles = StyleSheet.create({
  rowFront: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: screenWidth,
    height: 72,
    paddingHorizontal: 16,
    paddingVertical: 16,
    borderBottomColor: 'rgba(34,34,34,0.1)',
    borderBottomWidth: 1,
  },
  rowBack: {
      alignItems: 'center',
      backgroundColor: 'white',
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingLeft: 15,
      height: 72,
  },
  backTextWhite: {
      color: '#FFF',
  },
  backRightBtn: {
      flex: 1,
      alignItems: 'center',
      // bottom: 0,
      justifyContent: 'center',
      position: 'absolute',
      // top: 0,
      width: Dimensions.get('window').width*0.2,
      height: 72,
  },
  backRightBtnRight: {
      backgroundColor: 'red',
      right: 0,
  },  
  iconContent: {
    width: 40, 
    justifyContent: 'center', 
    alignContent: 'center', 
  },
});
