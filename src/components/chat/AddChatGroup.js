import React, {Component, useState} from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  TextInput,
  FlatList
} from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/Feather';
import {RFValue} from 'react-native-responsive-fontsize';
import {Link} from 'react-router-native';

import CHATAPI from '../../services/ChatApi';

import NavigationHeader from './../navigation/navigationHeader';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);


function Item(props) {
    return (
      <TouchableOpacity
        onPress={() => props.addGroup(props.item.username)}
        style={shipping_style.rowContent}>
          <View style={shipping_style.iconContent}> 
          <Image
            resizeMode="contain"
            style={{alignSelf: 'center'}}
            source={require('../../../assets/images/disc-jockey.png')}
          />
        </View>

        <View style={{ paddingStart: 16, width: screenWidth * 0.8 - 50, alignSelf: 'center',}}>
          <Text style={{ color: 'rgb(81,81,81)', fontWeight: 'normal', marginBottom: 5 }}>
            {props.item.name}
          </Text>
        </View>      
        <View style={shipping_style.iconContent}> 
          <Icon component={Icon} size={30} color={_.indexOf(props.groups, props.item.username) >= 0 ? "rgb(236, 178, 45)": "rgba(34,34,34,0.2)"} name={_.indexOf(props.groups, props.item.username) >= 0 ? 'plus-circle' : 'circle'} style={{ marginRight: 0 }} />         
        </View>
      </TouchableOpacity>
    )
  }

class AddChatGroup extends Component {
    state = {
        username: '',
        users: [],
        groups: [],
        name: ''
    };
    addGroup(username) {
      let groups = _.cloneDeep(this.state.groups);
      const index = _.indexOf(groups, username);
      if(index >= 0) {
        groups = _.pull(groups, username);
      } else {
        groups.push(username.toString());
      }
      this.setState({groups})
    }
    componentDidMount() {
        CHATAPI.getProfile().then(data => {
            const friends = data.bio;
            CHATAPI.getFriends(friends).then(res => {
              this.setState({users: res.users});
            }).catch(err => {
              console.log(err);
            })
          }).catch(err => {
            console.log(err);
          })
    }
    addFriend() {
      if(this.state.name && this.state.groups.length > 0) {
        const params = {
          name: this.state.name,
          members: this.state.groups
        };
        CHATAPI.addChatroom(params).then(res => {
          this.props.history.push(`/chat/${res.group._id}`);
        }).catch(err => {
          console.log('======', err);
        })
      } else {
        alert("Vui lòng nhập tên nhóm và chọn thành viên.");
      }
        
    }
    render() {
      return (
        <View style={{flex: 1, backgroundColor: '#f7f7f7'}}>
          <View style={{flex: 1.1}}>
            <NavigationHeader title="NHÓM MỚI" to={{pathname: "/chat", state: { go: "/addchatgroup", tab: 'contacts' }}} back={true} />
          </View>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View
            style={{
              flex: 8.9,
              width: screenWidth,
            }}>
              
              <Text style={{paddingHorizontal: 20, paddingVertical: 10, color: 'rgb(109, 109, 109)'}}></Text>
              <View style={shipping_style.viewContainerStyle}>
                <TextInput
                    placeholder="Đặt tên nhóm"
                    placeholderTextColor = "rgba(0,0,0,0.3)"
                    style={{
                        width: '70%',
                        height: '100%',
                        paddingTop: 0,
                        paddingBottom: 0,
                        marginLeft: '4%',
                        color: "#333"
                    }}
                    onChangeText={text => this.setState({ name: text })}
                />
                <TouchableOpacity onPress={this.addFriend.bind(this)} style={{ backgroundColor: 'rgb(236, 178, 45)', paddingVertical: 10, borderRadius: 20, paddingHorizontal: 30, marginRight: 10 }}>
                    <Text style={{ color: 'white' }}>Thêm</Text>
                </TouchableOpacity>
              </View>
              <View style={[shipping_style.viewContainerStyle, { marginTop: 10, justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'flex-start' }]}>
              <FlatList
                contentContainerStyle={{
                    flexGrow: 1,
                    paddingBottom: 100
                }}
                numColumns={1}
                data={this.state.users}
                renderItem={({ item }) => (
                    <Item item={item} width={0.42} height={0.27} groups={this.state.groups} addGroup={this.addGroup.bind(this)}/>
                )}
                keyExtractor={item => item._id.toString()}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
                />
              </View>
            
          </View>
          </TouchableWithoutFeedback>
        </View>
      );
    }
  }
  
  const shipping_style = {
    headerStyle: {
      color: 'rgb(173,14,23)',
      fontFamily: 'Roboto-Regular',
      fontSize: 16,
      marginTop: 0.027 * screenHeight,
    },
    viewContainerStyle: {
      width: screenWidth,
      padding: 10,
      backgroundColor: 'white',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 10
    },
    rowContent: {
      flexDirection: 'row',
      width: 0.9*screenWidth,
      height: 72,
      paddingHorizontal: 16,
      paddingVertical: 16,
      borderBottomColor: 'rgba(34,34,34,0.1)',
      borderBottomWidth: 1,
    },
    iconContent: {
      width: 40, 
      justifyContent: 'center', 
      alignContent: 'center', 
    },
  };
  
  export default AddChatGroup;
  
