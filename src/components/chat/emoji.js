import React from 'react';
import {View, TouchableOpacity, Image, Dimensions, Text, FlatList} from 'react-native';
import emoji from '../../config/emoji';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function Item(props) {
    console.log('======', props);
    return (
      <TouchableOpacity
        onPress={() => props._sendMessage(`:${props.item.name}:`)}
        style={{
          flexDirection: 'row',
          width: screenWidth/3,
          padding: 20,
        }}>
            <Image
                resizeMode="contain"
                style={{marginRight: 20, width:100, height: 100}}
                source={emoji[`:${props.item.name}:`]}
            />
      </TouchableOpacity>
    )
  }

const Emoji = (props) => {
  return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      <FlatList
          contentContainerStyle={{
            flexGrow: 1,
          }}
          numColumns={5}
          data={props.emojis}
          renderItem={({ item }) => (
            <Item item={item} width={0.42} height={0.27} _sendMessage={props._sendMessage}/>
          )}
          keyExtractor={item => item._id.toString()}
          onEndReachedThreshold={0.5}
          initialNumToRender={10}
        />
      
    </View>
  );
};
export default Emoji;
