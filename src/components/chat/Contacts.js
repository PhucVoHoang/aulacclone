import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image, Dimensions, Text, FlatList} from 'react-native';
import {Link} from 'react-router-native';
import Icon from 'react-native-vector-icons/Feather';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

function Item(props) {
  console.log('contact=====', props.item);
  return (
    <Link
      component={TouchableOpacity}
      to={{
        pathname: `/chat/${props.item.username}`,
        state: {
          go: props.gone,
          data: props.item,
          friend: props.item.username
        }
      }}
      style={contact_style.rowContent}>
        <View style={contact_style.iconContent}> 
          <Image
            resizeMode="contain"
            style={{alignSelf: 'center'}}
            source={require('../../../assets/images/disc-jockey.png')}
          />
        </View>

        <View style={{ paddingStart: 16, width: screenWidth * 0.7 - 40, alignSelf: 'center' }}>
          <Text style={{ color: 'rgb(81,81,81)', fontWeight: 'normal', marginBottom: 5 }}>
            {props.item.name}
          </Text>
          {/* <Text style={{ marginBottom: 10 }}>
            {props.item.status}
          </Text> */}
        </View>
    </Link>
  )
}

const Contacts = (props) => {
  return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      <Link
        component={TouchableOpacity}
        to={{
          pathname: `/livechat`,
          state: {
            go: props.gone,
            data: props.item,
          }
        }}
        style={contact_style.rowContent}>
        <View style={contact_style.iconContent}> 
          <Image
            resizeMode="contain"
            style={{alignSelf: 'center'}}
            source={require('../../../assets/images/disc-jockey.png')}
          />
        </View>
        
        <View style={{ marginStart: 16, width: screenWidth * 0.7 - 40 }}>
          <Text style={{ fontSize: 16, color: 'rgb(81,81,81)', fontWeight: 'normal', marginBottom: 5 }}>
            Tư vấn viên
          </Text>
          <Text style={{ color: 'rgb(151,151,151)', marginBottom: 10 }}>
            Tư vấn viên sẽ hỗ trợ bạn 24/7
          </Text>
        </View>
      </Link>
      <FlatList
          contentContainerStyle={{
            flexGrow: 1,
          }}
          numColumns={1}
          data={props.users}
          renderItem={({ item }) => (
            <Item item={item} width={0.42} height={0.27} />
          )}
          keyExtractor={item => item._id.toString()}
          onEndReachedThreshold={0.5}
          initialNumToRender={10}
        />
    </View>
  );
};

const contact_style = {
  rowContent: {
    flexDirection: 'row',
    width: screenWidth,
    height: 72,
    paddingHorizontal: 16,
    paddingVertical: 16,
    borderBottomColor: 'rgba(34,34,34,0.1)',
    borderBottomWidth: 1,
  },
  iconContent: {
    width: 40, 
    justifyContent: 'center', 
    alignContent: 'center', 
  },
};

export default Contacts;
