import React from 'react';
import {Dimensions, View, Text, Image} from 'react-native';
import {CheckBox} from 'react-native-elements';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const paymentOptions = props => {
  return (
    <View style={style_payment.container}>
      <View style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center', flex: 1.5}}>
          <CheckBox
            center
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-thin"
            checked={props.selected}
            onIconPress={props.click}
            onPress={props.click}
            checkedColor="rgb(215,84,14)"
            uncheckedColor="rgb(160,158,156)"
            containerStyle={{
              width: 30,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}
          />
          <View style={{justifyContent: 'center'}}>
            <Text style={style_payment.textStyle}>{props.title}</Text>
            <Image
              style={style_payment.imgStyle}
              resizeMode="contain"
              source={props.imgsrc}
            />
          </View>
        </View>
      </View>
    </View>
  );
};
const style_payment = {
  container: {
    width: 0.92 * screenWidth,
    marginTop: 0.013 * screenHeight,
    height: 0.12 * screenHeight,
    backgroundColor: 'rgb(255,255,255)',
  },
  imgStyle: {
    height: 0.044 * screenHeight,
    width: 0.086 * screenWidth,
    marginTop: 5,
  },
  textStyle: {
    color: 'rgb(173,14,23)',
    fontSize: 14,
  },
};

export default paymentOptions;
