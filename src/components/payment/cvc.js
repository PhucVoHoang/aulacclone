import React from 'react';
import {View, Text, Dimensions, TextInput} from 'react-native';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const cvc = props => {
  return (
    <View>
      <Text style={cvc_style.headerStyle}>Thông tin chủ thẻ</Text>
      <View style={cvc_style.containerStyle}>
        <TextInput
          style={{marginLeft: 0.04 * screenWidth}}
          placeholder="Tên chủ thẻ"
        />
      </View>
      <View style={cvc_style.containerStyle1}>
        <TextInput
          style={{marginLeft: 0.04 * screenWidth}}
          placeholder="Số thẻ"
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 0.015 * screenHeight,
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            height: 0.076 * screenHeight,
            width: 0.44 * screenWidth,
          }}>
          <TextInput
            style={{marginLeft: 0.04 * screenWidth}}
            placeholder="Thời gian hết hạn"
          />
        </View>
        <View
          style={{
            backgroundColor: 'white',
            height: 0.076 * screenHeight,
            width: 0.44 * screenWidth,
          }}>
          <TextInput
            style={{marginLeft: 0.04 * screenWidth}}
            placeholder="CVV / CVC"
            value={props.value}
            onChange={props.change}


            maxLength={5}
          />
        </View>
      </View>
    </View>
  );
};

const cvc_style = {
  headerStyle: {
    color: 'rgb(215,84,14)',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    marginTop: 0.025 * screenHeight,
  },
  containerStyle: {
    height: 0.076 * screenHeight,
    width: 0.92 * screenWidth,
    backgroundColor: 'white',
    marginTop: 0.01 * screenHeight,
  },
  containerStyle1: {
    height: 0.076 * screenHeight,
    width: 0.92 * screenWidth,
    backgroundColor: 'white',
    marginTop: 0.015 * screenHeight,
  },
};

export default cvc;
