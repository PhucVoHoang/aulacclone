/**
 * @format
 */
import {AppRegistry} from 'react-native';
console.log('getAppKeys()=',AppRegistry.getAppKeys());
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
