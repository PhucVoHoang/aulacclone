import React from 'react';
import {NativeRouter, Route, Switch} from 'react-router-native';
import {StatusBar} from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { store, persistor } from './src/redux/store';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SignIn from './src/screens/authentication/signIn';
import Registration from './src/screens/authentication/Registration';
import Register_Type from './src/screens/authentication/Register_Type';
import ChangeLocation from './src/screens/authentication/ChangeLocation';
import Policy from './src/screens/authentication/Policy';
import OTP from './src/screens/authentication/OTP';
import Phone_Inout from './src/screens/authentication/Phone_Input';
import ForgotPassword from './src/screens/authentication/ForgotPassword';
import Tatca from './src/screens/productCategories/categories/Tatca';
import Landing from './src/screens/landing/Landing';
import Products from './src/screens/productCategories/products/Products';
import ScanQRCode from './src/screens/productCategories/products/ScanQRCode';
import Giavi from './src/screens/productCategories/categories/Giavi';
import Cart from './src/screens/cart/Cart';
import Checkout from './src/screens/cart/Checkout';
import Success from './src/screens/cart/Successful';
import Notifications from './src/screens/landing/Notifications';
import Profile from './src/screens/landing/Profile';
import Chat from './src/screens/landing/Chat';
import ChatScreen from './src/screens/landing/ChatScreen';
import LiveChatScreen from './src/screens/landing/LiveChatScreen';
import UnderConstruction from './src/screens/landing/underConstruction';
import Filter from './src/screens/productCategories/products/Filter';
import Blog from './src/screens/blog/Blog';
import BlopPost from './src/screens/blog/blogPost';
import EditProfile from './src/screens/editProfile/editProfile';
import ChatSettings from './src/screens/editProfile/settings/ChatSettings';
import NotificationSettings from './src/screens/editProfile/settings/NotificationSettings';
import PrivacySettings from './src/screens/editProfile/settings/PrivacySettings';
import QRChatScan from './src/screens/editProfile/QRChatScan';
import MyQRCode from './src/screens/editProfile/MyQRCode';

import FilterResults from './src/screens/productCategories/products/filterResults';
import Payment from './src/screens/cart/Payment';
import ShippingDetails from './src/screens/cart/shippingDetails';
import OrderHistory from "./src/screens/editProfile/OrderHistory";
import OrderDetail from "./src/screens/editProfile/OrderDetail";
import Stores from "./src/screens/editProfile/Stores";
import StoreDetail from "./src/screens/editProfile/StoreDetail";
import MyFavorite from "./src/screens/editProfile/MyFavorite";
import MyProfile from "./src/screens/editProfile/settings/MyProfile";
import MyAddress from "./src/screens/editProfile/settings/MyAddress";
import AddFriend from "./src/components/chat/AddFriend";
import AddChatGroup from "./src/components/chat/AddChatGroup";
import SuggestFriend from "./src/components/chat/SuggestFriend";

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaProvider>
          <NativeRouter>
          <StatusBar
            translucent
            backgroundColor="transparent"
            barStyle="light-content"
          />
          <Switch>
            <Route exact path="/" component={ChangeLocation} />
            <Route exact path="/underconstruction" component={UnderConstruction}/>
            <Route exact path="/products/filter" component={Filter} />
            <Route exact path="/products/filter/filterresults" component={FilterResults} />

            <Route exact path="/profile" component={Profile} />
            <Route exact path="/profile/blog/:catId" component={Blog} />
            <Route exact path="/profile/blogpost" component={BlopPost} />
            <Route exact path="/profile/editprofile" component={EditProfile} />
            <Route exact path="/profile/orderhistory/:catId" component={OrderHistory} />
            <Route exact path="/profile/orders" component={OrderDetail} />
            <Route exact path="/profile/orders/:orderId" component={OrderDetail} />
            <Route exact path="/profile/stores" component={Stores} />
            <Route exact path="/profile/storedetail" component={StoreDetail} />
            <Route exact path="/profile/wishlist" component={MyFavorite} />

            <Route exact path="/profile/myprofile" component={MyProfile} />
            <Route exact path="/profile/myaddress" component={MyAddress} />
            <Route exact path="/profile/chatsettings" component={ChatSettings} />
            <Route exact path="/profile/notificationsettings" component={NotificationSettings} />
            <Route exact path="/profile/privacysettings" component={PrivacySettings} />

            <Route exact path="/chat" component={Chat} />
            <Route exact path="/chat/:roomId" component={ChatScreen} />
            <Route exact path="/livechat" component={LiveChatScreen} />
            <Route exact path="/addfriend" component={AddFriend} />
            <Route exact path="/addfriend/qrcode" component={QRChatScan} />
            <Route exact path="/addfriend/myqrcode" component={MyQRCode} />
            <Route exact path="/addchatgroup" component={AddChatGroup} />
            <Route exact path="/suggestfriend" component={SuggestFriend} />

            <Route exact path="/notification" component={Notifications} />
            <Route exact path="/register" component={Registration} />
            <Route exact path="/forgotpassword" component={ForgotPassword} />
            <Route exact path="/categories/tatca" component={Tatca} />
            <Route exact path="/signin" component={SignIn} />
            <Route exact path="/landing" component={Landing} />
            <Route exact path="/otp" component={OTP} />
            <Route exact path="/phoneInput" component={Phone_Inout} />
            <Route exact path="/policy" component={Policy} />
            <Route exact path="/registerType" component={Register_Type} />
            <Route exact path="/changelocation" component={ChangeLocation} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/products/qrcode" component={ScanQRCode} />
            <Route exact path="/categories/:catId" component={Giavi} />
            <Route exact path="/featurecategories/:catId" component={Tatca} />
            <Route exact path="/shopping/cart" component={Cart} />
            <Route exact path="/shopping/checkout" component={Checkout} />
            <Route exact path="/shopping/payment" component={Payment} />
            <Route exact path="/shopping/shipping" component={ShippingDetails} />

            <Route exact path="/shopping/success" component={Success} />
          </Switch>
        </NativeRouter>
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
